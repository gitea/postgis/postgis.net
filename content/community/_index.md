---
title: Community
date: 2022-02-01
description: "Community resources for improving PostGIS"
weight: 50
geekdocHidden: false
layout: toplevel
aliases: ['/support']
---

PostGIS is an open source project, and that means that it only exists to serve the community, and the community is the primary way people grow and learn more about PostGIS.
