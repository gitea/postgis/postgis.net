---
title: Support
date: 2022-02-01
weight: 10
description: "How to help support PostGIS and make it grow!"
geekdocHidden: false
---

How can you support the PostGIS community and the continued growth and maintenance of this software?

## Get Involved

This web site, the [documentation](/docs), the [workshop](/workshops/postgis-intro) materials, the [video]({{< relref "video" >}}) talks, the help people get on [online chat]({{< relref "chat" >}}) and [mailing lists]({{< relref "mailinglists" >}}), all of this works because members of the PostGIS community step forward and help each other.

## Donate Money

The PostGIS community historically holds a small development meeting once a year for core contributors, and the donations we receive via [OSGeo](https://osgeo.org) help support those events.

<div class="donate_button"><a target="_blank" href="https://www.paypal.com/donate/?hosted_button_id=UZTSJP9LQVXLE">Donate via PayPal or Credit Card</a></div>

<div class="donate_button"><a target="_blank" href="https://github.com/sponsors/OSGeo">Donate via Github Sponsors</a></div>

## Hire a Contributor

The most direct support the PostGIS development community gets is via governments, companies, and NGOs that directly fund the work of a contributor to make the software or the supporting materials better. The following organizations have employed a contributor over the past year.

<a href="https://crunchydata.com/"><img height="34" class="postgis_sponsor_logo" src="/sponsors/logo_crunchy.svg" style="background:white"></a>
<a href="https://strk.kbt.io/services.html"><img height="30" class="postgis_sponsor_logo" src="/sponsors/logo_netlab.png" style="background:white"></a>
<a href="https://www.paragoncorporation.com/"><img height="49" class="postgis_sponsor_logo" src="/sponsors/logo_paragon.jpg"></a>
<a href="https://www.kontur.io/"><img height="44" class="postgis_sponsor_logo" src="/sponsors/logo_kontur.png"></a>
<a href="https://oslandia.com"><img height="50" class="postgis_sponsor_logo" src="/sponsors/logo_oslandia.png" style="background-color:white"></a>
<!--<a href="https://aws.amazon.com/"><img height="34" class="postgis_sponsor_logo" src="/sponsors/logo_aws.svg" style="background-color:white" /></a>-->
<a href="https://nibio.no/"><img height="34" class="postgis_sponsor_logo" src="/sponsors/logo_nibio.png" style="background-color:black"></a>
