---
title: Podcast
date: 2023-10-20
weight: 61
description: "Podcast episodes about PostGIS"
geekdocHidden: false
---

Every once in a while we are invited or someone else is invited on Podcasts to talk about PostGIS.


* [2023-Oct - Hacking Postgres: Ep 6 PostGIS Regina Obe, Paul Ramsey](https://www.youtube.com/watch?v=q3TXM6Nu7Aw)
* [2023-Sept - Path to Citus Con: Why people care about PostGIS and Postgres with Paul Ramsey & Regina Obe](https://pathtocituscon.transistor.fm/episodes/why-people-care-about-postgis-and-postgres-with-paul-ramsey-regina-obe)
* [2023-Jun - Mapscaping: PostgreSQL – Listen and Notify Clients In Real Time - Paul Ramsey](https://mapscaping.com/podcast/postgresql-listen-and-notify-clients-in-real-time/)
* [2023-May - Mapscaping: Rasters in Databases - Paul Ramsey](https://mapscaping.com/podcast/rasters-in-a-database/) 
* [2020-Sept - Mapscaping: Spatial SQL – GIS without the GIS - Paul Ramsey](https://mapscaping.com/podcast/spatial-sql-gis-without-the-gis/)
* [2020-April - Mapscaping: Dynamic vector tiles straight from the database - Paul Ramsey](https://mapscaping.com/podcast/dynamic-vector-tiles-straight-from-the-database/)
* [2020-Feb - Mapscaping: PostgreSQL – An open source geospatial database for GIS practitioners - John Bryant](https://mapscaping.com/podcast/postgresql-an-open-source-geospatial-database-for-gis-practitioners/)
