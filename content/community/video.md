---
title: Video
date: 2022-02-01
weight: 60
description: "There is a lot of video content on how to use PostGIS."
geekdocHidden: false
---

Conferences and events are generating new and interesting talks all the time. Here we gather some useful examples from past events and around the PostGIS community.

## PostGIS Day

Since 2019 we have been celebrating "PostGIS Day" (the day after "[GIS Day](https://www.gisday.com/)") with a day of talks about the use and development of PostGIS.

* [2023](https://www.youtube.com/playlist?list=PLesw5jpZchueoLk5-dLP4c4wLwiknD-Dk)
	| [2022](https://www.youtube.com/playlist?list=PLesw5jpZchudJTmRukWO1eP5-6zPpIm5x)
	| [2021](https://www.youtube.com/playlist?list=PLesw5jpZchudjKjwvFks-gAbz9ZZzysFm)
	| [2020](https://www.youtube.com/playlist?list=PLesw5jpZchufVkPcwGYpiiijThFcPNlz_)
	| [2019](https://www.youtube.com/playlist?list=PLesw5jpZchudjfkx3vggVmJAOSBH7m4nj)

## Videos on OSGeo PeerTube instance

* [PostGIS @ OSGeo Videos](https://video.osgeo.org/w/p/gJUkxkKKv7L5KLZyFft6yU)

## FOSS4G

The international geospatial open source conference, [FOSS4G](https://foss4g.org) always features a lot of new PostGIS content:

* [PostGIS @ FOSS4G](https://www.youtube.com/@FOSS4G/search?query=postgis)


## Selected Favourites

* [PostGIS Day 2023 - Ask us anything](https://www.youtube.com/watch?v=FgbK82cbCnk), Paul Ramsey, Regina Obe
* [Everything About PostGIS!](https://www.youtube.com/watch?v=g4DgAVCmiDE), Paul Ramsey
* [PostGIS Vision: Past, Present, and Future](https://www.youtube.com/watch?v=xnF0PqMB3cI), Regina Obe
* [Exploring the Many Extensions of PostGIS!](https://www.youtube.com/watch?v=DvrktJ3Kvvw), Regina Obe
* [Skating to Where The Puck Is Going To Be: Full Stack Analytics in SQL](https://www.youtube.com/watch?v=5Zg8j9X2f-Y), Brian Timoney
* [PostGIS Function of the Week](https://www.youtube.com/playlist?list=PL2QkPKFw3_dTmW19lk_0vPKS9K6LCgTwT), Rhys Stewart
* [10 Things I've Learnt About PostGIS](https://www.youtube.com/watch?v=93bX1AO95nY), Daniel Silk
* [Leveraging PostGIS in an Enterprise Environment](https://youtu.be/oDRgryuxcIM), Cliff Patterson
* [Using QGIS with PostGIS](https://youtu.be/eddcoyLtqqs), Jessica Kane
