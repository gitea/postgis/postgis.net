---
title: "FOSS4G 2023 Asia"
date: "2023-06-01"
eventDates: "2023-11-28 to 2023-12-02"
expiryDate: "2023-12-03"
tags: ["FOSS4G"]
eventLink: "https://foss4g.asia/2023/"
location: "Seoul, Korea"
image: "foss4g-asia-logo.png"
geekdocHidden: true
---

It’s our great pleasure to let you know that FOSS4G-Asia 2023 will take place in Seoul at coming beautiful Autumn as an in-person event. This year’s FOSS4G-Asia will be held at [Seoul Hall of Urbanism & Architecture](https://sca.seoul.go.kr/seoulhour/site/urbanArch/home) from 28th November to 2nd December 2023.

<!--more-->

FOSS4G stands for Free and Open Source Software for Geospatial. It encompasses all the tools, platforms, and software that allow users to work with geographic data and create geospatial solutions. FOSS4G tools and software have become increasingly popular worldwide because they provide free and open solutions for working with geospatial data, which have historically been dominated by expensive proprietary software. The use of FOSS4G in the geospatial industry has resulted in a significant reduction of costs and an increase in innovation and collaboration among geospatial communities around the world.

FOSS4G-Asia 2023 Seoul conference will be a very unique place to discuss the future of FOSS4G especially in Asia. Attendees can meet many like-minded people and could share experiences during the event.

Please mark your calendar and come to Seoul this Autumn. We’ll warmly greet all of you.
