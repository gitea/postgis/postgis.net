---
title: "PgConf NYC 2025"
date: "2024-12-05"
eventDates: "2025-09-29 to 2025-10-01"
expiryDate: "2025-10-01"
tags: ["PgConf", "PostgreSQL"]
eventLink: "https://2025.pgconf.nyc/"
location: "New York, USA"
image: "pgconf_nyc_sm.png"
geekdocHidden: true
---

PGConf NYC 2025 is a 3-day conference packed with user stories and best practices for how to use PostgreSQL, the world's most advanced open source database. Join us in New York City and connect with other developers, DBAs, administrators, decisions makers, and contributors to the open source PostgreSQL community!

<!--more-->

![](pgconf_nyc_md.png)

[PGConf NYC 2025](https://2025.pgconf.nyc/) is part of a conference series produced by the [United States PostgreSQL Association](https://www.postgresql.us/), (PgUS), a 501(c)(3) nonprofit that supports growth and education of [PostgreSQL](https://www.postgresql.org/), the world's most advanced open source database. For over 25 years, the PostgreSQL community has worked to organize conferences with many technical and business talks by users and developers globally. Any profits from the event go to PgUS to fund future PgUS events and support the PgUS mission.
