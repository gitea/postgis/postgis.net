---
title: "PostGIS Day 2024"
date: "2024-08-29"
eventDates: "2024-11-21"
expiryDate: "2024-11-23"
tags: ["PostGIS"]
eventLink: "https://www.crunchydata.com/community/events/postgis-day-2024/"
location: "Online"
image: "postgis-day.jpg"
geekdocHidden: true
---

PostGIS Day is an opportunity for developers, users, and enthusiasts to come together to share knowledge, use cases, and the latest developments related to this spatial database technology. The online event is focused on the myriad ways in which PostGIS supports geographic information system (GIS) capabilities within PostgreSQL. PostGIS Day is observed the day after [GIS Day](https://gisday.com) and the day after GIS day in 2024 is November 21st, 2024.

<!--more-->

## What to Expect

Have a great story to tell about how you use PostGIS? We are looking for papers in a variety of use cases to represent the breadth and sophistication of PostGIS. Talk slots are 30 minutes long and talks generally last 20-25 minutes with time for transition and questions.

![](postgis-day-m.jpg)

Talks can be given live or pre-recorded. Talk slots can be timed based on your timezone. Presenters should expect to answer questions live and via chat following their presentation. Talks are recorded and posted to our Youtube Channel. Look at [PostGIS Day 2023 Videos](https://www.youtube.com/watch?v=-1lxoRwiw2Q&list=PLesw5jpZchueoLk5-dLP4c4wLwiknD-Dk) to get a flavor of what to expect. Speakers can opt out of video posting.


