---
title: "FOSS4G 2023 North America"
date: "2023-05-01"
eventDates: "2023-10-23 to 2023-10-25"
expiryDate: "2023-10-26"
tags: ["FOSS4G"]
eventLink: "https://foss4gna.org/"
location: "Baltimore, Maryland, USA"
image: "foss4g2023na-logo.png"
geekdocHidden: true
---

Join the community of data engineers, developers, managers, analysts, and all who love the location in Baltimore to learn, share, and break bread. This in-person event brings back the human relationships and opportunities of geospatial technology.

<!--more-->

## What to Expect

The aim of the conference is twofold: the exchange of information and experiences between users of geographic data and free and open source software for processing geographic information, and the possibility for new or potential users to discover the functionality of these systems and the availability of data, and to meet developers and experienced users.
