---
title: "PostgreSQL EU 2024"
date: "2024-10-09"
eventDates: "2024-10-22 to 2024-10-25"
expiryDate: "2024-10-25"
tags: ["PostgreSQL"]
eventLink: "https://www.postgresql.eu/events/pgconfeu2024/schedule/"
location: "Athens, Greece"
image: "postgresql-eu-2024.png"
geekdocHidden: true
---

PostgreSQL Europe is proud to announce the 14th Annual PostgreSQL Conference Europe 
which will be held at the Divani Caravel Hotel in Athens, Greece, 
on October 23–25, 2024 with an additional day of world-class PostgreSQL training offered on October 22. 

Also occurring on October 22nd in the same venue is the [Extensions Ecosystem Summit](https://www.eventbrite.com/e/extension-ecosystem-summit-tickets-1022518730047) where PostgreSQL extension developers will gather
together to discuss extension infrastructure in PostgreSQL.

<!--more-->

PostgreSQL Europe is great conference for PostGIS users because it provides a broader view of all that is possible in [PostgreSQL](http://www.postgresql.org/) community as well as the growing family of PostgreSQL extensions.