---
title: "FOSS4G 2023"
date: "2022-07-26"
eventDates: "2023-06-26 to 2023-07-01"
expiryDate: "2023-07-02"
tags: ["FOSS4G"]
eventLink: "https://2023.foss4g.org/"
location: "Prizren, Kosovo"
image: "foss4g2023-logo-sm.png"
geekdocHidden: true
---

The Free and Open Source Software for Geospatial (FOSS4G) is an annual recurring global event hosted by [OSGeo](https://www.osgeo.org).

<!--more-->

## What to Expect

This year FOSS4G will be held in Prizren, Kosovo. Prizren is known for its rich history, beautiful architecture, and stunning natural scenery. The city has been inhabited since ancient times and has been ruled by various empires and kingdoms throughout its history, including the Byzantine Empire, Ottoman Empire, and Yugoslavia.

![](foss4g2023-logo-med.png)

Organized by OSGeo and with more than 15 years of experience, FOSS4G is an international annual gathering of location enthusiasts is the largest global gathering for geospatial software.

FOSS4G brings together developers, users, decision-makers and observers from a broad spectrum of organizations and fields of operation. Through seven days of workshops, presentations, discussions, and cooperation, FOSS4G participants create effective and relevant geospatial products, standards, and protocols.

Conference attendees are

* Developers and Users of GeoSpatial Software
* Technical Leaders
* Private Companies
* National and International Organizations, both Governmental and NGO
* Teachers and Education Professionals
* Scientific Communities and Researchers

and the talks cover topics such as:

* Software status, new software/project development, benchmarking
* FOSS4G implementations in land management, crisis/disaster response, smart cities, population mapping, climate change, ocean and marine monitoring, etc.
* Data visualization: spatial analysis, manipulation and visualization of geospatial data
* Data collection, data sharing, data science, open data, big data, data exploitation platforms
* Sensors, remote sensing, laser-scanning, structure from motion
* New trends: IoT, Indoor mapping, drones, AI
* Open and reproducible science
* Standards, interoperability, SDIs
* Community & participatory FOSS4G
* FOSS4G at governmental institutions
* FOSS4G in education
* Business products powered by FOSS4G
