---
title: "FOSS4G 2024 Europe"
date: "2023-07-14"
eventDates: "2024-07-01 to 2024-07-07"
expiryDate: "2024-07-08"
tags: ["FOSS4G"]
eventLink: "https://2024.europe.foss4g.org/"
location: "Tartu, Estonia"
image: "foss4g2024-eu-sm.jpg"
geekdocHidden: true
---

Join the community of data engineers, developers, managers, analysts, and all who love location in Tartu to learn, share, and break bread. This in-person event brings back the human relationships and opportunities of geospatial technology.

<!--more-->

## What to Expect

The aim of the conference is twofold: the exchange of information and experiences between users of geographic data and free and open source software for processing geographic information, and the possibility for new or potential users to discover the functionality of these systems and the availability of data, and to meet developers and experienced users.
