---
title: "FOSS4G 2025 Europe"
date: "2024-12-05"
eventDates: "2025-07-14 to 2025-07-20"
expiryDate: "2025-07-21"
tags: ["FOSS4G"]
eventLink: "https://2025.europe.foss4g.org/"
location: "Mostar, Bosnia-Herzegovina"
image: "foss4g-eu-logo.png"
geekdocHidden: true
---

Join the community of data engineers, developers, managers, analysts, and all who love location in Tartu to learn, share, and break bread. This in-person event brings back the human relationships and opportunities of geospatial technology.

<!--more-->

## What to Expect

The aim of the conference is twofold: the exchange of information and experiences between users of geographic data and free and open source software for processing geographic information, and the possibility for new or potential users to discover the functionality of these systems and the availability of data, and to meet developers and experienced users.
