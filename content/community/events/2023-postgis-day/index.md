---
title: "PostGIS Day 2023"
date: "2023-10-01"
eventDates: "2023-11-16"
expiryDate: "2023-11-19"
tags: ["PostGIS"]
eventLink: "https://www.crunchydata.com/community/events/postgis-day-2023/"
location: "Online"
image: "postgis-day.jpg"
geekdocHidden: true
---

PostGIS Day is an opportunity for developers, users, and enthusiasts to come together to share knowledge, use cases, and the latest developments related to this spatial database technology. The online event is focused on the myriad ways in which PostGIS supports geographic information system (GIS) capabilities within PostgreSQL. Typically, PostGIS Day is observed on November 16th, the day after [GIS Day](https://gisday.com).

<!--more-->

## What to Expect

Have a great story tell about how you use PostGIS? We are looking for papers in a variety of use cases to represent the breadth and sophistication of PostGIS. Talk slots are 30 minutes long and talks generally last 20-25 minutes with time for transition and questions.

![](postgis-day-m.jpg)

Talks are generally given live but can be pre-recorded. Talk slots can be timed based on your timezone. Presenters should expect to answer questions live and via chat following their presentation. Talks are recorded and posted to our [YouTube channel](https://www.youtube.com/playlist?list=PLesw5jpZchudJTmRukWO1eP5-6zPpIm5x). Speakers can opt out of video posting.


