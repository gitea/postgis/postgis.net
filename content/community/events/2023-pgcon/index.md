---
title: "PgCon 2023"
date: "2023-05-04"
eventDates: "2023-05-30 to 2023-06-02"
expiryDate: "2023-06-03"
tags: ["PgCon", "PostgreSQL"]
eventLink: "https://www.pgcon.org/2023/"
location: "Ottawa, Canada"
image: "pgcon-logo.png"
geekdocHidden: true
---

PGCon is a great conference for PostGIS users because it provides a platform for learning, networking, and collaboration within the broader [PostgreSQL](http://www.postgresql.org/) community.

<!--more-->

[PGCon](https://www.pgcon.org/) is the place to meet, discuss, build relationships, learn valuable insights, and generally chat about the work you are doing with PostgreSQL. If you want to learn why so many people are moving to PostgreSQL, PGCon will be the place to find out why. Whether you are a casual user or you've been working with PostgreSQL for years, PGCon will have something for you.
