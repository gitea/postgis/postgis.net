---
title: "POSETTE 2024"
date: "2024-05-22"
eventDates: "2024-06-11 to 2024-06-13"
expiryDate: "2024-06-14"
tags: ["POSETTE", "PostgreSQL"]
eventLink: "https://www.citusdata.com/posette/2024/"
location: "Virtual"
image: "2024-posette-logo.png"
geekdocHidden: true
---

POSETTE, formerly know as Citus Con, is a three-day free and virtual event focused on PostgreSQL
and PostgreSQL extensions and brought to you by the Postgres Team at Microsoft.

Regina Obe, PostGIS PSC member, will be giving a keynote at this year's event.

<!--more-->

POSETTE is a great conference for PostGIS users because it provides a broader view of all that is possible in [PostgreSQL](http://www.postgresql.org/) community as well as the growing family of PostgreSQL extensions.