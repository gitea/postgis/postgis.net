---
title: "PgConf.Dev 2025"
date: "2024-12-06"
eventDates: "2025-05-13 to 2025-05-16"
expiryDate: "2025-05-17"
tags: ["PgCon", "PostgreSQL"]
eventLink: "https://2025.pgconf.dev/"
location: "Montreal, Canada"
image: "pgconfdev-sm.jpg"
geekdocHidden: true
---

[PostgreSQL Development Conference](https://2025.pgconf.dev/) is where users, developers, and community organizers come together to focus on PostgreSQL development and community growth.

<!--more-->

Meet PostgreSQL contributors, learn about upcoming features, and discuss development problems with PostgreSQL enthusiasts.

![](pgconfdev-sm.jpg)

PGConf.dev 2025 will be held in Montreal, Canada, on May 13-16 at the [Plaza Centre-Ville](https://maps.app.goo.gl/47BqKbFvDkbzeVT8A).
