---
title: "PgConf Europe"
date: "2023-06-01"
eventDates: "2023-12-12 to 2023-12-15"
expiryDate: "2023-12-16"
tags: ["PostgreSQL"]
eventLink: "https://2023.pgconf.eu/"
location: "Prague, Czechia"
image: "postgresql.png"
geekdocHidden: true
---

This year's conference is the 13th Annual PostgreSQL Conference Europe, and for this year we are going to Prague, Czechia. The conference is organised by PostgreSQL Europe, with participation from most of the PostgreSQL user groups around Europe, and is intended to be an important meeting and cooperation point for users both in and out of Europe.

<!--more-->

PGConf.EU is a unique chance for European PostgreSQL users and developers to catch up, learn, build relationships, get to know each other and consolidate a real network of professionals that use and work with PostgreSQL.

The event is moved between major cities in Europe each year, to make it easy for as many people as possible to come to the conference.

The conference covers a wide range of topics, as we expect talks ranging from internals discussions led by leading developers to end-user case-studies from small companies as well as large multinational corporations and government organisations, all who run their businesses on PostgreSQL.

The conference will run multiple parallel tracks, covering the different technical levels to make sure there is always something interesting for everybody.

Given this wide range of talks, the conference is suitable for many different audiences:

* DBAs already using PostgreSQL, or considering doing so
* Developers of any kind of application, from hobbyists to large web and enterprise applications
* Decision-makers interested in evaluating the world's most advanced open source database as an alternative to traditional proprietary products
* PostgreSQL contributors — code, documentation, support — whatever you help the project with
* Open source enthusiasts in general
