---
title: "FOSS4G 2024 North America"
date: "2024-05-22"
eventDates: "2024-09-09 to 2024-09-11"
expiryDate: "2024-09-12"
tags: ["FOSS4G"]
eventLink: "https://foss4gna.org/"
location: "Saint Louis, Missouri, USA"
image: "square_logo_foss4gna_stl_2024.webp"
geekdocHidden: true
---

Join the community of data engineers, developers, managers, analysts, and all who love the location in Saint Louis, one of the GIS capitals of USA to learn, share, and break bread. This in-person event brings back the human relationships and opportunities of geospatial technology.

<!--more-->

## What to Expect

The aim of the conference is twofold: the exchange of information and experiences between users of geographic data and free and open source software for processing geographic information, and the possibility for new or potential users to discover the functionality of these systems and the availability of data, and to meet developers and experienced users.

There is a great lineup of workshops on Monday Sept 9th covering many OSGeo projects. The next two days are conference
with FOSS4G talks involving OSGeo projects as well as other Free and Open Source GIS software.

## Submissions

Talk submission is closed, but you can still register a Poster at 
https://talks.osgeo.org/foss4g-na-2024/

## Register
You can register to attend at: https://www.foss4gna.org/#tickets
