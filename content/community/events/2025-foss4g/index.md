---
title: "FOSS4G 2025"
date: "2024-12-07"
eventDates: "2025-11-17 to 2025-11-23"
expiryDate: "2025-11-23"
tags: ["FOSS4G"]
eventLink: "https://2025.foss4g.org/"
location: "Auckland, New Zealand"
image: "foss4g-2025-logo.png"
geekdocHidden: true
---

The Free and Open Source Software for Geospatial (FOSS4G) is an annual recurring global event hosted by [OSGeo](https://www.osgeo.org).

<!--more-->

## What to Expect

This year FOSS4G will be held in Auckland, New Zealand.

![](foss4g-2025-logo.png)

Organized by OSGeo and with more than 15 years of experience, FOSS4G is an international annual gathering of location enthusiasts is the largest global gathering for geospatial software.

FOSS4G brings together developers, users, decision-makers and observers from a broad spectrum of organizations and fields of operation. Through seven days of workshops, presentations, discussions, and cooperation, FOSS4G participants create effective and relevant geospatial products, standards, and protocols.

Conference attendees are

* Developers and Users of GeoSpatial Software
* Technical Leaders
* Private Companies
* National and International Organizations, both Governmental and NGO
* Teachers and Education Professionals
* Scientific Communities and Researchers

and the talks cover topics such as:

* Software status, new software/project development, benchmarking
* FOSS4G implementations in land management, crisis/disaster response, smart cities, population mapping, climate change, ocean and marine monitoring, etc.
* Data visualization: spatial analysis, manipulation and visualization of geospatial data
* Data collection, data sharing, data science, open data, big data, data exploitation platforms
* Sensors, remote sensing, laser-scanning, structure from motion
* New trends: IoT, Indoor mapping, drones, AI
* Open and reproducible science
* Standards, interoperability, SDIs
* Community & participatory FOSS4G
* FOSS4G at governmental institutions
* FOSS4G in education
* Business products powered by FOSS4G
