---
title: Web Forums
date: 2022-02-01
weight: 80
description: "Web discussion and Q&A boards are hosting more and more PostGIS conversations."
geekdocHidden: false
---

## GIS Stack Exchange

* [gis.stackexchange.com](https://gis.stackexchange.com/tags/postgis) holds a huge amount of practical PostGIS and PostgreSQL content. It is almost always worthwhile to search for your question here first.


## Reddit

There are relevant communities on Reddit, also!

* [PostGIS Reddit](https://www.reddit.com/r/postgis/)
* [PostgreSQL Reddit](https://www.reddit.com/r/PostgreSQL/)
* [GIS Reddit](https://www.reddit.com/r/gis/)
