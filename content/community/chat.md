---
title: Chat
date: 2022-02-01
weight: 40
description: "Real-time chat online is a growing source of community engagement."
geekdocHidden: false
---

There are a **large number** of online chat spaces devoted to PostGIS topics.

## Matrix

The [Matrix](https://matrix.org/) federated chat network includes a [#postgis:osgeo.org](https://matrix.to/#/#postgis:osgeo.org) channel for PostGIS discussion. Choose a matrix client and start chatting.


## Slack

There are a number of Slack communities that include PostGIS channels.

* [PostgreSQL](https://postgresteam.slack.com/)
* [OSGeo](https://osgeo.slack.com/)
* [Spatial Community](https://thespatialcommunity.slack.com)


## Discord

[PostGIS](https://discord.com/channels/710918545906597938/915988834414653520) channel on "PostgreSQL, People & Data" server. There is also a "GIS" server but no specific "PostGIS" channel there so far.


## IRC

`#postgis` channel on the [Libera.chat](https://libera.chat/) network. Install an IRC client or use the [web client](https://web.libera.chat/).
