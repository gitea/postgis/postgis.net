---
title: Mailing Lists
date: 2022-02-01
weight: 20
description: "The beating heart of the PostGIS community is the mailing lists."
geekdocHidden: false
---

## postgis-users

The [postgis-users](http://lists.osgeo.org/mailman/listinfo/postgis-users) mailing list is a supportive community forum to discuss issues, to learn new techniques, and to share your knowledge with other learners.

* Join the [mailing list](http://lists.osgeo.org/mailman/listinfo/postgis-users).
* Read the list [archives](http://lists.osgeo.org/pipermail/postgis-users).


## postgis-devel

The [postgis-devel](http://lists.osgeo.org/mailman/listinfo/postgis-devel) list is a place for discussions about the implementation and maintenance of features in PostGIS. If you want to get involved in PostGIS develompent, or just understand the development process, this is the list for you.

* Join the [mailing list](http://lists.osgeo.org/mailman/listinfo/postgis-devel).
* Read the list [archives](http://lists.osgeo.org/pipermail/postgis-devel).
