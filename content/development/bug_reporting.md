---
title: Bug Reporting
date: 2022-02-01
weight: 60
description: "The right way to report a bug so that it can be fixed."
geekdocHidden: false
---

## Steps to Report a Bug

* **Stop**, and consider if you have actually found a *defect*, or if you simply have a question you need answered. If you have a *question*:
    * Join the [user mailing list]({{< ref "/community/mailinglists" >}}) and ask your question there.
    * Go to one of the other [community resources]({{< ref "/community/" >}}) and ask your question there.

* Otherwise, **sign in** to the [PostGIS ticket tracker](https://trac.osgeo.org/postgis/) with your [OSGeo userid](https://www.osgeo.org/community/getting-started-osgeo/osgeo_userid/).

* **Search** to find if your problem has already been reported.
    * Add any extra context you might have found, or at least indicate that you too are having the problem. This will help us prioritize common issues.

* **Simplify** your issue, to make it very very easy for others to reproduce it.
    * The best reports are just a *single line of SQL* that demonstrates the problem.
    * If another person cannot easily replicate your issue, it will likely not get addressed.

* **Test** your issue, if you can, across other versions of PostGIS.
    * Do newer versions also have the issue?
    * Do older versions?

* [**Create** a new ticket](https://trac.osgeo.org/postgis/newticket) for your issue.
    * Include your steps and SQL to reproduce the issue.
    * Include your version information:
      ```postgres
      SELECT version();
      SELECT postgis_full_version();
      ```
<br/>

## Security Issues

If you have discovered a security issue that should not be recorded in the public ticket track please do the following:

* **Follow** the steps above to verify and characterize the issue you have discovered.

* **E-Mail** the information about your issue to security@postgis.net, following the [Security Policies and Procedures](https://git.osgeo.org/gitea/postgis/postgis/src/branch/master/SECURITY.md).
