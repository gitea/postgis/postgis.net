---
title: Versioning & EOL
date: 2022-02-01
description: "Project policy on the meaning of version numbers, releases and when to end development."
weight: 120
geekdocHidden: false
---

The following are general guidance and expectations, not hard-and-fast rules.


## Versioning

PostGIS uses "[semantic versioning](https://semver.org/)" with a **MAJOR.MINOR.PATCH** version number.

* **MAJOR** updates may include backward incompatibility and require special procedures for upgrade. Functions and parameters may be deleted or renamed.
* **MINOR** updates may introduce new functionality, but will not (substantially) alter existing functionality or remove functionality.
* **PATCH** updates will repair defects but not add, remove or change functionality.

The functionality of any given version of PostGIS will depend to an extent on the underlying functionality of the libraries PostGIS uses.

* Each minor version of PostGIS may introduce features that are only exposed on the latest versions of PostgreSQL, Proj, GEOS, or GDAL. 
* When PostGIS is compiled against earlier versions of GEOS, GDAL, and Proj, some features that depend on new library enhancements will be disabled.


## Releases

* PostGIS releases new **minor** versions **approximately once a year**, shortly before PostgreSQL releases their latest version which is currently around early October.
* PostGIS releases new **patch** versions **as necessary** when the number of fixes or the severity of open issues calls for them.


## End-of-Life (EOL)

The PostGIS project strives to support each minor version of PostGIS for **2-4 years** after initial release and at the very least until the lowest PostgreSQL version supported by the PostGIS minor version is EOL'd.

Support for PostgreSQL releases generally runs for five years. Refer to [PostgreSQL EOL policy](https://www.postgresql.org/support/versioning/) and our [compatibility and support matrix](https://trac.osgeo.org/postgis/wiki/UsersWikiPostgreSQLPostGIS) for information on the compatibility of specific version combinations. 
