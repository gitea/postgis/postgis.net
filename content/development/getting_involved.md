---
title: Getting Involved
date: 2024-03-13
weight: 40
description: "Some tips for how to get started with contributing code and documentation to the project."
geekdocHidden: false
---

## Dream Small

While it may be tempting to think of the most audacious improvement you could make to PostGIS, and try to tackle that, working on an open source project is a **collective endeavor**, and so the first thing you need to build is **not software**, it is **trust**.

* Contributors with a history of constructive work with other members of the community get more of a hearing than folks with a grand plan.
* The way to build up a history is to choose very small pieces of improvement, and move them through the process of acceptance.
* Make small enhancements, to code and documentation, and get a feel for what improvements people are looking for.
* Communicate your interest in contributing, and discuss your plans on the [postgis-devel](http://lists.osgeo.org/mailman/listinfo/postgis-devel) list before investing a great deal of time.


## Learn the Basics

Before you can contribute anything, you need to be able to build the software and documentation.

* Consider installing PostGIS and its dependencies ([GDAL](https://gdal.org), [GEOS](https://libgeos.org), [Proj](https://proj.org)) from source. This will allow you to debug deeply into whatever you are working on.
* Install the documentation dependencies tools (docbook-xsl, xsltproc).

## Submitting Patches

If you've read all the above, and are ready to submit a patch, do the following:

1.  Create a [new ticket](https://trac.osgeo.org/postgis/newticket) on our ticket tracker 
    or find an existing ticket you would like to submit a fix for. Although we will accept new features via pull requests without a ticket, we prefer a ticket in place.
2.  In the ticket detail the new feature purpose, and attach a patch or alternatively a link to 
    a pull request on one of the postgis [git mirrors or primary repo](../source_code/#source-code-repository).

If your feature involves a new function/ feature, we expect documentation to accompany this.
Details on how to prepare documentation are on [Submitting documentation for a new feature](https://trac.osgeo.org/postgis/wiki/DevWikiDocNewFeature)


