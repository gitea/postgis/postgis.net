---
title: Source Code
date: 2022-02-01
weight: 20
geekdocHidden: false
description: "Where to find the source repository and released source code"
---

## Current Releases

The current source code releases are:

{{< sourcelinks release>}}

Older releases are available directly from the [source download directory]({{< loc "postgis.release_source">}}). Refer to our [version and end-of-life policy]({{< relref "versions_eol" >}}) for details on code life cycle. Logos are available in the [logo directory](https://download.osgeo.org/postgis/logo_suite/).


## Source Code Repository

The **official** PostGIS GIT repository is hosted at OSGeo.

* **https://git.osgeo.org/gitea/postgis/**

Changes pushed to the official repository will be replicated to the two mirror repository sites. Changes committed to the mirror sites will be over-written the next time a changes are synced from OSGeo.

* GitHub mirror: https://github.com/postgis/postgis/
* GitLab mirror: https://gitlab.com/postgis/postgis/
