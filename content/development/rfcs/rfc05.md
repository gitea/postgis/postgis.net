---
title: "RFC-5: PostGIS Core Contributor Guidelines"
author: Regina Obe
version: 2.0
category: rfcs
date: "2022-03-21"
draft: false
type: rfc
relatedInSidebar: false
singlecolumn: true
sidebar: false
geekdocHidden: true
geekdocHiddenTocTree: false
---

# Changes to this file
This RFC should be only changed by PostGIS PSC members. All PSC members must agree to the terms of the changes before the change is considered final except in the case of wording changes and formatting that do not alter the terms of the guidelines.

# Purpose
To formalize source tree access, and specify some guidelines for primary source committers and patch submitters.

# Election to Core Contributors Group
 Permission for write source tree access to our primary repo (​https://git.osgeo.org/gitea/postgis/postgis.git) shall be provided to contributors only if accepted by the PostGIS Project Steering Committee. A proposal should be written to the PSC for new contributors and voted on normally. It is not necessary to write an RFC document for these votes, a proposal to postgis-dev is sufficient. After approval, the approved contributor shall be given write access to the PostGIS source repo and also added to the documentation under the section ​Core Contributors Present

Removal of write access should be handled by the same process.

The contributor should have demonstrated commitment to PostGIS and knowledge of the PostGIS source code and processes to the committee's satisfaction, usually by reporting bugs, submitting patches, and/or actively participating in the PostGIS mailing list(s).

The contributor should also be prepared to support any new feature or changes in future releases that the contributor commits to the PostGIS source tree, or to find someone to which to delegate responsibility to should the contributor stop being available to support the portions of code the contributor is responsible for.

All core contributors should also be a member of the postgis-dev mailing list so they can stay informed on policies, technical developments and release preparation.

New core contributors are responsible for having read, and understood this document.

# Code and Documentation Conventions

* C code should follow our designated ​Style Guidelines to the best of your abilities
* To accomplish Guidelines standards, we have implemented the use of ​editorconfig. Our repo as of version 2.4 now contains a .editorconfig file which editors configured with editorconfig support abide by.
* We ask all developers to work with an editor that has editorconfig support. Many editors have it or have it as plugins.
* All source code in GIT should be in Unix text format as opposed to DOS text mode with possible exception of .bat files.
* Each new/changed function should be documented in the official docs following our [Documentation Guidelines](https://trac.osgeo.org/postgis/wiki/DevWikiDocNewFeature)

# Repo Administrator
One member of the Project Steering Committee will be designated as the Repo Administrator. That person will be responsible for giving write access to folks, updating the CREDITS, and other repo related management.

Paul Ramsey is currently designated as the Repo Administrator.

# Commit and Bug, Feature Tracking Practices

The following are considered good source commit and tracking practices for the PostGIS project.

  1. Use meaningful descriptions for commit log entries. Format the git log to have a succint oneline description optionally followed by a detailed description after an empty line. Keep lines within 70 columns. Include trac ticket references in the detailed description. Ticket references will be signaled to trac so they can close tickets if "Closes #xxx" is found in the commit log or only result in comments if "References #xxx" is found. References to pull requests are better encoded as full links.
  2. We allow contributions via patches (on trac or via mailing lists) or pull requests from our primary or [CodeMirrors git mirrors](../../source_code).  If you are given a patch or pull request from a community person, make sure to keep their names in the commit log (`git commit --author "Name Surname <email>"`) and NEWS file. DO NOT USE the `MERGE` button on [CodeMirrors git mirrors](../../source_code) as mirrors will be overridden and the merged commit will be discarded; merge manually to official repo instead.
  3. We use https://trac.osgeo.org/postgis for planning so make sure pending changes have tickets there so they are not overlooked when cutting releases. If you forget to add trac ticket references in a commit log, edit the trac ticket and put the notes in, such as "Fixed in master ..git hash and in branches/3.0 <githas>". The 'hash/git' enables Trac to create a hyperlink from the ticket to the changeset.
  4. Make sure the NEWS file is updated in master branch for new features/breaking changes and in all the stable branches for bugfixes.
  5. Never commit new features to a stable branch without permission of the PSC or release manager. Normally only fixes should go into stable branches.
  6. New features go in the master branch.
  7. Only bug fixes should be committed to the master branch during pre-release code freeze, without permission from the PSC or release manager.
  8. Significant changes to the master branch, in particular breaking backward-compatibility, should be discussed on the postgis-dev list before you make them, and larger changes will require an RFC approved by the PSC.
  9. As a general rule, do not create new branches in the official repository. An exception to this rule is for when you want all CI bots to test your changes before pushing them to the target branch, in which case prefix your branch with a `test/` string.
  10. When committing new features or significant changes to existing source code, the committer should take reasonable measures to insure that the source code continues to build and work on the most commonly supported platforms (currently Linux, FreeBSD, Mac, and Windows), either by testing on those platforms directly, running CI tests, or by getting help from other developers working on those platforms. If new files or library dependencies are added, then the configure.in, Makefile.in and related documentations should be kept up to date.
  11. After each commit, verify that all the build bots are happy on this page - https://trac.osgeo.org/postgis/ and if their unhappiness is caused by your commit, fix the issue before making any other changes.


# Committer Tracking

A list of all core contributors is listed in the manual under the section "Core Contributors Present" in file doc/introduction.xml. This will be the responsibility of the PSC to keep up to date:

* Full name: the users actual name.
* A brief indication of areas of responsibility.

This will be the responsibility of the documentation lead (currently Regina Obe) to manage. All stable non-EOL'd documents should list this information for core contributors.

# Projects under PostGIS umbrella

The PostGIS project has 3 additional sub-projects under it's umbrella.  These projects may be impacted by changes in the core PostGIS project and have their own community of developers.

Current list of these are:

* Docker PostGIS - (https://github.com/postgis/docker-postgis) community managed docker builds of PostGIS.
* PostGIS Java - ( https://github.com/postgis/postgis-java ) Java bindings for PostGIS
* PostGIS Workshop (https://github.com/postgis/postgis-workshops) - Workshop material for PostGIS

# Relationship with other Projects

Some parts of the PostGIS code base are dependent on other upsteam projects or other projects rely heavily on functionality in PostGIS. Changes in those areas should go first into those upstream projects and then applied to PostGIS. In event of major changes to PostGIS, said projects should be regression tested (before a PostGIS release) to ensure the latest version still works with the latest RTM version of PostGIS.

Currently the list of those areas is :

* postgresql ( https://www.postgresql.org)
* geos ( https://libgeos.org)
* proj ( https://proj.org)
* gdal ( https://gdal.org)
* sfcgal (https://sfcgal.org)

GIS FOSS suites that may be impacted by PostGIS major release:
* mapserver (https://mapserver.org)
* geoserver (https://geoserver.org)
* openjump (http://openjump.org)
* qgis (https://qgis.org)
* gvSig (https://www.gvsig.org)
* pgRouting (https://www.pgrouting.org/)
* mobilitydb (https://mobilitydb.com/)
* pgpointcloud (https://github.com/pgpointcloud)
* osm2pgsql + other openstreetmap components (mapnik etc) (https://www.openstreetmap.org/)


# Legal

Core contributors are the front line gatekeepers to keep the code base clear of improperly contributed code. It is important to the PostGIS users, developers and the OSGeo foundation to avoid contributing any code to the project without it being clearly licensed under the project license.

Generally speaking the key issues are that those providing code to be included in the repository understand that the code will be released under the original GPL license, and that the person providing the code has the right to contribute the code. The core contributor that accepts and committs the code should verify the understanding of the contributor unless the committer is very comfortable that the contributor understands the license (for instance frequent contributors).

If the contribution was developed on behalf of an employer (on work time, as part of a work project, etc) then it is important that an appropriate representative of the employer understand that the code will be contributed under the GPL license. The arrangement should be cleared with an authorized supervisor/manager, etc.

The code should be developed by the contributor, or the code should be from a source which can be rightfully contributed such as from the public domain, or from an open source project under a compatible license.

All unusual situations need to be discussed and/or documented.

Core contributors should adhere to the following guidelines, and may be personally legally liable for improperly contributing code to the source repository:

* Make sure the contributor (and possibly employer) is aware of the contribution terms.
* Code coming from a source other than the contributor (such as adapted from another project) should be clearly marked as to the original source, copyright holders, license terms and so forth. This information can be in the file headers, but should also be added to the project licensing file if not exactly matching normal project licensing.
* Existing copyright headers and license text should never be stripped from a file. If a copyright holder wishes to give up copyright they must do so in writing to the OSGeo foundation before copyright messages are removed. If license terms are changed it has to be by agreement (written in email is ok) of the copyright holders.
* Code with licenses requiring credit, or disclosure to users should be added to LICENSE.TXT.
* When substantial contributions are added to a file (such as substantial patches) the author/contributor should be added to the list of copyright holders for the file.
* If there is uncertainty about whether a change is proper to contribute to the code base, please seek more information from the project steering committee, or the foundation legal counsel.
* New contributors and company contributors should be added to the credits in doc/introduction.xml of the latest release of the PostGIS manual.
* It is the responsibility of the document lead to ensure when new enhancements are added or breaking changes are made, these are noted in the relevant branch/NEWS as soon as conveniently possible. The note should include the trac # (unless a major feature with many tickets) and contributors to the feature/change.

# Bootstrapping

The following existing committers will be considered authorized PostGIS committers as long as they each review the committer guidelines, and agree to adhere to them. The administrator will be responsible for checking with each person. Current committers are listed in alphabetical order with (P) to denote current Project Steering Committee members
**TODO: this might have been the boot strap but not current developer list not sure we even need a bootstrap here **

* Jorge Arévalo
* Nicklas Avén
* Mark Cave-Ayland (P)
* Olivier Courtin
* Chris Hodgson (P)
* Mark Leslie
* Mateusz Loskot
* Kevin Neufeld
* Regina Obe  (P) Documentation Lead
* Bborie Park (P)
* Pierre Racine
* Paul Ramsey (P) SVN Administrator
* Sandro Santilli (P)
* David Zwarg
