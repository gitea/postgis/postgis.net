---
title: Home
weight: 20
layout: single
---

PostGIS extends the capabilities of the [PostgreSQL](https://postgresql.org) relational database by adding support for storing, indexing, and querying geospatial data.

PostGIS features include:

* **Spatial Data Storage**: Store different types of spatial data such as points, lines, polygons, and multi-geometries, in both 2D and [3D](docs/PostGIS_Special_Functions_Index.html#PostGIS_3D_Functions) data.
* **Spatial Indexing**: Quickly [search](docs/using_postgis_dbmanagement.html#build-indexes) and retrieve spatial data based on its location.
* **Spatial Functions**: A wide range of spatial functions that allow you to filter and analyze spatial data, measuring [distances](docs/ST_Distance.html) and [areas](docs/ST_Area.html), [intersecting](docs/ST_Intersection.html) geometries, [buffering](docs/ST_Buffer.html), and more.
* **Geometry Processing**: Tools for [processing](docs/ST_Union.html) and [manipulating](docs/ST_Translate.html) geometry data, such as [simplification](docs/ST_Simplify.html), [conversion](docs/ST_AsMVT.html), and generalization.
* **Raster Data Support**: Storage and processing of [raster data](docs/RT_reference.html), such as elevation data and weather data.
* **Geocoding and Reverse Geocoding**: Functions for [geocoding](docs/postgis_installation.html#loading_extras_tiger_geocoder) and reverse geocoding.
* **Integration**: Access and work with PostGIS using third party tools such as [QGIS](https://qgis.org), [GeoServer](https://geoserver.org), [MapServer](https://mapserver.org), ArcGIS, Tableau.
