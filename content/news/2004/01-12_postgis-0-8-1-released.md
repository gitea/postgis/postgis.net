---
layout: post
category: news
title: PostGIS 0.8.1 Released
author: Paul Ramsey
tags: [release, 0.8]
date: "2004-01-12"
---

<p>
PostGIS <A HREF='{{< loc "postgis.release_source">}}/postgis-0.8.1.tar.gz'>0.8.1</A> is a bug fix release with some fixes to memory handling and the
interface with <A HREF='https://libgeos.org/'>GEOS</A>.
</p>
