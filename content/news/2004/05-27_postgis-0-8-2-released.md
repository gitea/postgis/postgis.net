---
layout: post
category: news
title: PostGIS 0.8.2 Released
author: Paul Ramsey
tags: [release, 0.8]
date: "2004-05-27"
---

<p>
PostGIS <A HREF='{{< loc "postgis.release_source">}}/postgis-0.8.2.tar.gz'>0.8.2</A> is an important maintainance
release, including the following new features:
<ul>
<li>PostgreSQL 7.5 Support</li>
<li>PostgreSQL 7.5 Statistics Integration</li>
<li>Optional Experimental Light-weight Geometry (LWGEOM)</li>
<li>Faster queries</li>
<li>Smaller databases</li>
<li>Four dimensional geometry</li>
<li>New functions</li>
</ul>
<p>
The release also includes numerous small bug fixes, for different platforms
and high load use cases.
</p>
