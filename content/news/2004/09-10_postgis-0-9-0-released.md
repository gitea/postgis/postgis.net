---
layout: post
category: news
title: PostGIS 0.9.0 Released
author: Paul Ramsey
tags: [release, 0.9]
date: "2004-09-10"
---

<p>
PostGIS <A HREF='{{< loc "postgis.release_source">}}/postgis-0.9.0.tar.gz'>0.9.0</A> is an important maintainance
release, including the following new features:
</p>
<ul>
<li>PostgreSQL 8.0 Support</li>
<li>PostgreSQL 8.0 Statistics Integration </li>
<li>VACUUM ANALYZE now automatically updates spatial selectivity stats</li>
<li>PostgreSQL 8.0 Windows Support</li>
<li>GEOS 2.0 Support</li>
<li>GEOS 2.0 includes important enhancements to Buffer(), Intersection() and Union()</li>
<li>AsSVG() Function</li>
<li>Improved spatial index estimation</li>
<li>Cleaner build system</li>
</ul>
<p>
The release also includes numerous small bug fixes.
</p>
