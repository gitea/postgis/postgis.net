---
title: PostGIS 3.3.0 Released
layout: post
category: news
tags: [release,3.3]
author: Regina Obe
date: "2022-08-27"
thumbnail:
---
The PostGIS Team is pleased to release PostGIS 3.3.0.

<!--more-->

This release would not be possible without the various developers listed in the credits as well as the companies that provided funding and developer time.

Companies that contributed significantly to this release are:

* [Crunchy Data](https://www.crunchydata.com/) Geometry processing functions and improvements to [GEOS](https://libgeos.org)
* [Kontur](https://www.kontur.io/) Speed improvements to raster, MVT, GiST indexing, better usability of geometry processing.
* [NetLab](https://strk.kbt.io/services.html) Continuous improvement of PostGIS topology, PostGIS upgrade plumbing,  PostGIS test plumbing, and CI bot management
* [NIBIO](https://nibio.no) Developer funding for topology improvements
* [Oslandia](https://oslandia.com) Developer funding for SFCGAL improvements
* [Paragon Corporation](https://www.paragoncorporation.com) -- General CI bot management, release management, alignment of PostGIS with PostgreSQL versions.


Best served with [PostgreSQL 15](https://www.postgresql.org/docs/15/release.html). This version of PostGIS can utilize the faster GiST building support API introduced in PostgreSQL 15.
 If compiled with recently released [GEOS 3.11.0](https://lists.osgeo.org/pipermail/geos-devel/2022-July/010728.html) you can take advantage of new functions added and improvements to some existing functions.  This release also includes many additional functions and improvements for `postgis`, `postgis_raster` , `postgis_sfcgal`, and `postgis_topology` extensions.

### 3.3.0

This release supports PostgreSQL 11 and higher, GEOS 3.6 and higher, and proj 5.2 and higher.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.0.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.0/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.3.0.pdf) [de]({{< loc "postgis.release_docs">}}/postgis-3.3.0-de.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.3.0.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.3/) [de]({{< loc "site.root">}}docs/manual-3.3/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-3.3/postgis-ja.html) [ko_KR]({{< loc "site.root">}}docs/manual-3.3/postgis-ko_KR.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.3.0.epub)

Details of this release can be found in NEWS file:
[https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.3.0/NEWS](https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.3.0/NEWS)
and is also included in the source tar ball.

If you come across any issues, feel free to report via our
[ticket tracker](https://trac.osgeo.org/postgis)  or mailing list with details as described in [SUPPORT](https://postgis.net/support/)


View all [closed tickets for 3.3.0][1].

After installing the binaries or after running pg_upgrade:

For upgrading from PostGIS 3.0, do the following which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.3.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
