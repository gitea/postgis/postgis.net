---
title: PostGIS 3.3.0beta1
layout: post
category: news
tags: [release, 3.3]
author: Regina Obe
thumbnail:
date: "2022-07-03"
---

The PostGIS Team is pleased to release PostGIS 3.3.0beta1!
Best Served with [PostgreSQL 15 Beta2](https://www.postgresql.org/about/news/postgresql-15-beta-2-released-2479/)
and [GEOS 3.11.0](https://libgeos.org/posts/2022-07-01-geos-3-11-0-released/).

This release supports PostgreSQL 11-15.

### 3.3.0beta1

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.0beta1.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.3.0beta1.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.0beta1/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.3.0beta1.pdf)


This release is a beta of a major release, it includes bug fixes since PostGIS 3.2.1 and new features.

<!--more-->

Changes since PostGIS 3.3.0alpha1

### Enhancements
- [#5158](https://trac.osgeo.org/postgis/ticket/5158), pgtopo_import / pgtopo_export manpages (Sandro Santilli)
- [#5170](https://trac.osgeo.org/postgis/ticket/5170), add a optional max_rows_per_copy to -Y option to raster2pgsql to
  control number of rows per copy statement.
  Default to 50 when not specified (Regina Obe)
- [#4939](https://trac.osgeo.org/postgis/ticket/4939), [#5161](https://trac.osgeo.org/postgis/ticket/5161), ST_LineMerge now has option to keep the directions of input linestrings,
  useful when processing road graphs. Requires GEOS 3.11. (Sergei Shoulbakov)
- ST_ConcaveHull GEOS 3.11+ polygon-respecting native implementation (Paul Ramsey, Martin Davis)
- [#5039](https://trac.osgeo.org/postgis/ticket/5039), postgis_tiger_geocoder TIGER 2021 (Regina Obe)

### New features
- [#5169](https://trac.osgeo.org/postgis/ticket/5169), ST_SimplifyPolygonHull (requires GEOS 3.11)
  (Paul Ramsey, Martin Davis)
- [#5162](https://trac.osgeo.org/postgis/ticket/5162), ST_TriangulatePolygon with GEOS 3.11+
  (Paul Ramsey, Martin Davis)

### Bug fixes
- [#5173](https://trac.osgeo.org/postgis/ticket/5173) st_asflatgeobuf detoast crash (Paul Ramsey)
- [#4932](https://trac.osgeo.org/postgis/ticket/4932), Bug with geography ST_Intersects / ST_Distance (Paul Ramsey)
- [#5114](https://trac.osgeo.org/postgis/ticket/5114), pgsql2shp segfault with long or many truncated columns


### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.3.0beta1 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.3.0&milestone=PostGIS+3.2.2&milestone=PostGIS+3.2.1&milestone=PostGIS+3.1.6
