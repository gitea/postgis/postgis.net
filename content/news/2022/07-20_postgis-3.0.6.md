---
title: PostGIS 3.0.6
layout: post
category: news
tags: [release, 3.0]
author: Regina Obe
date: "2022-07-20"
thumbnail:
---

The PostGIS Team is pleased to release PostGIS 3.0.6.
This release works for PostgreSQL 9.5-13.

### 3.0.6

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.6.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.0.6.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.6/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.0.6.pdf)

<!--more-->

This release is a bug fix release, addressing issues found in the previous 3.0 releases.

  - [#4835](https://trac.osgeo.org/postgis/ticket/4835), Occasional distance errors in polar area (Paul Ramsey)
  - [#5152](https://trac.osgeo.org/postgis/ticket/5152), Fix infinite loop with ST_Split (Sandro Santilli)
  - [#5087](https://trac.osgeo.org/postgis/ticket/5087), Prevent configure for PostgreSQL >= 14
           and note in docs, PostgreSQL > 13 major
           is not supported (Regina Obe)
  - [#5115](https://trac.osgeo.org/postgis/ticket/5115), Allow dropping topologies with pending constraints (Sandro Santilli)
  - [#5120](https://trac.osgeo.org/postgis/ticket/5120), Fix not-null result from ST_EstimatedExtent against
           truncated tables with spatial index (Sandro Santilli)
	- [#5151](https://trac.osgeo.org/postgis/ticket/5151), garden crash ST_SetPoint with empty geometries (Regina Obe)
  - [#5150](https://trac.osgeo.org/postgis/ticket/5150), Change signature of AddToSearchPath (Regina Obe)
  - [#5155](https://trac.osgeo.org/postgis/ticket/5155), More schema qual fixes (Regina Obe)
  - [#5154](https://trac.osgeo.org/postgis/ticket/5154), ST_Value is undercosted (Regina Obe)


<!--more-->

### Upgrading

After installing the binaries or after running pg_upgrade:

For upgrading from PostGIS 3.0, do the following which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.0.6 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.0.6&&milestone=PostGIS+2.5.7