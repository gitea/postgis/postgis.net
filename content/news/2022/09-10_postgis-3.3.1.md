---
title: PostGIS 3.3.1
layout: post
category: news
tags: [release,3.3]
author: Regina Obe
date: "2022-09-10"
thumbnail:
---
The PostGIS Team is pleased to release PostGIS 3.3.1.

This is a bug fix release to address an issue compiling against PostgreSQL 15 Beta 4.

Best served with [PostgreSQL 15 Beta 4](https://www.postgresql.org/about/news/postgresql-15-beta-4-released-2507/).

<!--more-->

### 3.3.1

This release supports PostgreSQL 11 and higher, GEOS 3.6 and higher, and proj 5.2 and higher.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.1/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.3.1.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.3.1.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.3/) [de]({{< loc "site.root">}}docs/manual-3.3/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-3.3/postgis-ja.html) [ko_KR]({{< loc "site.root">}}docs/manual-3.3/postgis-ko_KR.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.3.1.epub)

### Bug Fixes
- [#5227](https://trac.osgeo.org/postgis/ticket/5227), typo in ST_LineLocatePoint error message (Sandro Santilli)
- [#5231](https://trac.osgeo.org/postgis/ticket/5231), PG15 no longer compiles because SQL/JSON removed PG upstream (Regina Obe)


Details of this release can be found in NEWS file:
[https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.3.1/NEWS](https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.3.1/NEWS)
and is also included in the source tar ball.

If you come across any issues, feel free to report via our
[ticket tracker](https://trac.osgeo.org/postgis)  or mailing list with details as described in [SUPPORT](https://postgis.net/support/)

View all [closed tickets for 3.3.1][1].

After installing the binaries or after running pg_upgrade:

For upgrading from PostGIS 3.0, do the following which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.3.1&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
