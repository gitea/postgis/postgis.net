---
title: PostGIS 3.2.1
layout: post
category: news
tags: [release, 3.2]
author: Regina Obe
thumbnail:
date: "2022-02-12"
---

The PostGIS Team is pleased to release PostGIS 3.2.1!

### 3.2.1

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.1.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.2.1.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.1/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.2.1.pdf)


This release is a bug fix release, addressing issues found in the previous 3.1 releases.

<!--more-->

### Bug Fixes

- [#5033](https://trac.osgeo.org/postgis/ticket/5033), [#5035](https://trac.osgeo.org/postgis/ticket/5035), allow upgrades in presence of views using deprecated
					functions (Sandro Santilli)
- [#5046](https://trac.osgeo.org/postgis/ticket/5046), Fix upgrades in absence of old library (Sandro Santilli)
- [#5063](https://trac.osgeo.org/postgis/ticket/5063), Fix ST_Srid(TopoGeometry) against a set (Sandro Santilli)
- [#3056](https://trac.osgeo.org/postgis/ticket/3056), spurious notice on ST_StartPoint(empty) (Paul Ramsey)
- [#5041](https://trac.osgeo.org/postgis/ticket/5041), postgis_tiger_geocoder: loader_generate_script
					generates script with invalid syntax (Regna Obe)
- [#5076](https://trac.osgeo.org/postgis/ticket/5076), stop install when pgaudit is active (Paul Ramsey)
- [#5069](https://trac.osgeo.org/postgis/ticket/5069), search_path vulnerabilty during install/upgrade
					(Regina Obe)
- [#5090](https://trac.osgeo.org/postgis/ticket/5090), [#5091](https://trac.osgeo.org/postgis/ticket/5091) configure --with-protobuf-lib / --with-protobufdir
					, --without-protobuf broken (Tobias Bussmann)

### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.2.1 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.2.1&milestone=PostGIS+3.1.5&milestone=PostGIS+3.0.5
