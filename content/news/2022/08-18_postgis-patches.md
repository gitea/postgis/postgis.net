---
title: PostGIS 3.2.3, 3.1.7, 3.0.7, 2.5.8 Released
slug: "postgis-patches"
layout: post
category: news
tags: [release,3.2,3.1,3.0,2.5]
author: Regina Obe
date: "2022-08-18"
thumbnail:
---

The PostGIS development team is pleased to provide
bug fix and performance enhancements
 3.2.3, 3.1.7, 3.0.7, 2.5.8  for the 3.2, 3.1, 3.0, 2.5 stable branches.

<!--more-->

Most importantly in these releases is a fix for upgrading PostGIS
for recently released PostgreSQL 14.5, 13.8, 12.12, 11.17, 10.22.

### 3.2.3

This release works with PostgreSQL 9.6-15  and GEOS >= 3.6.
Additional features exposed for GEOS 3.10+, PostgreSQL 12+ and Proj 6+

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.3.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.3/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.2.3.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.2.3.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.2.3.epub)

### 3.1.7

This release works with PostgreSQL 9.6-14  and GEOS >= 3.6
Additional features exposed for GEOS 3.9+, PostgreSQL 12+ and Proj 6+

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.7.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.7/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.7.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.1.7.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.1.7.epub)

### 3.0.7

This release works with PostgreSQL 9.5-13beta2  and GEOS >= 3.6
Designed to take advantage of features in PostgreSQL 12+ and Proj 6+

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.7.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.7/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.7.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.7.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.7.epub)

### 2.5.8

This release supports PostgreSQL 9.3-12
You are encouraged to use the PostGIS 3 or higher with PostgreSQL 12 instead of 2.5
because 3.0 has features specifically designed to take
advantage of features new in PostgreSQL 12.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.8.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.8/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.5.8.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.5.8.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-2.5) [de]({{< loc "site.root">}}docs/manual-2.5/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-2.5/postgis-ja.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.5.8.epub)


View all [closed tickets for 3.2.3, 3.1.7, 3.0.7, 2.5.8][1].

After installing the binaries or after running pg_upgrade:

For PostGIS 3 and above:
```postgres
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

For PostGIS < 3:
```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
```

[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.2.3&milestone=PostGIS+3.1.7&milestone=PostGIS+3.0.7&milestone=PostGIS+2.5.8&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
