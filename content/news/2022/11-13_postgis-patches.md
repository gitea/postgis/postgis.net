---
title: PostGIS 3.3.2, 3.2.4, 3.1.8, 3.0.8 Patch Releases
layout: post
category: news
tags: [release,3.3,3.2,3.1,3.0]
author: Regina Obe
thumbnail:
date: "2022-11-13"
---

The PostGIS development team is pleased to provide
security, bug fixes and performance enhancements
3.3.2, 3.2.4, 3.1.8 and 3.0.8 for the 3.3, 3.2, 3.1, and 3.0 stable branches.

<!--more-->
### 3.3.2

This release supports PostgreSQL 11-15, GEOS 3.6 and higher, and proj 5.2 and higher.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.2.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.2/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.3.2.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.3.2.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.3/) [de]({{< loc "site.root">}}docs/manual-3.3/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-3.3/postgis-ja.html) [ko_KR]({{< loc "site.root">}}docs/manual-3.3/postgis-ko_KR.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.3.2.epub)

### 3.2.4

This release works for PostgreSQL 9.6-15.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.4.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.2.4.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.4/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.2.4.pdf)

### 3.1.8

This release supports PostgreSQL 9.6-14.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.8.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.8/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.8.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.1.8.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.1)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.1.8.epub)

### 3.0.8

This release works with PostgreSQL 9.5-13  and GEOS >= 3.6
Designed to take advantage of features in PostgreSQL 12+ and Proj 6+

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.8.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.8/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.8.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.8.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.8.epub)

View all [closed tickets for  3.1.8, 3.0.8][1].

After installing the binaries or after running pg_upgrade:

For PostGIS 3.3, 3.2, 3.1, 3.0, 2.5 do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.4 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.1.8&milestone=PostGIS+3.0.8&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
