---
title: PostGIS 3.2.2
layout: post
category: news
tags: [release, 3.2]
author: Regina Obe
thumbnail:
date: "2022-07-23"
---

The PostGIS Team is pleased to release PostGIS 3.2.2!
This release works for PostgreSQL 9.6-15.

### 3.2.2

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.2.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.2.2.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.2/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.2.2.pdf)


This release is a bug fix release, addressing issues found in the previous 3.2 releases.

<!--more-->

### Bug Fixes

  - [#5182](https://trac.osgeo.org/postgis/ticket/5182), Fix loading topology.sql in new database (Sandro Santilli)
  - [#4835](https://trac.osgeo.org/postgis/ticket/4835), Occasional distance errors in polar area (Paul Ramsey)
  - [#5124](https://trac.osgeo.org/postgis/ticket/5124), GRANT USAGE on topology schema to PUBLIC (Sandro Santilli)
  - [#5120](https://trac.osgeo.org/postgis/ticket/5120), Fix not-null result from ST_EstimatedExtent against
           truncated tables with spatial index (Sandro Santilli)
  - [#5115](https://trac.osgeo.org/postgis/ticket/5115), Allow dropping topologies with pending constraints
           (Sandro Santilli)
  - [#5105](https://trac.osgeo.org/postgis/ticket/5105), Fix false invalidity report from ValidateTopology
           (Sandro Santilli)
  - [#5039](https://trac.osgeo.org/postgis/ticket/5039), postgis_tiger_geocoder: Support for TIGER 2021 (Regina Obe)
  - [#5097](https://trac.osgeo.org/postgis/ticket/5097), Ensure spatial index is used during topology population
           at the getClosestEdge step (Sandro Santilli)
  - [#5091](https://trac.osgeo.org/postgis/ticket/5091), Fix --without-protobuf builds (Tobias Bussmann)
  - [#5100](https://trac.osgeo.org/postgis/ticket/5100), Don't use pg_atoi (removed in PG15) (Laurenz Albe)
  - [#5114](https://trac.osgeo.org/postgis/ticket/5114), Crash with long column names pgsql2shp (Paul Ramsey)
  - [#5123](https://trac.osgeo.org/postgis/ticket/5123), Support for PG15 (JSON funcs exposed) (Regina Obe)
  - [#5125](https://trac.osgeo.org/postgis/ticket/5125), Fix search path function (Sandro Santilli)
  - [#5150](https://trac.osgeo.org/postgis/ticket/5150), Change signature of AddToSearchPath (Regina Obe)
  - [#5151](https://trac.osgeo.org/postgis/ticket/5151), ST_SetPoint with empty geometries (Regina Obe)
  - [#5152](https://trac.osgeo.org/postgis/ticket/5152), Fix infinite loop with ST_Split (Sandro Santilli)
  - [#5173](https://trac.osgeo.org/postgis/ticket/5173), Crash creating flatgeobuf on row without geometry (Julien Rouhaud)
  - [#4541](https://trac.osgeo.org/postgis/ticket/4541), ST_ConcaveHull returns a "geometrycollection" type
        instead of the expected "polygon" (Regina Obe)
  - [#5154](https://trac.osgeo.org/postgis/ticket/5154), raster ST_Value is undercosted (Regina Obe)

### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.2.2 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.2.2&milestone=PostGIS+3.1.6&milestone=PostGIS+3.0.6
