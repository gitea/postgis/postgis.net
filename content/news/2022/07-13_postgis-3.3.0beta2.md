---
title: PostGIS 3.3.0beta2
layout: post
category: news
tags: [release, 3.3]
author: Regina Obe
thumbnail:
date: "2022-07-13"
---

The PostGIS Team is pleased to release PostGIS 3.3.0beta2!
Best Served with [PostgreSQL 15 Beta2](https://www.postgresql.org/about/news/postgresql-15-beta-2-released-2479/)
,[GEOS 3.11.0](https://libgeos.org/posts/2022-07-01-geos-3-11-0-released/)
, and [SFCGAL 1.4.1](https://gitlab.com/Oslandia/SFCGAL/-/tags/v1.4.1)

Lower versions of the aforementioned dependencies will not have all new features.

This release supports PostgreSQL 11-15.

### 3.3.0beta2

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.0beta2.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.3.0beta2.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.0beta2/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.3.0beta2.pdf)


This release is a beta of a major release, it includes bug fixes since PostGIS 3.2.1 and new features.

<!--more-->

Changes since PostGIS 3.3.0beta1:

### New features
- [#5183](https://trac.osgeo.org/postgis/ticket/5183), topology.RemoveUnusedPrimitives (Sandro Santilli)

### Enhancements
- [GH698](https://github.com/postgis/postgis/pull/698), support parallel aggregate for ST_Union (Sergei Shoulbakov)

### Bug fixes
- [#5179](https://trac.osgeo.org/postgis/ticket/5179) pgsql2shp syntax error on big-endian (Bas Couwenberg)


### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.3.0beta2 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=closed&milestone=PostGIS+3.3.0&milestone=PostGIS+3.2.2&milestone=PostGIS+3.2.1&milestone=PostGIS+3.1.6
