---
title: PostGIS 2.5.7
layout: post
category: news
tags: [release, 2.5]
author: Regina Obe
thumbnail:
date: "2022-07-19T12:43:40"
---

The PostGIS Team is pleased to release PostGIS 2.5.7!

### 2.5.7

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.7.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-2.5.7.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.7/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-2.5.7.pdf)


This release is a bug fix release, addressing issues found in the previous 2.5 releases.

<!--more-->

### Bug Fixes

  - [#5151](https://trac.osgeo.org/postgis/ticket/5151), ST_SetPoint with empty geometries (Regina Obe)
  - [#5152](https://trac.osgeo.org/postgis/ticket/5152), Fix infinite loop with ST_Split (Sandro Santilli)
  - [#5150](https://trac.osgeo.org/postgis/ticket/5150), Change signature of AddToSearchPath (Regina Obe)
  - [#5155](https://trac.osgeo.org/postgis/ticket/5155), More schema qual fixes (Regina Obe)


### Upgrading

After installing the binaries or after running pg_upgrade:

do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 2.5.7 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+2.5.7
