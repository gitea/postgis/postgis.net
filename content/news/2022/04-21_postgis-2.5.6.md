---
title: PostGIS 2.5.6
layout: post
category: news
tags: [release, 2.5]
author: Regina Obe
thumbnail:
date: "2022-04-21"
---

The PostGIS Team is pleased to release PostGIS 2.5.6!

### 2.5.6

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.6.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-2.5.6.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.6/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-2.5.6.pdf)


This release is a bug fix release, addressing issues found in the previous 2.5 releases.

<!--more-->

### Bug Fixes

  - [4968](https://trac.osgeo.org/postgis/ticket/4968) Update autogen.sh to handle autoconf 2.71 issues
  - [4871](https://trac.osgeo.org/postgis/ticket/4871) TopoGeometry::geometry cast returns NULL for empty
           TopoGeometry objects (Sandro Santilli)
  - [4757](https://trac.osgeo.org/postgis/ticket/4757) Handle line collapse due to snap-to-existing node (Sandro Santilli)
  - [4769](https://trac.osgeo.org/postgis/ticket/4769) Fix segfault in st_addband (Raúl Marín)
  - [4863](https://trac.osgeo.org/postgis/ticket/4863) Update bboxes after affine/scale operations (Paul Ramsey)
  - [4326](https://trac.osgeo.org/postgis/ticket/4326) Fix CircularPolygon area calculation (Paul Ramsey)
  - [4971](https://trac.osgeo.org/postgis/ticket/4971) Cunit fixes for newer CUnit (Raúl Marín, Regina Obe)
  - [5069](https://trac.osgeo.org/postgis/ticket/5069) search_path vulnerabilty during install/upgrade
           (Regina Obe)
  - [5086](https://trac.osgeo.org/postgis/ticket/5086) regress cleanly (clip by box, make valid)
           with GEOS 3.8+. For GEOS 3.8+
           use native GEOS makevalid, buildarea, clipbybox (Regina Obe)
  - [4457](https://trac.osgeo.org/postgis/ticket/4457) Stop trying to createlang in regress
           plpgsql is always installed (Regina Obe)
  - [4980](https://trac.osgeo.org/postgis/ticket/4980) Support pkg-config for pcre detection (Regina Obe)

### Upgrading

After installing the binaries or after running pg_upgrade:

do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 2.5.6 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+2.5.6&milestone=PostGIS+2.4.10
