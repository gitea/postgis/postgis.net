---
title: PostGIS 3.0.5
layout: post
category: news
tags: [release, 3.0]
author: Regina Obe
date: "2022-02-02"
thumbnail:
---

The PostGIS Team is pleased to release PostGIS 3.0.5.

### 3.0.5

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.5.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.0.5.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.5/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.0.5.pdf)

<!--more-->

This release is a bug fix release, addressing issues found in the previous 3.0 releases.

- [#5026](https://trac.osgeo.org/postgis/ticket/5026), fix DropTopology in presence of UPDATE triggers on topology layers (Sandro Santilli)
- [#5046](https://trac.osgeo.org/postgis/ticket/5046), Fix upgrades in absence of old library (Sandro Santilli)
- [#5069](https://trac.osgeo.org/postgis/ticket/5069), search_path vulnerabilty during install/upgrade (Regina Obe)

<!--more-->

### Upgrading

After installing the binaries or after running pg_upgrade:

For upgrading from PostGIS 3.0, do the following which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.0.5 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.0.5
