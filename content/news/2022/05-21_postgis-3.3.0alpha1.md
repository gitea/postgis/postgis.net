---
title: PostGIS 3.3.0alpha1
layout: post
category: news
tags: [release, 3.3]
author: Regina Obe
thumbnail:
date: "2022-05-21"
---

The PostGIS Team is pleased to release PostGIS 3.3.0alpha1!
This is the first release to support PostgreSQL 15.  Best Served with [PostgreSQL 15 Beta1](https://www.postgresql.org/about/news/postgresql-15-beta-1-released-2453/). This release supports PostgreSQL 11-15.


### 3.3.0alpha1

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.0alpha1.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.3.0alpha1.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.0alpha1/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.3.0alpha1.pdf)


This release is an alpha of a major release, it includes bug fixes since PostGIS 3.2.1 and new features.

<!--more-->

### Breaking changes
- Drop support for PostgreSQL 9.6 and 10 (Regina Obe)
- Change output for WKT MULTIPOINT. All points now
  wrapped in parens. (Even Roualt)
- [GH674](https://github.com/postgis/postgis/pull/674), geometry validation and fixing is disabled
  for `ST_DumpAsPolygons` and `ST_Polygon` so it works faster
  but might produce invalid polygons. (Aliaksandr Kalenik)

### Enhancements
- [#2861](https://trac.osgeo.org/postgis/ticket/2861), Add index on topology.node(containing_face) speeding up
  splitting and merging of faces (Sandro Santilli)
- [#2083](https://trac.osgeo.org/postgis/ticket/2083), Speed up ST_RemEdge topology functions adding index on
relation(element_id) and edge_data(abs_next*) (Sandro Santilli)
- [#5118](https://trac.osgeo.org/postgis/ticket/5118), Allow dropping topologies with missing topogeometry sequences
(Sandro Santilli)
- [#5111](https://trac.osgeo.org/postgis/ticket/5111), faster topology face MBR computation (Sandro Santilli)
- postgis_extensions_upgrade() support for upgrades from any PostGIS
version, including yet to be released ones (Sandro Santilli)
- [#5040](https://trac.osgeo.org/postgis/ticket/5040), add postgis_sfcgal_full_version (Loïc Bartoletti)
- [GH655](https://github.com/postgis/postgis/pull/655), GiST: balance the tree splits better in recursive calls (Darafei Praliaskouski)
- [GH657](https://github.com/postgis/postgis/pull/657), GiST: do not call no-op decompress function (Aliaksandr Kalenik)
- [#4912](https://trac.osgeo.org/postgis/ticket/4912), GiST: fix crash on STORAGE EXTERNAL for geography (Aliaksandr Kalenik)
- `ST_ConcaveHull` GEOS 3.11+ native implementation (Paul Ramsey, Martin Davis)
- [#4574](https://trac.osgeo.org/postgis/ticket/4574), [GH678](https://github.com/postgis/postgis/pull/678), [#5121](https://trac.osgeo.org/postgis/ticket/5121) Enable Link-Time Optimizations using --enable-lto (Sergei Shoulbakov)
- [GH676](https://github.com/postgis/postgis/pull/676), faster ST_Clip (Aliaksandr Kalenik)
- [#5135](https://trac.osgeo.org/postgis/ticket/5135), Fast GiST index build is enabled by default for PostgreSQL 15+ (Sergei Shoulbakov)

### New features
- [#5116](https://trac.osgeo.org/postgis/ticket/5116), Topology export/import scripts (Sandro Santilli)
- ST_Letters creates geometries that look like letters (Paul Ramsey)
- [#5037](https://trac.osgeo.org/postgis/ticket/5037), postgis_sfcgal: ST_3DConvexHull (Loïc Bartoletti)
- postgis_sfcgal: sfcgal_full_version - reports BOOST and CGAL version
  (Loïc Bartoletti)
- [GH659](https://github.com/postgis/postgis/pull/659), MARC21/XML, ST_GeomFromMARC21, ST_AsMARC21 (Jim Jones)
- [#5132](https://trac.osgeo.org/postgis/ticket/5132), [GH683](https://github.com/postgis/postgis/pull/683), sfcgal: ST_3DUnion aggregate function (Sergei Shoulbakov)
- [#5143](https://trac.osgeo.org/postgis/ticket/5143), SFCGAL ST_AlphaShape and ST_OptimalAlphaShape (Loïc Bartoletti)

### Bug Fixes
- [#5100](https://trac.osgeo.org/postgis/ticket/5100), Support for PostgreSQL 15 (atoi removal) (Laurenz Albe)
- [#5123](https://trac.osgeo.org/postgis/ticket/5123), Support for PostgreSQL 15 - PG15 now exposes json types and functions,
			do not include for PG15+ (Regina Obe)
- [#5088](https://trac.osgeo.org/postgis/ticket/5088), Memory corruption in mvt_agg_transfn (Victor Collod)
- [#5137](https://trac.osgeo.org/postgis/ticket/5137), resetting interrupt flags before query execution (Sergei Shoulbakov)
- [#5148](https://trac.osgeo.org/postgis/ticket/5148), ST_Clip is more robust to alignment of raster and clip geometry (Sergei Shoulbakov)


### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.3.0alpha1 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.3.0&milestone=PostGIS+3.1.5&milestone=PostGIS+3.0.5
