---
title: PostGIS 3.3.0rc1
layout: post
category: news
tags: [release, 3.3]
author: Regina Obe
thumbnail:
date: "2022-08-08"
---

The PostGIS Team is pleased to release PostGIS 3.3.0rc1!
Best Served with [PostgreSQL 15 beta2](https://www.postgresql.org/about/news/postgresql-15-beta-2-released-2479/)
,[GEOS 3.11.0](https://libgeos.org/posts/2022-07-01-geos-3-11-0-released/)
, and [SFCGAL 1.4.1](https://gitlab.com/Oslandia/SFCGAL/-/tags/v1.4.1)

Lower versions of the aforementioned dependencies will not have all new features.

This release supports PostgreSQL 11-15.

### 3.3.0rc1

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.0rc1.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.3.0rc1.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.0rc1/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.3.0rc1.pdf)


This release is a release candidate of a major release, it includes bug fixes since PostGIS 3.2.2 and new features.

<!--more-->

Changes since PostGIS 3.3.0beta2:

### Bug fixes
  - [#5154](https://trac.osgeo.org/postgis/ticket/5154) raster `ST_Value` is undercosted (Regina Obe)
  - [#5157](https://trac.osgeo.org/postgis/ticket/5157) Revise minimum_bounding_circle Cunit test
           to be tolerant of small 32-bit floating point differences
           (Regina Obe)
  - [#5191](https://trac.osgeo.org/postgis/ticket/5191) Functions should use integer instead of int4 (Regina Obe)
  - [#5139](https://trac.osgeo.org/postgis/ticket/5139) PostGIS causes to_jsonb to no longer be parallel safe,
           `ST_AsGeoJSON` and `ST_AsGML` are also parallel unsafe
           (Regina Obe, Paul Ramsey)
  - [#5025](https://trac.osgeo.org/postgis/ticket/5025) Ensure that additional operators are not appended
           when the function and opfamily disagree
           about dimensionality (Paul Ramsey)
  - [#5195](https://trac.osgeo.org/postgis/ticket/5195), [#5196](https://trac.osgeo.org/postgis/ticket/5196) Change address_standardizer
           and postgis_tiger_geocoder CREATE EXTENSION to use CREATE
           instead of CREATE OR REPLACE. (Regina Obe)
  - [#5202](https://trac.osgeo.org/postgis/ticket/5202) Guard against downgrade (Sandro Santilli)
  - [#5104](https://trac.osgeo.org/postgis/ticket/5104) postgis_extensions_upgrade() fails with pgextwlist (Regina Obe)

### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.3.0rc1 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=closed&milestone=PostGIS+3.3.0&milestone=PostGIS+3.2.2&milestone=PostGIS+3.2.1&milestone=PostGIS+3.1.6
