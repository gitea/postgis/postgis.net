---
title: PostGIS 3.1.5
layout: post
category: news
tags: [release, 3.1]
author: Regina Obe
date: "2022-02-01"
thumbnail:
---

The PostGIS Team is pleased to release PostGIS 3.1.5!

### 3.1.5

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.5.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.1.5.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.5/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.1.5.pdf)


This release is a bug fix release, addressing issues found in the previous 3.1 releases.

<!--more-->

- [#4994](https://trac.osgeo.org/postgis/ticket/4994), Random missing INSERT in shp2pgsql (Sandro Santilli)
- [#5016](https://trac.osgeo.org/postgis/ticket/5016), loader (shp2pgsq): Respect LDFLAGS (Greg Troxel)
- [#5018](https://trac.osgeo.org/postgis/ticket/5018), pgsql2shp basic support for WITH CTE clause (Regina Obe)
- [#5026](https://trac.osgeo.org/postgis/ticket/5026), fix DropTopology in presence of UPDATE triggers on
				 topology layers (Sandro Santilli)
- [#5033](https://trac.osgeo.org/postgis/ticket/5033), #5035, allow upgrades in presence of views using deprecated
				 functions (Sandro Santilli)
- [#5046](https://trac.osgeo.org/postgis/ticket/5046), Fix upgrades in absence of old library (Sandro Santilli)
- [#5069](https://trac.osgeo.org/postgis/ticket/5069), search_path vulnerabilty during install/upgrade
				 (Regina Obe)
- [#5041](https://trac.osgeo.org/postgis/ticket/5041), postgis_tiger_geocoder: loader_generate_script
				 generates script with invalid syntax (Regina Obe)


### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.1.5 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.1.5
