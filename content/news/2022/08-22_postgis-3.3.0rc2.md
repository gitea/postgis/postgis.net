---
title: PostGIS 3.3.0rc2
layout: post
category: news
tags: [release, 3.3]
author: Regina Obe
thumbnail:
date: "2022-08-22"
---

The PostGIS Team is pleased to release PostGIS 3.3.0rc2!
Best Served with [PostgreSQL 15 beta3](https://www.postgresql.org/about/news/postgresql-145-138-1212-1117-1022-and-15-beta-3-released-2496/)
,[GEOS 3.11.0](https://libgeos.org/posts/2022-07-01-geos-3-11-0-released/)
, and [SFCGAL 1.4.1](https://gitlab.com/Oslandia/SFCGAL/-/tags/v1.4.1)

Lower versions of the aforementioned dependencies will not have all new features.

This release supports PostgreSQL 11-15.

### 3.3.0rc2

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.0rc2.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.3.0rc2.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.0rc2/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.3.0rc2.pdf)


This release is a release candidate of a major release, it includes bug fixes since PostGIS 3.2.3.

<!--more-->

Changes since PostGIS 3.3.0rc1:

### Bug fixes

  - [#5089](https://trac.osgeo.org/postgis/ticket/5089) `ST_Reverse` also reverses components of CompoundCurve (Paul Ramsey)
  - [#5181](https://trac.osgeo.org/postgis/ticket/5181) Reset proj error state after failed parse (Paul Ramsey)
  - [#5171](https://trac.osgeo.org/postgis/ticket/5171)  Short circuit geodesic distance when inputs equal (Paul Ramsey)


### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.3.0rc2 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=closed&milestone=PostGIS+3.3.0&milestone=PostGIS+3.2.3&milestone=PostGIS+3.1.7&milestone=PostGIS+3.0.7&milestone=PostGIS+2.5.8
