---
title: PostGIS 2.5.9 EOL
layout: post
category: news
tags: [release, 2.5, "EOL"]
author: Regina Obe
date: "2022-11-12T10:00:00Z"
---
The PostGIS Team is pleased to release PostGIS **2.5.9**! This is the end-of-life release of the 2.5 branch.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.9.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-2.5.9.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.9/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-2.5.9.pdf)

This release is a bug fix release, addressing issues found in the previous 2.5 releases.

<!--more-->

This release works with PostgreSQL versions 9.4 - 12.

### Bug Fixes

  - [5241](https://trac.osgeo.org/postgis/ticket/5241), Crash on ST_SnapToGrid with empty multis (Regina Obe)
  - [5280](https://trac.osgeo.org/postgis/ticket/5280), shp2pgsql: Handle load of dbase character fields with no width specified (Regina Obe)
  - [5240](https://trac.osgeo.org/postgis/ticket/5240), ST_DumpPoints crash with empty polygon (Regina Obe)
  - [5084](https://trac.osgeo.org/postgis/ticket/5084), Bad rasterization of linestring (Gilles Vuidel)

### Upgrading

After installing the binaries or after running `pg_upgrade` do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis) or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 2.5.9 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+2.5.9
