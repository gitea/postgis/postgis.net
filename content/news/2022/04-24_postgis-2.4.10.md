---
title: PostGIS 2.4.10
layout: post
category: news
tags: [release, 2.4, "EOL"]
author: Regina Obe
date: "2022-04-24"
---
The PostGIS Team is pleased to release PostGIS 2.4.10! This is the end-of-life release of the 2.4 branch.

### 2.4.10

* [source download]({{< loc "postgis.release_source">}}/postgis-2.4.10.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-2.4.10.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.4.10/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-2.4.10.pdf)


This release is a bug fix release, addressing issues found in the previous 2.4 releases.

<!--more-->

### Bug Fixes

  - [4871](https://trac.osgeo.org/postgis/ticket/4871) TopoGeometry::geometry cast returns NULL for empty
           TopoGeometry objects (Sandro Santilli)
  - [4757](https://trac.osgeo.org/postgis/ticket/4757) Handle line collapse due to snap-to-existing node (Sandro Santilli)
  - [4769](https://trac.osgeo.org/postgis/ticket/4769) Fix segfault in st_addband (Raúl Marín)
  - [4727](https://trac.osgeo.org/postgis/ticket/4727) Fix geocentric bounding box computation for rare case (Paul Ramsey)
  - [4326](https://trac.osgeo.org/postgis/ticket/4326) Fix CircularPolygon area calculation (Paul Ramsey)
  - [4968](https://trac.osgeo.org/postgis/ticket/4968) Update autogen.sh to handle autoconf 2.71 issues (Raúl Marín, Regina Obe)
  - [4971](https://trac.osgeo.org/postgis/ticket/4971) Cunit fixes for newer CUnit (Raúl Marín, Regina Obe)
  - [5140](https://trac.osgeo.org/postgis/ticket/5140) regress cleanly against GEOS 3.7-3.8, GDAL 3.4,
           fix address_standardizer install
           and allow to install via pkg-config  (Regina Obe)

### Upgrading

After installing the binaries or after running pg_upgrade:

do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 2.4.10 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+2.4.10
