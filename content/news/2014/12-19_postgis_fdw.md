---
title: Foreign Data Wrappers for PostGIS
slug: "postgis_fdw"
layout: post
category: news
tags: [fdw, oracle, ogr]
author: Paul Ramsey
date: "2014-12-19"
thumbnail:
---
The last couple weeks have seen two interesting updates in the world of PostgreSQL  "[foreign data wrappers](https://wiki.postgresql.org/wiki/Foreign_data_wrappers)" (FDW). Foreign data wrappers allow you to access remote data inside your database, exactly like other tables. PostgreSQL ships with two example implementations, one for accessing text files, and the other for accessing remote PostgreSQL servers.

The two updates of interest to PostGIS users are:

* The [Oracle FDW](http://laurenz.github.io/oracle_fdw/) implementation was recently enhanced to support spatial columns, so an Oracle table with SDO_GEOMETRY columns can be exposed in PostgreSQL as a table with PostGIS geometry columns.
* A new [OGR FDW](https://github.com/pramsey/pgsql-ogr-fdw) implementation [was released](http://boundlessgeo.com/2014/12/ogr-fdw-ftw/) that supports exposing any OGR data source as a table with PostGIS geometry columns.

Now you can access your PostGIS data without even going to the trouble of importing it first!
