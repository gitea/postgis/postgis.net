---
title: PostGIS 2.1.5 Released
layout: post
category: news
tags: [release, 2.1]
author: Paul Ramsey
thumbnail:
date: "2014-12-18"
---
The 2.1.5 release of PostGIS is [now available]({{< loc "postgis.release_source">}}/postgis-2.1.5.tar.gz).

The PostGIS development team is happy to release patch for PostGIS 2.1, the 2.1.5 release. As befits a patch release, the focus is on bugs, breakages, and performance issues

<{{< loc "postgis.release_source">}}/postgis-2.1.5.tar.gz>

<!--more-->

[html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.1.5.tar.gz)
[pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.1.5.pdf)

**Enhancements**

* [#2933](http://trac.osgeo.org/postgis/ticket/2933), Speedup construction of large multi-geometry objects

**Bug Fixes**

* [#2947](http://trac.osgeo.org/postgis/ticket/2947), Fix memory leak in lwgeom_make_valid for single-component collection input
* [#2949](http://trac.osgeo.org/postgis/ticket/2949), Fix memory leak in lwgeom_mindistance2d for curve input
* [#2931](http://trac.osgeo.org/postgis/ticket/2931), BOX representation is case sensitive
* [#2942](http://trac.osgeo.org/postgis/ticket/2942), PostgreSQL 9.5 support
* [#2953](http://trac.osgeo.org/postgis/ticket/2953), 2D stats not generated when Z/M values are extreme
* [#3009](http://trac.osgeo.org/postgis/ticket/3009), Geography cast may effect underlying tuple


View all [closed tickets][1].

  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.1.5&order=priority

