---
title: PostGIS 2.1.2 Released
layout: post
category: news
tags: [release, 2.1]
author: Paul Ramsey
thumbnail:
date: "2014-03-31"
---

The 2.1.2 release of PostGIS is [now available]({{< loc "postgis.release_source">}}/postgis-2.1.2.tar.gz).

The PostGIS development team is happy to release patch for PostGIS 2.1, the 2.1.2 release. As befits a patch release, the focus is on bugs and breakages.

Best served with a bottle of GEOS 3.4.2.

<{{< loc "postgis.release_source">}}/postgis-2.1.2.tar.gz>

<!--more-->

[html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.1.2.tar.gz)
[pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.1.2.pdf)

**Changes**

* [#2514](http://trac.osgeo.org/postgis/ticket/2514), Change raster license from GPL v3+ to v2+, allowing distribution of PostGIS Extension as GPLv2.

**Bug Fixes**

* [#2666](http://trac.osgeo.org/postgis/ticket/2666), Error out at configure time if no SQL preprocessor can be found
* [#2534](http://trac.osgeo.org/postgis/ticket/2534), st_distance returning incorrect results for large geographies
* [#2539](http://trac.osgeo.org/postgis/ticket/2539), Check for json-c/json.h presence/usability before json/json.h
* [#2543](http://trac.osgeo.org/postgis/ticket/2543), invalid join selectivity error from simple query
* [#2546](http://trac.osgeo.org/postgis/ticket/2546), GeoJSON with string coordinates parses incorrectly
* [#2547](http://trac.osgeo.org/postgis/ticket/2547), Fix ST_Simplify(TopoGeometry) for hierarchical topogeoms
* [#2552](http://trac.osgeo.org/postgis/ticket/2552), Fix NULL raster handling in ST_AsPNG, ST_AsTIFF and ST_AsJPEG
* [#2555](http://trac.osgeo.org/postgis/ticket/2555), Fix parsing issue of range arguments of ST_Reclass
* [#2556](http://trac.osgeo.org/postgis/ticket/2556), geography ST_Intersects results depending on insert order
* [#2580](http://trac.osgeo.org/postgis/ticket/2580), Do not allow installing postgis twice in the same database
* [#2589](http://trac.osgeo.org/postgis/ticket/2589), Remove use of unnecessary void pointers
* [#2607](http://trac.osgeo.org/postgis/ticket/2607), Cannot open more than 1024 out-db files in one process
* [#2610](http://trac.osgeo.org/postgis/ticket/2610), Ensure face splitting algorithm uses the edge index
* [#2615](http://trac.osgeo.org/postgis/ticket/2615), EstimatedExtent (and hence, underlying stats) gathering wrong bbox
* [#2619](http://trac.osgeo.org/postgis/ticket/2619), Empty rings array in GeoJSON polygon causes crash
* [#2634](http://trac.osgeo.org/postgis/ticket/2634), regression in sphere distance code
* [#2638](http://trac.osgeo.org/postgis/ticket/2638), Geography distance on M geometries sometimes wrong
* [#2648](http://trac.osgeo.org/postgis/ticket/2648), [#2653](http://trac.osgeo.org/postgis/ticket/2653), Fix topology functions when "topology" is not in search_path
* [#2654](http://trac.osgeo.org/postgis/ticket/2654), Drop deprecated calls from topology
* [#2655](http://trac.osgeo.org/postgis/ticket/2655), Let users without topology privileges call postgis_full_version()
* [#2674](http://trac.osgeo.org/postgis/ticket/2674), Fix missing operator = and hash_raster_ops opclass on raster
* [#2675](http://trac.osgeo.org/postgis/ticket/2675), [#2534](http://trac.osgeo.org/postgis/ticket/2534), [#2636](http://trac.osgeo.org/postgis/ticket/2636), [#2634](http://trac.osgeo.org/postgis/ticket/2634), [#2638](http://trac.osgeo.org/postgis/ticket/2638), Geography distance issues with tree optimization

**Enhancements**

* [#2494](http://trac.osgeo.org/postgis/ticket/2494), avoid memcopy in GiST index (hayamiz)
* [#2560](http://trac.osgeo.org/postgis/ticket/2560), soft upgrade: avoid drop/recreate of aggregates that hadn't changed

View all [closed tickets][1].

  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.1.2&order=priority

