---
title: PostGIS 2.1.4 Released
layout: post
category: news
tags: [release, 2.1]
author: Regina Obe
thumbnail:
date: "2014-09-10"
---
The 2.1.4 release of PostGIS is [now available]({{< loc "postgis.release_source">}}/postgis-2.1.4.tar.gz).

The PostGIS development team is happy to release patch for PostGIS 2.1, the 2.1.4 release. As befits a patch release, the focus is on bugs, breakages, and performance issues

<{{< loc "postgis.release_source">}}/postgis-2.1.4.tar.gz>

<!--more-->

[html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.1.4.tar.gz)
[pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.1.4.pdf)

**Enhancements**

* [#2745](http://trac.osgeo.org/postgis/ticket/2745), Speedup `ST_Simplify` calls against points
* [#2747](http://trac.osgeo.org/postgis/ticket/2747), Support for GDAL 2.0
* [#2749](http://trac.osgeo.org/postgis/ticket/2749), Make rtpostgis_upgrade_20_21.sql ACID
* [#2811](http://trac.osgeo.org/postgis/ticket/2811), Do not specify index names when loading shapefiles/rasters
* [#2829](http://trac.osgeo.org/postgis/ticket/2829), Shortcut `ST_Clip(raster)` if geometry fully contains the raster
           and no NODATA specified
* [#2895](http://trac.osgeo.org/postgis/ticket/2895), Raise cost of `ST_ConvexHull(raster)` to 300 for better query plans

**Bug Fixes**

* [#2605](http://trac.osgeo.org/postgis/ticket/2605), armel: _`ST_Covers()` returns true for point in hole
* [#2911](http://trac.osgeo.org/postgis/ticket/2911), Fix output scale on `ST_Rescale`/`ST_Resample`/`ST_Resize` of rasters
           with scale 1/-1 and offset 0/0.
  - Fix crash in `ST_Union(raster)`
* [#2704](http://trac.osgeo.org/postgis/ticket/2704), `ST_GeomFromGML()` does not work properly with array of gml:pos
    (Even Roualt)
* [#2708](http://trac.osgeo.org/postgis/ticket/2708), updategeometrysrid doesn't update srid check when schema
           not specified. Patch from Marc Jansen
* [#2720](http://trac.osgeo.org/postgis/ticket/2720), lwpoly_add_ring should update maxrings after realloc
* [#2759](http://trac.osgeo.org/postgis/ticket/2759), Fix postgis_restore.pl handling of multiline object comments
           embedding sql comments
* [#2774](http://trac.osgeo.org/postgis/ticket/2774), fix undefined behavior in ptarray_calculate_gbox_geodetic
  - Fix potential memory fault in `ST_MakeValid`
* [#2784](http://trac.osgeo.org/postgis/ticket/2784), Fix handling of bogus argument to --with-sfcgal
* [#2772](http://trac.osgeo.org/postgis/ticket/2772), Premature memory free in RASTER_getBandPath (`ST_BandPath)`
* [#2755](http://trac.osgeo.org/postgis/ticket/2755), Fix regressions tests against all versions of SFCGAL
* [#2775](http://trac.osgeo.org/postgis/ticket/2775), lwline_from_lwmpoint leaks memory
* [#2802](http://trac.osgeo.org/postgis/ticket/2802), `ST_MapAlgebra` checks for valid callback function return value
* [#2803](http://trac.osgeo.org/postgis/ticket/2803), `ST_MapAlgebra` handles no userarg and STRICT callback function
* [#2834](http://trac.osgeo.org/postgis/ticket/2834), `ST_Estimated_Extent` and mixedCase table names (regression bug)
* [#2845](http://trac.osgeo.org/postgis/ticket/2845), Bad geometry created from `ST_AddPoint`
* [#2870](http://trac.osgeo.org/postgis/ticket/2870), Binary insert into geography column results geometry being inserted
* [#2872](http://trac.osgeo.org/postgis/ticket/2872), make install builds documentation (Greg Troxell)
* [#2819](http://trac.osgeo.org/postgis/ticket/2819), find isfinite or replacement on Centos5 / Solaris
* [#2899](http://trac.osgeo.org/postgis/ticket/2899), geocode limit 1 not returning best answer (tiger geocoder)
* [#2903](http://trac.osgeo.org/postgis/ticket/2903), Unable to compile on FreeBSD
* [#2927](http://trac.osgeo.org/postgis/ticket/2927)  reverse_geocode not filling in direction prefix (tiger geocoder)
           get rid of deprecated `ST_Line_Locate_Point` called

View all [closed tickets][1].

  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.1.4&order=priority

