---
title: Security releases 2.0.6 and 2.1.3
slug: "postgis-2.0.6_and_2.1.3"
layout: post
category: news
tags: [release, 2.1, 2.0]
author: Sandro Santilli
thumbnail:
date: "2014-05-19"
---

It has come to our attention that the PostGIS Raster support
may give more privileges to users than an administrator is
willing to grant.  These include reading files from the filesystem
and opening connections to network hosts.

<!--more-->

Both issues can be limited in existing installations by setting
the GDAL_SKIP variable (in the PostgreSQL server environment)
to the list of all gdal drivers, but some drivers would still be
forceably loaded by some operations.

Releases 2.1.3 and 2.0.6 strengthen the code to load no drivers
by default and allows for a fine-grained tuning of what's allowed
and what not through postgis-specific environment variables:

  - `POSTGIS_GDAL_ENABLED_DRIVERS`
    Specifies a list of GDAL drivers to _enable_ (rather than _skip_)
    By default all drivers are disabled.
    Example value: "GTiff PNG JPEG"

  - `POSTGIS_ENABLE_OUTDB_RASTERS`
    Enables read support for out-db raster bands if set to 1.
    By default out-db raster bands reading is disabled.

Upgrade is highly recommended, especially for online services
allowing users to run arbitrary SQL queries.

Downloads:

 - <{{< loc "postgis.release_source">}}/postgis-2.0.6.tar.gz>
   MD5: ba6fc0a3f13b86e83cd8db1532110636

 - <{{< loc "postgis.release_source">}}/postgis-2.1.3.tar.gz>
   MD5: 104e4468429ea0882259fc268c62d6c7

Special thanks to Even Rouault for bringing up the issue and giving advice
on its resolution.
