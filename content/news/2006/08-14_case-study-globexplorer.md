---
layout: post
category: news
title: GlobeXplorer
author: Paul Ramsey
date: "2006-08-14"
---

<p>In the second entry of our collection of
<a href='/documentation/casestudies'>case studies</a>,
read about
<a href='/documentation/casestudies/globexplorer/'>how GlobeXplorer</a> is using PostGIS
to serve millions of image and map requests every day.
</p>
