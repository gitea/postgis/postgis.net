---
layout: post
category: news
title: "Case Study : Institut G&eacute;ographique National, France"
author: Paul Ramsey
date: "2006-10-15"
---



<p>In the latest entry of our collection of
<a href='/documentation/casestudies'>case studies</a>,
read about how the
<a href='/documentation/casestudies/ign/'>Institut G&eacute;ographique National</a>
is using PostGIS
to manage over 100 million topographic features in PostGIS/PostgreSQL and provide read/write access to over 100 field researchers around the country.
</p>
