---
layout: post
category: news
title: "PostGIS @ FOSS4G 2006"
date: "2006-08-25"
author: Paul Ramsey
---

<p>The <a href='http://www.foss4g2006.org/'>fourth annual world gathering</a> of open source geospatial developers and users this year is <a href='http://www.foss4g2006.org/'>FOSS4G</a> in Lausanne, Switzerland from September 12-16.  From the PostGIS development team, <a href='http://www.foss4g2006.org/contributionDisplay.py?contribId=47&sessionId=61&confId=1'>Paul Ramsey</a> and <a href='http://www.foss4g2006.org/contributionDisplay.py?contribId=109&sessionId=42&confId=1'>Sandro Santilli</a> (better known as "strk") will be in attendance and giving talks, we hope to see you there!
</p>
