---
layout: post
category: news
title: "Case Study : EU Joint Reseach Centre"
author: Paul Ramsey
date: "2006-10-25"
---

<p>In the latest entry of our collection of
<a href='/documentation/casestudies'>case studies</a>,
read about how the
<a href='/documentation/casestudies/jrc/'>EU Joint Research Centre</a>
is using billion dollar radar satellites and PostGIS to monitor fishing vessels in the North Atlantic and provide real-time reporting and mapping to decision makers and fisheries regulators.
</p>
