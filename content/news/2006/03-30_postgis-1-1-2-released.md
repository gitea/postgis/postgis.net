---
layout: post
category: news
title: PostGIS 1.1.2 Released
author: Paul Ramsey
tags: [release, 1.1]
date: "2006-03-30"
---

<p>The <A HREF='{{< loc "postgis.release_source">}}/postgis-1.1.2.tar.gz'>1.1.2 release</A> is a minor release consisting primarily of bug fixes and code cleanups.
</p>

<UL>
<LI>Regress tests can now be run *before* postgis intallation
<LI>BUGFIX in SnapToGrid() computation of output bounding box
<LI>More portable ./configure script
<LI>Changed ./run_test script to have more sane default behaviour
<LI>Fixed support for 64bit archs
<LI>jdbc2 SRID handling fixes in JTS code
<LI>New affine() matrix transformation functions
<LI>New rotate{,X,Y,Z}() function
<LI>Old translating and scaling functions now use affine() internally
<LI>BUGFIX in EnforceRHR()
<LI>Embedded access control in estimated_extent() for builds against pgsql >= 8.0.0
</UL>

