---
layout: post
category: news
title: "Case Study : US Davis Soil Lab"
author: Paul Ramsey
date: "2006-08-18"
---

<p>In the third entry of our collection of
<a href='/documentation/casestudies'>case studies</a>,
read about
<a href='/documentation/casestudies/casoil/'>how the UC Davis Soils lab</a> is using PostGIS
to support their research and provide public access to soil survey data.
</p>
