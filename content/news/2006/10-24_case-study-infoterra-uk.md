---
layout: post
category: news
title: "Case Study : Infoterra, UK"
author: Paul Ramsey
date: "2006-10-24"
---

<p>In the latest entry of our collection of
<a href='/documentation/casestudies'>case studies</a>,
read about how
<a href='/documentation/casestudies/infoterra/'>Infoterra</a>, a leading European satellite and aerial imagery provider, runs their data provision and sales systems on PostGIS, and stores the complete Ordnance Survey database (over 500M features!) on PostGIS.
</p>
