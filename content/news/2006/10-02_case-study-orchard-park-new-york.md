---
layout: post
category: news
title: "Case Study : Orchard Park, New York"
author: Paul Ramsey
date: "2006-10-02"
---

<p>In the latest entry of our collection of
<a href='/documentation/casestudies'>case studies</a>,
read about how
<a href='/documentation/casestudies/orchardpark/'>Orchard Park, New York</a> is using PostGIS
to manage their physical infrastructure inventory.
</p>
