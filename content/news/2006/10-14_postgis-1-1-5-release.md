---
layout: post
category: news
title: PostGIS 1.1.5 Release
author: Paul Ramsey
tags: [release, 1.1]
date: "2006-10-14"
---

<p>The 1.1.5 maintenance release of PostGIS is <a href='{{< loc "postgis.release_source">}}'>now available</a>.
This is a very minor bugfix and compatibility release.
The following changes have occurred:
</p>

<ul>
<li>Java:
<ul>
<li>Removed obsolete synchronization from Jts code.
<li>Fixed nullpointer Exception in Geometry.equals() method
<li>Added EJB3Spatial.odt to fulfill the GPL requirement of
    distributing the "preferred form of modification"
</ul>
<li>Added -S option for non-multi geometries to shp2pgsql
<li>Updated heavily outdated README files for shp2pgsql/pgsql2shp by
    merging them with the manpages.
<li>Fixed MingW link error that was causing pgsql2shp to
    segfault on Win32 when compiled for PostgreSQL 8.2
</ul>

