---
layout: post
category: news
title: PostGIS by the Numbers
author: Paul Ramsey
date: "2006-07-12"
---

<p>The popularity of PostGIS continues to grow every year! While there
is no such thing as "sales figures" for an open source project, there
are at least statistics about web site use, code downloads, and so on.

Here are the current numbers!
</p>
<ul>
<li>1101 mailing list members</li>
<li>24117 hits per day on web site</li>
<li>3532 visits per day on web site</li>
<li>100 downloads per day of source code</li>
<li>top search string references "postgis" and "shp2pgsql"</li>
<li>813000 google search results</li>
<li>great google <a href='https://trends.google.com/trends/explore?date=all&q=postgis,oracle%20spatial,arcsde'>trends</a>
</ul>

