---
layout: post
category: news
title: PostGIS 1.1.1 Released
author: Paul Ramsey
tags: [release, 1.1]
date: "2006-01-22"
---

<p>The <A HREF='{{< loc "postgis.release_source">}}/postgis-1.1.1.tar.gz'>1.1.1 release</A> is a minor release consisting primarily of bug fixes and code cleanups.  Bugs in LRS functions, database migration, and GeometryCollection handling have been resolved.  Code has been cleansed of most compiler warnings.
</p>

<UL>
 <LI>Source code cleanups
 <LI>Solaris 2.7 and MingW support improvements
 <LI>added NumInteriorRing() alias due to OpenGIS ambiguity
 <LI>BUGFIX in geometrycollection handling of GEOS-CAPI connector
 <LI>BUGFIX in line_locate_point()
 <LI>Fixed handling of postgresql paths
 <LI>Fixed a premature exit in postgis_restore.pl
 <LI>BUGFIX in line_substring()
 <LI>New Z and M interpolation in line_substring()
 <LI>New Z and M interpolation in line_interpolate_point()
 <LI>Added support for localized cluster in regress tester
</LI>

