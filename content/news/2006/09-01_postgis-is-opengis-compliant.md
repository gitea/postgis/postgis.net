---
layout: post
category: news
title: PostGIS is OpenGIS Compliant!
author: Paul Ramsey
tags: ["ogc", "sfsql"]
date: "2006-09-01"
---

It is finally official!  After several years of unofficial "conformance"
we recently took the plunge and submitted our PostGIS 1.1
[compliance tests]({{< loc "postgis.source_dev_branch">}}/extras/ogc_test_suite/)
to the <a href="http://www.opengis.org">OpenGIS Consortium</a>
for <a href="http://www.opengeospatial.org/resource/testing">testing</a>.
Last week we received the news that
we <a href="opengis-certification.png">are compliant</a>.  That means
our web site can proudly display the OpenGIS button badge, and... well
not much more than that.  It is still the same database after all.
But it is still nice to get some external validation.



