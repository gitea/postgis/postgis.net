---
layout: post
category: news
title: PostGIS 1.0.3 Released
author: Paul Ramsey
tags: [release, 1.0]
date: "2005-08-07"
---

<p>The 1.0.3 release includes a number of small bug fixes and one large bug fix
for <A HREF='http://lists.osgeo.org/pipermail/postgis-users/2005-July/008768.html'>bounding box calculation</A>.  This bug will affect indexing in rare cases.
</p>
