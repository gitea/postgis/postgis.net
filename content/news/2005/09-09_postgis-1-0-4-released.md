---
layout: post
category: news
title: PostGIS 1.0.4 Released
author: Paul Ramsey
tags: [release, 1.0]
date: "2005-09-09"
---

<p>The <A HREF='{{< loc "postgis.release_source">}}/postgis-1.0.4.tar.gz'>1.0.4 release</A> includes a number of small bug fixes and one large bug fix
for memory management when building spatial indexes. Users with very large
databases may have experienced out of memory errors prior to this fix.
</p>
