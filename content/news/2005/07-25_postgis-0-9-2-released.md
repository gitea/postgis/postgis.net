---
layout: post
category: news
title: PostGIS 0.9.2 Released
author: Paul Ramsey
tags: [release, 0.9]
date: "2005-07-25"
---

<p>A patch release for the 0.9 series has been created.  It is available on the <A href='{{< loc "postgis.release_source">}}'>download</a> page.
</p>
<ul>
<li>BUGFIX in dumper and GeomFromWKB to support 64bit archs
<li>Support for postgresql 8.1
<li>Leak plugged in dumper
<li>fault-tolerant btree ops
<li>BUGFIX in rtree index
</ul>
