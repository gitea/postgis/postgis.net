---
layout: post
category: news
title: GEOS 2.1.2 Released
author: Paul Ramsey
tags: [release, "geos", "geos-2.1"]
date: "2005-06-08"
---

<p>The latest maintenance release of GEOS includes a number of bug fixes for special cases and processing issues
discovered since the 2.1.1 release:
</p>
<ul>
<li> Segfault fix in Point::isEmpty
<li> Mem Leak fix in OffsetCurveBuilder::getRingCurve
<li> Bugfix in LineSegment::reverse
<li> Added multipolygon buffering test in source/test/testLeaksBig
<li> Ported JTS robustness patch for RobustLineIntersector
<li> Removed useless Coordinate copies in OverlayOp::mergeZ()
<li> Avoided throws by IsValid on invalid input
<li> Stricter C++ syntax (math.h=>cmath, ieeefp.h in "C" block,  ostringstream
     instead of sprintf)
<li> Better support for older compilers (Polygonizer::LineStringAdder  friendship)
<li> Removed useless Coordinate copies in CGAlgorithms::isOnLine()
<li> Added support for polygonize and parametrized buffer tests in  XMLTester
<li> Fixed support for --includedir and --libdir
<li> Fixed Z interpolation in LineIntersector
<li> Handled NULL results from getCentroid() in XMLTester
<li> Segfault fix in (EMPTY)Geometry::getCentroid()
<li> Made polygon::getBoundary() always OGC-valid (no LinearRings)
<li> Input checking and promoting in  GeometryFactory::createMultiLineString()
<li> Segfault fix in GeometryEditor::editPolygon()
</ul>

