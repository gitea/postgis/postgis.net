---
layout: post
category: news
title: PostGIS 1.0.0 Release Candidate #3
author: Paul Ramsey
tags: [release, 1.0]
date: "2005-02-24"
---

<p>
The third (and hopefully final) release candidate is now available, with bug fixes from RC2. Issues with Mapserver
and other third-party software connectivity have hopefully been resolved.
Please <A HREF='{{< loc "postgis.release_source">}}'>download</A> it and try it out!
</p>
