---
layout: post
category: news
title: "PostGIS 1.0.0 Release Candidate #1"
author: Paul Ramsey
tags: [release, "1.0"]
date: "2005-01-14"
---

<p>
The first release candidate of new PostGIS 1.0 series is now ready for
general download and testing. The 1.0 series is actually a departure
from the previous 0.X series, in that the underlying disk representation
of geometries is completely new. The new "lightweight" geometries are
much smaller (particularly for 2D features, points, and short lines) and
the indexes are also much smaller. The result is fewer disk accesses
needed, better use of memory cache, and a faster system overall, particularly
for large databases.
</p>
<p>
The new disk representation is only one of many important changes:
</p>
<ul>
<li>The "canonical" representation (the string value spit out by default
at the command line and for database dumps) is now an exact hexadecimal
representation of the geometry values. This will reduce to zero geometry
drift caused by converting doubles into strings and back again during
feature transit between the database and the loader/dumper tools. If you
need to see a human readable version of geometries, you will need to
use the standard OGC AsText() function now.</li>
<li>The loader and dumper have been retrofitted to use the new canonical
representation, so there will no longer be even the smallest chance of
coordinate drift.</li>
<li>A new suite of geometry contructor functions has been added, so for
example, it is now possible to create a point like this: <code>MakePoint( lon, lat )</code>.  The old way, by comparison was: <code>GeometryFromText('POINT(' || lon || ' ' || lat || ')',-1)</code></li>
<li>PostGIS can now support up to 4 dimensions (the fourth dimension being "measure" for use in linear referencing systems).</li>
<li>PostGIS can now create GML output directly, using the AsGML() function.</li>
<li>It is possible to enforce right-hand-rule on polygons with the ForceRHR() function.</li>
<li>A fast function to return the extent of a geometry column is now available, estimate_extent(). Previous, a complete table scan was required.</li>
<li>The index selectivity system has been upgraded to also calculate selectivity correctly during spatial joins (where both sides of the spatial overlap operator are columns: <code>select info from t1,t2 where t1.geom && t2.geom</code>)</li>
<li>The Intersection() function now respects z-value information where possible, rather than dropping it from the output.</li>
<li>It is possible to build polygon geometry inside PostGIS using the Polygonize() function.</li>
<li>PostgreSQL array functions can now be applied to collections of geometries, which can be built with the Accum() function.</li>
<li>SVG can be output directly from the database with the AsSVG() function.</li>
</ul>
<p>
There is a great deal of worthwhile new functionality available. Note that because the geometry representations have changed, a dump-and-restore is *required* for upgrade.
</p>
<P>The <code>utils/postgi_restore.pl</code> script should make upgrading a simpler task than it has sometimes been in the past.</p>

