---
layout: post
category: news
title: "PostGIS 1.0.0 Release Candidate #5"
author: Paul Ramsey
tags: [release, "1.0"]
date: "2005-03-25"
---

<p>
The fifth (and yet still hopefully final) release candidate is now available, with bug fixes from RC4.
Issues with segfaults in some functions and architectures have hopefully been resolved.
Please <A HREF='{{< loc "postgis.release_source">}}'>download</A> it and try it out!
</p>
