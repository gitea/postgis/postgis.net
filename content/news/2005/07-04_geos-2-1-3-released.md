---
layout: post
category: news
title: GEOS 2.1.3 Released
author: Paul Ramsey
tags: [release, "geos", "geos-2.1"]
date: "2005-07-04"
---

<p>The latest maintenance release of <A HREF='https://libgeos.org/'>GEOS</A> includes a number of bug fixes and performance improvements:
</p>
<ul>
<li>Win32/mingw build support
<li>Segfault fix in LinearRing and LineString constructors
<li>Segfault fix in Polygonizer
<li>XMLTester installed by default
<li>XMLTester code cleanup
<li>Fixed handling of collection input in GeometryFactory::buildGeometry
<li>Added shortcircuit test for Union operation
<li>Reduced useless Coordinate copies in CGAlgorithms::isPointInRing()
<li>Performance improvements in CGAlgorithms::isOnLine()
<li>Other minor performance improvements
<li>New Node::isIncidentEdgeInResult() method
<li>OverlayOp's PointBuilder performance improvement by reduction of LineIntersector calls
<li>Optimizations in Buffer operation
<li>Sever BUGFIX in DepthSegmentLT as suggested by Graeme Hiebert
</ul>

