---
layout: post
category: news
title: "GiST Indexes Improved for PostgreSQL 8.1"
author: Paul Ramsey
date: "2005-10-04"
---

<p>Refractions Research has led a project to fund the improvement of locking and
concurrency in the PostgreSQL GiST index subsystem.  The improvements will
dramatically improve read/write performance of PostGIS starting with the
PostgreSQL 8.1 release.
developers.
</p>
