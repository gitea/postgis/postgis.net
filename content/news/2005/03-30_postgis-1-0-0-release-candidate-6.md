---
layout: post
category: news
title: "PostGIS 1.0.0 Release Candidate #6"
author: Paul Ramsey
tags: [release, "1.0"]
date: "2005-03-30"
---

<p>
The sixth (and somehow yet still hopefully final) release candidate is now available, with bug fixes from RC5.
Issues with multi() and other functions and architectures have hopefully been resolved. This version requires a dump and restore. Please test the postgis_restore.pl script.
Please <a href="{{< loc "postgis.release_source">}}">download</a> it and try it out!
</p>
