---
layout: post
category: news
title: PostGIS 1.0.0 Release Candidate #2
author: Paul Ramsey
tags: [release, 1.0]
date: "2005-01-26"
---

<p>
The second release candidate is now available, with bug fixes from RC1.
Please <A HREF='{{< loc "postgis.release_source">}}'>download</A> it and try it out!
</p>
