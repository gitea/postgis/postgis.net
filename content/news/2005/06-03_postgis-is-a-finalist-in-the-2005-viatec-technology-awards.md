---
layout: post
category: news
title: PostGIS is a finalist in the 2005 VIATeC Technology Awards
author: Paul Ramsey
tags: ["awards"]
date: "2005-06-03"
---

<p><a href="http://www.viatec.ca/tech_awards.html"><img src="http://www.refractions.net/news/img/2005-VIATeC-Technology-Award.jpg" border="0"/></a></p>
<p>
Refractions Research is honoured that PostGIS is a finalist in the 2005 <a href="http://www.viatec.ca/tech_awards.html">VIATeC Technology Awards</a> for Product of the Year!
</p>

<p>
The VIATeC awards celebrate the accomplishments of BC's fastest-growing technology region, and recognise companies, individuals and technologies demonstrating excellence, innovation and commitment to the technology community.
</p>
