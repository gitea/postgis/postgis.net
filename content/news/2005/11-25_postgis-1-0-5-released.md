---
layout: post
category: news
title: PostGIS 1.0.5 Released
author: Paul Ramsey
tags: [release, 1.0]
date: "2005-11-25"
---

<p>The <A HREF='{{< loc "postgis.release_source">}}/postgis-1.0.5.tar.gz'>1.0.5 release</A> includes many small bug fixes and improved support for less common platforms.
</p>
<UL>
<LI>New "Reporting Bugs" chapter in manual
<LI>Segfault fix in shp2pgsql (utf8 encoding)
<LI>Fixed computation of null values fraction in analyzer
<LI>Fixed return code of shp2pgsql
<LI>Fixed handling of trailing dots in dbf numerical attributes
<LI>Schema aware postgis_proc_upgrade.pl, support for pgsql 7.2+
<LI>Fixed back-compatibility issue in loader (load of null shapefiles)
<LI>Fixed a small bug in the getPoint4d_p() low-level function
<LI>Fixed memory alignment problems
<LI>Speedup of serializer functions
<LI>Bug fix in force_4d, force_3dm and force_3dz functions
</UL>
