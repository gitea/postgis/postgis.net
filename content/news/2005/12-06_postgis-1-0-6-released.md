---
layout: post
category: news
title: PostGIS 1.0.6 Released
author: Paul Ramsey
tags: [release, 1.0]
date: "2005-12-06"
---

<p>The <A HREF='{{< loc "postgis.release_source">}}/postgis-1.0.6.tar.gz'>1.0.6 release</A> includes many small bug fixes and initial support for PostgreSQL 8.2.
</p>
<UL>
<LI>Fixed palloc(0) call in collection deserializer (only gives
  problem with --enable-cassert)
<LI>Fixed bbox cache handling bugs
<LI>Fixed geom_accum(NULL, NULL) segfault
<LI>Initial support for postgresql 8.2
<LI>Fixed segfault in addPoint()
<LI>Fixed short-allocation in lwcollection_clone()
<LI>Fixed bug in segmentize()
<LI>Added missing SRID mismatch checks in GEOS ops
<LI>Fixed bbox computation of SnapToGrid output
</UL>
