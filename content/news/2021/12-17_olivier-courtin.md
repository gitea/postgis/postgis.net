---
title: Olivier Courtin Farewell
layout: post
slug: "olivier-courtin"
category: news
tags: [developers, sfcgal]
author: Regina Obe
date: "2021-12-17"
---

In March 2020 we lost a long time PostGIS developer and friend, Olivier Courtin. The PostGIS 3.2.0 release is named in his honor.
<br />
<img src="/images/olivier_courtin.png" width="300px" alt="Olivier at Code Sprint"/>
<!--more-->
Olivier was a long time contributor to the PostGIS project and co-founder of [Oslandia](https://oslandia.com), a French Geospatial company focused on Open Source GIS and spatial data solutions. In more than a 10 year span, he contributed to PostGIS on many sides. He helped introduce and enhance many of the PostGIS output functions: ST_AsGML, ST_AsKML, ST_AsGeoJSON, and ST_AsSVG. He also helped implement 3D surface geometry support in PostGIS 2.0 and addition of advanced 2D and 3D functions via [SFCGAL](https://oslandia.gitlab.io/SFCGAL/).

Olivier was a charter member of the Open Source Geospatial Foundation [OSGeo](https://osgeo.org) and a vocal advocate of Open Source software. In addition to the PostGIS OSGeo project, he contributed to other OSGeo projects: QGIS, and MapServer. He also was a speaker in many geospatial and PostgreSQL conferences and was an organizer of OSGeo Code Sprint in Paris (2012) as well as several PostgreSQL Day France conferences.
