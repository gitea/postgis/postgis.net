---
title: PostGIS 3.1.4, 3.0.4 Released
layout: post
category: news
tags: [release,3.1,3.0]
author: Regina Obe
thumbnail:
date: "2021-09-11"
---

The PostGIS development team is pleased to provide
bug fix and performance enhancements
 3.1.4 and 3.0.4 for the 3.1, 3.0 stable branches.

<!--more-->
**3.1.4**
This release supports PostgreSQL 9.6-14.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.4.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.4/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.4.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.1.4.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.1)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.1.4.epub)

**3.0.4**
This release works with PostgreSQL 9.5-13  and GEOS >= 3.6
Designed to take advantage of features in PostgreSQL 12+ and Proj 6+

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.4.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.4/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.4.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.4.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.4.epub)


View all [closed tickets for  3.1.4, 3.0.4][1].

After installing the binaries or after running pg_upgrade:

For PostGIS 3.1, 3.0, 2.5
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.4 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.1.4&milestone=PostGIS+3.0.4&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
