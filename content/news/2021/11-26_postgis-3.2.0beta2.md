---
title: PostGIS 3.2.0beta2 Released
layout: post
category: news
tags: [release,3.2]
author: Regina Obe
thumbnail:
date: "2021-11-26"
---

The PostGIS Team is pleased to release the second beta of the upcoming PostGIS
3.2.0 release.

Best served with [PostgreSQL 14](https://www.postgresql.org/docs/14/release.html). This version of PostGIS utilizes the faster GiST building support API introduced in PostgreSQL 14. If compiled with recently released [GEOS 3.10.1](https://lists.osgeo.org/pipermail/geos-devel/2021-November/010556.html) you can take advantage of improvements in ST_MakeValid and numerous speed improvements.  This release also includes many additional functions and improvements for `postgis_raster` and `postgis_topology` extensions.

<!--more-->

**3.2.0beta2**

This release supports PostgreSQL 9.6-14, GEOS 3.6 and higher, and proj 4.9 and higher.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.0beta2.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.0beta2/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.2.0beta2.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.2.0beta2.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.2)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.2.0beta2.epub)

Details of this release can be found in NEWS file:
https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.2.0beta2/NEWS
Which is also included in the source tar ball.

If you come across any issues, feel free to report via our
[ticket tracker](https://trac.osgeo.org/postgis)  or mailing list with details as described in [SUPPORT](https://postgis.net/support/)

Changes since PostGIS 3.2.0beta1 release are listed below.

View all [closed tickets for  3.2.0beta2][1].

**Bug Fixes and Breaking Changes**

* #5016, loader (shp2pgsq): Respect LDFLAGS (Greg Troxel)
* #5005, ST_AsFlatGeoBuf crashes on tables when geometry
				 column is not the first column (Björn Harrtell)
* #5017, topology.ValidateTopology error relation "shell_check"
				 already exists (Sandro Santilli)

**Enhancements**

* #5018, pgsql2shp basic support for WITH CTE clause (Regina Obe)
* #5019, address_standardizer: Add support for pcre2 (Paul Ramsey)
* GH-647, ST_AsMVTGeom now uses faster clipping (Aliaksandr Kalenik)
* GH-648, ST_PixelAsCentroids, ST_PixelAsCentroid
				 reimplemented in C (Sergei Shoulbakov)

After installing the binaries or after running pg_upgrade:

For PostGIS 3.1, 3.0, 2.5
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.2.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
