---
title: PostGIS 3.2.0beta1 Released
layout: post
category: news
tags: [release,3.2]
author: Regina Obe
thumbnail:
---

The PostGIS Team is pleased to release the first beta of the upcoming PostGIS
3.2.0 release.

Best served with [PostgreSQL 14](https://www.postgresql.org/docs/14/release.html). This version of PostGIS utilizes the faster GiST building support API introduced in PostgreSQL 14. If compiled with recently released [GEOS 3.10.0](https://lists.osgeo.org/pipermail/geos-devel/2021-October/010484.html) you can take advantage of improvements in ST_MakeValid and numerous speed improvements.  This release also includes many additional functions and improvements for `postgis_raster` and `postgis_topology` extensions.

<!--more-->

**3.2.0beta1**

This release supports PostgreSQL 9.6-14.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.0beta1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.0beta1/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.2.0beta1.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.2.0beta1.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.2)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.2.0beta1.epub)

Details of this release can be found in NEWS file:
https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.2.0beta1/NEWS
Which is also included in the source tar ball.

If you come across any issues, feel free to report via our
[ticket tracker](https://trac.osgeo.org/postgis)  or mailing list with details as described in [SUPPORT](https://postgis.net/support/)

Changes since PostGIS 3.2.0alpa1 release are listed below.

View all [closed tickets for  3.2.0beta1][1].

**Bug Fixes and Breaking Changes**

 * #5012, Clean regress against released GEOS 3.10.0
	(Regina Obe, Paul Ramsey)
 * #5000, Turn off Window support in ST_AsMVT aggregate
	as no real use-case for it and it crashes with random input
	(Paul Ramsey)
 * #4994, shp2pgsql is sometimes missing the INSERT statements
	(Sandro Santilli)
 * #4990, Topology getfacecontainingpoint fails on i386 (Sandro Santilli)
 * #5008, Have ST_DWithin with EMPTY operand always return false
	(Sandro Santilli)
 * #5002, liblwgeom should build with warning flags by default
	(Sandro Santilli)

**Enhancements**

 * #4997, FlatGeobuf format input/output (Björn Harrtell)


After installing the binaries or after running pg_upgrade:

For PostGIS 3.1, 3.0, 2.5
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.2.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
