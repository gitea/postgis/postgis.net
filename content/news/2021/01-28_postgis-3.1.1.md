---
title: PostGIS 3.1.1
layout: post
category: news
tags: [release, 3.1]
author: Paul Ramsey
thumbnail:
date: "2021-01-28"
---

The PostGIS Team is pleased to release the release of PostGIS 3.1.1!

This release is a bug fix release, addressing issues found in the previous 3.1 release.

- [#4814](https://trac.osgeo.org/postgis/ticket/4814), Crash passing collection with only empty components to ST_MakeValid
- [#4818](https://trac.osgeo.org/postgis/ticket/4818), Make the VSICURL synthetic driver work as documented
- [#4825](https://trac.osgeo.org/postgis/ticket/4825), Unstable results from ST_MakeValid
- [#4823](https://trac.osgeo.org/postgis/ticket/4823), Avoid listing the same geometry in different collections

<!--more-->

**3.1.1**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.1/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.1.pdf)

Details of this release can be found in the NEWS file:

  https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.1.1/NEWS

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.1.1 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.1.1
