---
title: PostGIS 3.2.0alpha1 Released
layout: post
category: news
tags: [release,3.2]
author: Regina Obe
thumbnail:
date: "2021-09-11"
---

The PostGIS Team is pleased to release the first alpha of the upcoming PostGIS
3.2.0 release.

Best served with [PostgreSQL 14 beta3](https://www.postgresql.org/docs/14/release.html). This version of PostGIS utilizes the faster GiST building support API introduced in PostgreSQL 14. If compiled with the in-development [GEOS 3.10dev](https://lists.osgeo.org/pipermail/geos-devel/2021-September/010315.html) you can take advantage of improvements in ST_MakeValid.  This release also includes many additional functions and improvements for `postgis_raster` and `postgis_topology` extensions.

<!--more-->

**3.2.0alpha1**
This release supports PostgreSQL 9.6-14.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.0alpha1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.0alpha1/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.2.0alpha1.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.2.0alpha1.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.2)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.2.0alpha1.epub)

Details of this release can be found in NEWS file:
https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.2.0alpha1/NEWS
Which is also included in the source tar ball.

If you come across any issues, feel free to report via our
[ticket tracker](https://trac.osgeo.org/postgis)  or mailing list with details as described in [SUPPORT](https://postgis.net/support/)


View all [closed tickets for  3.2.0alpha1][1].

**Breaking changes**

 * #4824, Removed `--without-wagyu` build option. Using Wagyu is now mandatory to build with MVT support.
 * #4933, topology.GetFaceByPoint will not work with topologies having invalid edge linking.
 * #4981, ST_StartPoint support any geometry. No longer returns null for non-linestrings.
 * #4149, ST_AsMVTGeom now preserves more of original geometry's details at scale close to target extent.
 If you need previous simplifying behaviour, you can ST_Simplify the geometry in advance.
 (Darafei Praliaskouski)
 * Proj 4.9 or higher is required

**Enhancements**

 * #2592, Do not allow CreateTopology to define topologies with SRID < 0
(Sandro Santilli)
 * #3232, Prevent moving an isolated node to different face
(Sandro Santilli)
- Consider collection TopoGeometries while editing topology primitives.
(Sandro Santilli)
 * #3248, Prevent removing isolated edges if used in a TopoGeometry
(Sandro Santilli)
 * #3231, Prevent removing isolated nodes if used in a TopoGeometry
(Sandro Santilli)
 * #3239, Prevent headling topology edges if the connecting node is
used in the definition of a TopoGeometry (Sandro Santilli)
 * #4950, Speed up checking containing_face for nodes in ValidateTopology
(Sandro Santilli)
 * #4945, Multi-shell face check in ValidateTopology (Sandro Santilli)
 * #4944, Side-location conflict check in ValidateTopology (Sandro Santilli)
 * #3042, ValidateTopology check for edge linking (Sandro Santilli)
 * #3276, ValidateTopology check for face's mbr (Sandro Santilli)
 * #4936, Bounding box limited ValidateTopology (Sandro Santilli)
 * #4933, Speed up topology building in presence of big faces (Sandro Santilli)
 * #3233, ValidateTopology check for node's containing_face (Sandro Santilli)
 * #4830, ValidateTopology check for edges side face containment
(Sandro Santilli)
 * #4827, Allow NaN coordinates in WKT input (Paul Ramsey)
 * ST_Value() accepts resample parameter to add bilinear option (Paul Ramsey)
 * #3778, #4401, ST_Boundary now works for TIN and does not linearize curves (Aliaksandr Kalenik)
 * #4881, #4884, Store sign of edge_id for lineal TopoGeometry in relation table
to retain direction (Sandro Santilli)
 * #4628, Add an option to disable ANALYZE when loading shapefiles (Stefan Corneliu Petrea)
 * #4924, Faster ST_RemoveRepeatedPoints on large multipoints, O(NlogN) instead of O(N^2)
(Aliaksandr Kalenik, Darafei Praliaskouski)
 * #4925, fix ST_DumpPoints to not overlook points (Aliaksandr Kalenik)
 * ST_SRID(topogeometry) override, to speedup lookups (Sandro Santilli)
 * #2175, Avoid creating additional nodes when adding same closed
line to topology (Sandro Santilli)
 * #4974, Upgrade path for address_standardizer_data_us
(Jan Katins of Aiven, Regina Obe)
 * #4975, PostGIS upgrade change to not use temp tables (Jan Katins of Aiven)
 * #4981, ST_StartPoint support any geometry (Aliaksandr Kalenik)
 * #4799, Include srs in GeoJSON where it exists in spatial_ref_sys.
 * #4986, GIST indexes on Postgres 14 are now created faster using     Hilbert-sorting method. (Han Wang, Aliaksandr Kalenik, Darafei Praliaskouski, Giuseppe Broccolo)
 * 4949, Use `proj_normalize_for_visualization` to hand "axis swap" decisions (Paul Ramsey)

 **New features**
 * #4923, topology.ValidateTopologyRelation (Sandro Santilli)
 * #4933, topology.GetFaceContainingPoint (Sandro Santilli)
 * #2175, ST_Scroll (Sandro Santilli)
 * #4841, FindTopology to quickly get a topology record (Sandro Santilli)
 * #4869, FindLayer to quickly get a layer record (Sandro Santilli)
 * #4851, TopoGeom_addTopoGeom function (Sandro Santilli)
 * ST_MakeValid(geometry, options) allows alternative validity building
    algorithms with GEOS 3.10 (Paul Ramsey)
 * ST_InterpolateRaster() fills in raster cells between sample points
    using one of a number of algorithms (inverse weighted distance, average, etc)
    using algorithms from GDAL
    (Paul Ramsey)
 * ST_Contour() generates contour lines from raster values
    using algorithms from GDAL (Paul Ramsey)
 * ST_SetZ()/ST_SetM() fills in z/m coordinates of a geometry using data read
    from a raster (Paul Ramsey)
 * New postgis.gdal_vsi_options GUC allows out-db rasters on VSI network
    services to be accessed with authentication keys, etc. (Paul Ramsey)
 * ST_DumpSegments returns a set of segments of input geometry (Aliaksandr Kalenik)
 * #4859, ST_Point, ST_PointZ, ST_PointM, ST_PointZM, constructors
    with SRID parameter (Paul Ramsey)
 * #4808, ST_ClusterKMeans now supports max_radius argument. Use it when you're not sure what is
    the number of clusters but you know what the size of clusters should be. (Darafei Praliaskouski)

After installing the binaries or after running pg_upgrade:

For PostGIS 3.1, 3.0, 2.5
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.2.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
