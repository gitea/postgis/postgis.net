---
title: PostGIS 3.2.0rc1 Released
layout: post
category: news
tags: [release,3.2]
author: Regina Obe
thumbnail:
date: "2021-12-10"
---

The PostGIS Team is pleased to release the first rc of the upcoming PostGIS
3.2.0 release.

Best served with [PostgreSQL 14](https://www.postgresql.org/docs/14/release.html). This version of PostGIS can utilize the faster GiST building support API introduced in PostgreSQL 14. If compiled with recently released [GEOS 3.10.1](https://lists.osgeo.org/pipermail/geos-devel/2021-November/010556.html) you can take advantage of improvements in ST_MakeValid and numerous speed improvements.  This release also includes many additional functions and improvements for `postgis`, `postgis_raster` and `postgis_topology` extensions and a new input/export format [FlatGeobuf](https://flatgeobuf.org/).

<!--more-->

**3.2.0rc1**

This release supports PostgreSQL 9.6-14, GEOS 3.6 and higher, and proj 4.9 and higher.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.0rc1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.0rc1/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.2.0rc1.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.2.0rc1.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.2)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.2.0rc1.epub)

Details of this release can be found in NEWS file:
[https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.2.0rc1/NEWS](https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.2.0rc1/NEWS)
and is also included in the source tar ball.

If you come across any issues, feel free to report via our
[ticket tracker](https://trac.osgeo.org/postgis)  or mailing list with details as described in [SUPPORT](https://postgis.net/support/)

Due to some query performance degradation
with the new fast index build that requires PG14,
we have decided to disable the feature by default
until we get more user testing
as to the true impact of real-world queries.

If you are running PG14+, you can reenable it by doing

```postgres
ALTER OPERATOR FAMILY gist_geometry_ops_2d
	USING gist
  ADD FUNCTION 11 (geometry)
  geometry_gist_sortsupport_2d (internal);
```

To revert the change:
```postgres
ALTER OPERATOR FAMILY gist_geometry_ops_2d
	USING gist
  DROP FUNCTION 11 (geometry);
```

and then reindex your gist indexes.

View all [closed tickets for  3.2.0rc1][1].

After installing the binaries or after running pg_upgrade:

For PostGIS 3.1, 3.0, 2.5
Do the below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.2.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
