---
title: PostGIS 3.1.3
layout: post
category: news
tags: [release, 3.1]
author: Regina Obe
thumbnail:
date: "2021-07-02"
---

The PostGIS Team is pleased to release PostGIS 3.1.3!
Best Served with PostgreSQL 14 beta2.

This release is a bug fix release, addressing issues found in the previous 3.1 release.

- [#4929](https://trac.osgeo.org/postgis/ticket/4929), Fix missing error from GetRingEdges when invoked with unexistent topology or edge (Sandro Santilli)

- [#4927](https://trac.osgeo.org/postgis/ticket/4927),  Fix PostgreSQL 14 compile FuncnameGetCandidates changes needed to compile against PostgreSQL 14 beta2 or higher  (Regina Obe, Julien Rouhaud)

<!--more-->

**3.1.3**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.3.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.3/NEWS)

Details of this release can be found in the NEWS file:

  https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.1.3/NEWS

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.1.3 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.1.3
