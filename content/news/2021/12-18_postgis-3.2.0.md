---
title: PostGIS 3.2.0 Released
layout: post
category: news
tags: [release,3.2]
author: Regina Obe
date: "2021-12-18"
thumbnail:
---
The PostGIS Team is pleased to release PostGIS 3.2.0, the [Olivier Courtin](/2021/12/17/olivier-courtin/) release.

This release would not be possible without the various developers listed in the credits as well as the companies that provided funding and developer time.

Companies that contributed significantly to this release are:

* [CrunchyData](https://www.crunchydata.com/) Geometry processing functions (and improvements to [GEOS](https://libgeos.org), new raster functions, and address_standardizer pcre2 support
* [Kontur](https://www.kontur.io/) Speed improvements to raster, MVT, GiST indexing, better usability of geometry processing, and new radius clustering mode.
* [Netlab](https://strk.kbt.io/services.html) Continuous improvement of PostGIS topology, PostGIS upgrade plumbing,  PostGIS test plumbing, and CI bot management
* [NIBIO](https://nibio.no) Developer funding for topology improvements
* [Paragon Corporation](https://www.paragoncorporation.com) -- General CI bot management, release management, alignment of PostGIS with PostgreSQL versions.

<!--more-->

Best served with [PostgreSQL 14](https://www.postgresql.org/docs/14/release.html). This version of PostGIS can utilize the faster GiST building support API introduced in PostgreSQL 14. If compiled with recently released [GEOS 3.10.1](https://lists.osgeo.org/pipermail/geos-devel/2021-November/010556.html) you can take advantage of improvements in ST_MakeValid and numerous speed improvements.  This release also includes many additional functions and improvements for `postgis`, `postgis_raster` and `postgis_topology` extensions and a new input/export format [FlatGeobuf](https://flatgeobuf.org/).

**3.2.0**

This release supports PostgreSQL 9.6-14, GEOS 3.6 and higher, and proj 4.9 and higher.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.0.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.0/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.2.0.pdf) [de]({{< loc "postgis.release_docs">}}/postgis-3.2.0-de.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.2.0.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.2/) [de]({{< loc "site.root">}}docs/manual-3.2/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-3.2/postgis-ja.html) [ko_KR]({{< loc "site.root">}}docs/manual-3.2/postgis-ko_KR.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.2.0.epub)

Details of this release can be found in NEWS file:
[https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.2.0/NEWS](https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.2.0/NEWS)
and is also included in the source tar ball.

If you come across any issues, feel free to report via our
[ticket tracker](https://trac.osgeo.org/postgis)  or mailing list with details as described in [SUPPORT](https://postgis.net/support/)

Due to some query performance degradation
with the new fast index build that requires PG14,
we have decided to disable the feature by default
until we get more user testing
as to the true impact of real-world queries.

If you are running PG14+, you can reenable it by doing

```postgres
ALTER OPERATOR FAMILY gist_geometry_ops_2d
	USING gist
  ADD FUNCTION 11 (geometry)
  geometry_gist_sortsupport_2d (internal);
```

To revert the change:
```postgres
ALTER OPERATOR FAMILY gist_geometry_ops_2d
	USING gist
  DROP FUNCTION 11 (geometry);
```

and then reindex your gist indexes.

View all [closed tickets for  3.2.0][1].

After installing the binaries or after running pg_upgrade:

For upgrading from PostGIS 3.0, do the following which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.2.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
