---
title: PostGIS 3.1.2
layout: post
category: news
tags: [release, 3.1]
author: Paul Ramsey
thumbnail:
date: "2021-05-21"
---

The PostGIS Team is pleased to release the release of PostGIS 3.1.2!

This release is a bug fix release, addressing issues found in the previous 3.1 release.

- [#4871](https://trac.osgeo.org/postgis/ticket/4871), TopoGeometry::geometry cast returns NULL for empty TopoGeometry objects (Sandro Santilli)
- [#4826](https://trac.osgeo.org/postgis/ticket/4826), postgis_tiger_geocoder Better answers when no zip is provided (Regina Obe)
- [#4817](https://trac.osgeo.org/postgis/ticket/4817), handle more complex compound coordinate dystems (Paul Ramsey)
- [#4842](https://trac.osgeo.org/postgis/ticket/4842), Only do axis flips on CRS that have a "Lat" as the first column (Paul Ramsey)
- Support recent Proj versions that have removed pj_get_release (Paul Ramsey)
- [#4835](https://trac.osgeo.org/postgis/ticket/4835), Adjust tolerance for geodetic calculations (Paul Ramsey)
- [#4840](https://trac.osgeo.org/postgis/ticket/4840), Improper conversion of negative geographic azimuth to positive (Paul Ramsey)
- [#4853](https://trac.osgeo.org/postgis/ticket/4853), DBSCAN cluster not formed when recordset length equal to minPoints (Dan Baston)
- [#4863](https://trac.osgeo.org/postgis/ticket/4863), Update bboxes after scale/affine coordinate changes (Paul Ramsey)
- [#4876](https://trac.osgeo.org/postgis/ticket/4876), Fix raster issues related to PostgreSQL 14 tablefunc changes (Paul Ramsey, Regina Obe)
- [#4877](https://trac.osgeo.org/postgis/ticket/4877), mingw64 PostGIS / PostgreSQL 14 compile (Regina Obe, Tom Lane)
- [#4838](https://trac.osgeo.org/postgis/ticket/4838), Update to support Tiger 2020 (Regina Obe)
- [#4890](https://trac.osgeo.org/postgis/ticket/4890), Change Proj cache lifetime to last as long as connection (Paul Ramsey)
- [#4845](https://trac.osgeo.org/postgis/ticket/4845), Add Pg14 build support (Paul Ramsey)


<!--more-->

**3.1.2**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.2.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.2/NEWS)

Details of this release can be found in the NEWS file:

  https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.1.2/NEWS

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.1.2 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.1.2
