---
layout: post
category: news
title: PostGIS 1.4.0 Release
author: Paul Ramsey
tags: [release, 1.4]
date: "2009-07-24"
---

<p>The 1.4.0 release of PostGIS is <a href='{{< loc "postgis.release_source">}}'>now available</a>.
This is a significant release including many performance enhancements, bug fixes, improved internal testing system, more detailed documentation.</p>
<ul>
<li>API Stability - new policies surrounding minor releases</li>
<li>Support for PostgreSQL 8.4</li>
<li>New methods - ST_GeoHash, ST_LineCrossingDirection, ST_AsGeoJSON, ST_MinimumBoundingCircle to name a few</li>
<li>Significant performance improvements for many spatial operations, including ST_Union, ST_Intersects, ST_Contains, and ST_Within</li>
<li>Vastly improved documentation and reference manual</li>
<li>Improved build system, library structure, debugging framework, unit testing</li>
<li>More stable support for CIRCULARSTRING, COMPOUNDCURVE and CURVEPOLYGON</li>
</ul>

<p><a href="/news/20090724/">Read more...</a></p>

