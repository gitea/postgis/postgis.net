---
layout: post
category: news
title: PostGIS 1.4.1 Release
author: Paul Ramsey
tags: [release, 1.4]
date: "2009-12-17"
---

<p>The 1.4.1 release of PostGIS is <a href='{{< loc "postgis.release_source">}}'>now available</a>.
This is a minor release addressing a few issues that have been filed since the 1.4.0 release.</p>
<ul>
<li>Bug fixes for ST_LineCrossingDirection, ST_Union, ST_MakeLine</li>
<li>Improved parsing for higher dimensional geometries</li>
<li>Addressed memory issues with ST_As* functions</li>
<li>Corrected inconsistent results when using the ~= operator</li>
</ul>

<p><a href="/news/20091217/">Read more...</a></p>

