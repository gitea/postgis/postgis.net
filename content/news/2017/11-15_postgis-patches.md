---
title: PostGIS Patch Releases
layout: post
category: news
tags: [release, 2.3, 2.4]
author: Paul Ramsey
date: "2017-11-15"
thumbnail:
---

The PostGIS development team has uploaded bug fix releases
for the 2.3 and 2.4 stable branches.

**2.3.5**

* [Download]({{< loc "postgis.release_source">}}/postgis-2.3.5.tar.gz)
* [NEWS](https://svn.osgeo.org/postgis/tags/2.3.5/NEWS)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.3.5/ChangeLog)

**2.4.2**

* [Download]({{< loc "postgis.release_source">}}/postgis-2.4.2.tar.gz)
* [NEWS](https://svn.osgeo.org/postgis/tags/2.4.2/NEWS)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.4.2/ChangeLog)
