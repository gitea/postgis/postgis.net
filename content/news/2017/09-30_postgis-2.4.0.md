---
title: PostGIS 2.4.0 Released
layout: post
category: news
tags: [release, 2.4]
author: Regina Obe
thumbnail:
date: "2017-09-30"
---


The PostGIS development team is pleased to announce the release of PostGIS 2.4.0.
Best served with [PostgreSQL 10rc1](https://www.postgresql.org/docs/10/static/index.html)
and [pgRouting 2.5.0](https://github.com/pgRouting/pgrouting/releases/tag/v2.5.0).
See the full list of changes in the [news file](https://git.osgeo.org/gogs/postgis/postgis/raw/2.4.0/NEWS).


**IMPORTANT NOTES**

If you are upgrading from an existing PostGIS install, make sure after installing PostGIS binaries to do.

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you have additional postgishy extensions below upgrade them too

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
ALTER EXTENSION pgrouting UPDATE;
```

In order to have Map Box Vector Tiles support enabled, you'll need to compile with protobuf support
and pkg-config  to verify the correct minimum version of protobuf-c see [protobuf](https://github.com/protobuf-c/protobuf-c)
for details.  <a href="/docs/ST_FrechetDistance.html" target="_blank">```ST_FrechetDistance```</a>
function will not be enabled if PostGIS is compiled with lower than GEOS 3.7.0.
GEOS 3.7.0 is not released yet but is expected sometime next month.


* [Source Download]({{< loc "postgis.release_source">}}/postgis-2.4.0.tar.gz)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.4.0.tar.gz) (this version includes [Japanese]({{ site.root }}docs/manual-2.4/postgis-ja.html), [German]({{ site.root }}docs/manual-2.4/postgis-de.html), [Portuguese]({{ site.root }}docs/manual-2.4/postgis-br.html), and [Korean]({{ site.root }}docs/manual-2.4/postgis-ko_KR.html) translation files)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.0.pdf)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.0.epub)
* [ChangeLog](https://git.osgeo.org/gogs/postgis/postgis/raw/2.4.0/ChangeLog)
* pgsql help files for non-english languages: [ja]({{ postgis.dev_download }}/pgsql-help-2.4-ja.tar.gz), [de]({{ postgis.dev_download }}/pgsql-help-2.4-de.tar.gz), [br]({{ postgis.dev_download }}/pgsql-help-2.4-br.tar.gz), [ko_KR]({{ postgis.dev_download }}/pgsql-help-2.4-ko_KR.tar.gz)


<!--more-->

[Full News](https://git.osgeo.org/gogs/postgis/postgis/raw/2.4.0/NEWS)

Please [report bugs](http://postgis.net/support/) that you find in this release.

**New Features**

* 3822, Have postgis_full_version() also show and check version of
PostgreSQL the scripts were built against (Sandro Santilli)
* 2411, curves support in ST_Reverse (Sandro Santilli)
* 2951, ST_Centroid for geography (Danny Götte)
* 3788, Allow postgis_restore.pl to work on directory-style (-Fd) dumps
(Roger Crew)
* 3772, Direction agnostic ST_CurveToLine output (Sandro Santilli / KKGeo)
* 2464, ST_CurveToLine with MaxError tolerance (Sandro Santilli / KKGeo)
* 3599, Geobuf output support via ST_AsGeobuf (Björn Harrtell)
* 3661, Mapbox vector tile output support via ST_AsMVT (Björn Harrtell / Carto)
* 3689, Add orientation checking and forcing functions (Dan Baston)
* 3753, Gist penalty speed improvements for 2d and nd points
(Darafei Praliaskouski)
* 3677, ST_FrechetDistance (Shinichi Sugiyama)
- Most aggregates (raster and geometry),
and all stable / immutable (raster and geometry) marked as parallel safe
* 2249, ST_MakeEmptyCoverage for raster (David Zwarg, ainomieli)
* 3709, Allow signed distance for ST_Project (Darafei Praliaskouski)
* 524, Covers support for Polygon-on-polygon, line on line,
point on line for geography (Danny Götte)

**Enhancements**

* Support for PostgreSQL 10
* 3645, Avoid loading logically deleted records from shapefiles
* 3747, Add zip4 and address_alphanumeric as attributes
         to norm_addy tiger_geocoder type.
* 3748, address_standardizer lookup tables update
         so pagc_normalize_address better standardizes abbreviations
* 3647, better handling of noding in ST_Node using GEOSNode
        (Wouter Geraedts)
* 3684, Update to EPSG register v9
        (Even Rouault)
* 3830, Fix initialization of incompatible type (>=9.6) address_standardizer
* 3662, Make shp2pgsql work in debug mode by sending debug to stderr
* 3405, Fixed memory leak in lwgeom_to_points
* 3832, Support wide integer fields as int8 in shp2pgsql
* 3841, Deterministic sorting support for empty geometries in btree geography
* 3844, Make = operator a strict equality test, and < > to rough "spatial sorting"
* 3855, ST_AsTWKB memory and speed improvements

**Breaking Changes**

* Dropped support for PostgreSQL 9.2.
* 3810, GEOS 3.4.0 or above minimum required to compile
* Most aggregates now marked as parallel safe,
  which means most aggs have to be dropped / recreated.
  If you have views that utilize PostGIS aggs,
  you'll need to drop before upgrade and recreate after upgrade

* B-Tree index operators < = > changed to provide better spatial locality on sorting
and have expected behavior on GROUP BY. It now works on exact geometry instead of bounding box.
If you have btree index for geometry or geography, you need to REINDEX it, or review
if it was created by accident and needs to be replaced with GiST index.
If your code relies on old left-to-right box compare ordering,
update it to use << >> operators.

* `_ST_DumpPoints` removed, was no longer needed after PostGIS 2.1.0
when ST_DumpPoints got reimplemented in C

View all [closed tickets for 2.4.0][1].
  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.4.0&order=priority
