---
title: PostGIS 2.1.9 Released EOL
slug: "postgis-2.1.9"
layout: post
category: news
tags: [release, 2.1, EOL]
author: Paul Ramsey
thumbnail:
date: "2017-09-19"
---

The PostGIS development team has uploaded the final release
of the PostGIS 2.1 branch. The 2.1 branch is now end-of-life.
As befits a patch release, the focus is on bugs and breakages.

* [Download]({{< loc "postgis.release_source">}}/postgis-2.1.9.tar.gz)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.1.9.tar.gz)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.1.9.pdf)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.1.9/ChangeLog)

<!--more-->

Please [report bugs](http://postgis.net/support/) that you find in this release.

**Bug Fixes and Improvements**


* [2232](https://trac.osgeo.org/postgis/ticket/2232), avoid accumulated error in SVG rounding
* [2283](https://trac.osgeo.org/postgis/ticket/2283), Import nested holey multipolygons correctly
* [3222](https://trac.osgeo.org/postgis/ticket/3222), Fix uninitialized stddev in stats computation
* [3196](https://trac.osgeo.org/postgis/ticket/3196), do not let DropTopology drop non-topology schemes
* [3198](https://trac.osgeo.org/postgis/ticket/3198), ST\_AddEdgeModFace docs report wrong side of new face
* [3245](https://trac.osgeo.org/postgis/ticket/3245), Ensure lwgeom/geos returns are fully owned
* [3280](https://trac.osgeo.org/postgis/ticket/3280), Fix topology import of almost collinear linestrings
* [3281](https://trac.osgeo.org/postgis/ticket/3281), Do not export liblwgeom symbols from the PostgreSQL module
* [3351](https://trac.osgeo.org/postgis/ticket/3351), Set endnodes isolation on ST\_RemoveIsoEdge
* [3355](https://trac.osgeo.org/postgis/ticket/3355), Fix geography ST\_Segmentize() does not add geodetic box ST\_Intersects and other relationship functions always return false as result.
* [3359](https://trac.osgeo.org/postgis/ticket/3359), Fix toTopoGeom loss of low-id primitives from TopoGeometry definition
* [3375](https://trac.osgeo.org/postgis/ticket/3375), crash in repeated point removal for collection(point)
* [3378](https://trac.osgeo.org/postgis/ticket/3378), Fix handling of hierarchical TopoGeometries with of multiple topologies
* [3389](https://trac.osgeo.org/postgis/ticket/3389), Buffer overflow in lwgeom\_to\_geojson
* [3393](https://trac.osgeo.org/postgis/ticket/3393), ST\_Area NaN for some polygons
* [3436](https://trac.osgeo.org/postgis/ticket/3436), memory handling mistake in ptarray\_clone\_deep
* [3461](https://trac.osgeo.org/postgis/ticket/3461), ST\_GeomFromKML crashes Postgres when there are innerBoundaryIs and no outerBoundaryIs
* [3429](https://trac.osgeo.org/postgis/ticket/3429), upgrading to 2.2 or 2.3 can cause crash/hang on some platforms
* [3565](https://trac.osgeo.org/postgis/ticket/3565), ST\_SetPoint can crash backend
* [3579](https://trac.osgeo.org/postgis/ticket/3579), Crash in LWGEOM2GEOS
* [3583](https://trac.osgeo.org/postgis/ticket/3583), Crash in ST\_GeomFromGeoJSON on malformed multipolygon
* [3607](https://trac.osgeo.org/postgis/ticket/3607), Fix inconsistency with multilinestring in ST\_LocateBetweenElevations (Artur Zakirov)
* [3608](https://trac.osgeo.org/postgis/ticket/3608), Fix crash passing -W UTF-8 to shp2pgsql (Matt Amos)
* [3644](https://trac.osgeo.org/postgis/ticket/3644), Deadlock on interrupt
* [3774](https://trac.osgeo.org/postgis/ticket/3774), Trigonometric length for CompoundCurves
* [3731](https://trac.osgeo.org/postgis/ticket/3731), Crash on very small table of homogenous data

See the full list of changes in the [news file](http://svn.osgeo.org/postgis/tags/2.1.9/NEWS) and please [report bugs](https://trac.osgeo.org/postgis) that you find in the release.
Binary packages will appear in [repositories](http://postgis.net/install) over the coming weeks as packagers roll out builds.

View all [closed tickets for 2.1.9][1].
  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.1.9&order=priority
