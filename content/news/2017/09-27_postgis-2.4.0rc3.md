---
title: PostGIS 2.4.0rc3 Released
layout: post
category: news
tags: [release, 2.4]
author: Regina Obe
thumbnail:
date: "2017-09-27"
---

The PostGIS development team is pleased to announce the release of PostGIS 2.4.0rc3.
Best served with [PostgreSQL 10rc1](https://www.postgresql.org/docs/10/static/index.html)
and [pgRouting 2.5.0](https://github.com/pgRouting/pgrouting/releases/tag/v2.5.0).
See the full list of changes in the [news file](https://git.osgeo.org/gogs/postgis/postgis/raw/2.4.0rc3/NEWS).

This will be the final rc before we have our 2.4.0 release.

We encourage everyone to test and in particular package maintainers
to ensure no surprises at final release time.

**IMPORTANT NOTES**

If you are upgrading from an existing PostGIS install, make sure after installing PostGIS binaries to do.

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you have additional postgishy extensions below upgrade them too

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
ALTER EXTENSION pgrouting UPDATE;
```



* [Source Download]({{< loc "postgis.release_source">}}/postgis-2.4.0rc3.tar.gz)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.4.0rc3.tar.gz) (this version includes Japanese, German, Portugese and Korean single html files)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.0rc3.pdf)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.0rc3.epub)
* pgsql help files for non-english languages: [ja]({{ postgis.dev_download }}/pgsql-help-2.4-ja.tar.gz), [de]({{ postgis.dev_download }}/pgsql-help-2.4-de.tar.gz), [br]({{ postgis.dev_download }}/pgsql-help-2.4-br.tar.gz), [ko_KR]({{ postgis.dev_download }}/pgsql-help-2.4-ko_KR.tar.gz)
* [ChangeLog](https://git.osgeo.org/gogs/postgis/postgis/raw/2.4.0rc3/ChangeLog)

<!--more-->

[Full News](https://git.osgeo.org/gogs/postgis/postgis/raw/2.4.0rc3/NEWS)

**Changes since PostGIS 2.4.0rc2 release**

* #3781 st_contains cannot handle curvepolygon / compoundcurve consisting of only straight lines
* #3866 server crashing from large values in ST_AsTWKB
* #3818 Allow triangles unclosed in M to be WKT parsed
* several packaging changes regarding checking trailing blanks
