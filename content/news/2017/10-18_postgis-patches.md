---
title: PostGIS Patch Releases 2.2.6, 2.3.4, 2.4.1
layout: post
category: news
tags: [release, 2.2, 2.3, 2.4]
author: Paul Ramsey
thumbnail:
---

The PostGIS development team has uploaded bug fix releases
for the 2.2, 2.3 and 2.4 stable branches.

**2.2.6**

* [Download]({{< loc "postgis.release_source">}}/postgis-2.2.6.tar.gz)
* [NEWS](https://svn.osgeo.org/postgis/tags/2.2.6/NEWS)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.2.6/ChangeLog)

**2.3.4**

* [Download]({{< loc "postgis.release_source">}}/postgis-2.3.4.tar.gz)
* [NEWS](https://svn.osgeo.org/postgis/tags/2.3.4/NEWS)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.3.4/ChangeLog)

**2.4.1**

* [Download]({{< loc "postgis.release_source">}}/postgis-2.4.1.tar.gz)
* [NEWS](https://svn.osgeo.org/postgis/tags/2.4.1/NEWS)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.4.1/ChangeLog)
