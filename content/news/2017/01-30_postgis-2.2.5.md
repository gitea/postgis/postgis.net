---
title: PostGIS 2.2.5 Released
layout: post
category: news
tags: [release, 2.2]
author: Regina Obe
thumbnail:
date: "2017-01-30"
---

The PostGIS development team is pleased to announce the release of PostGIS 2.2.5
As befits a patch release, the focus is on bugs and breakages.

* [Download]({{< loc "postgis.release_source">}}/postgis-2.2.5.tar.gz)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.2.5.tar.gz)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.2.5.pdf)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.2.5/ChangeLog)

<!--more-->

Please [report bugs](http://postgis.net/support/) that you find in this release.

**Bug Fixes and Improvements**

 * [3418](https://trac.osgeo.org/postgis/ticket/3418), KNN recheck in 9.5+ fails with index returned tuples in wrong order
 * [3680](https://trac.osgeo.org/postgis/ticket/3680), PostGIS upgrade scripts missing GRANT for views

See the full list of changes in the [news file](http://svn.osgeo.org/postgis/tags/2.2.5/NEWS) and please [report bugs](https://trac.osgeo.org/postgis) that you find in the release.
Binary packages will appear in [repositories](http://postgis.net/install) over the coming weeks as packagers roll out builds.

View all [closed tickets for 2.2.5][1].
  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.2.5&order=priority
