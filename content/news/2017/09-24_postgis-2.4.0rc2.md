---
title: PostGIS 2.4.0rc2 Released
layout: post
category: news
tags: [release, 2.4]
author: Regina Obe
date: "2017-09-24"
thumbnail:
---

The PostGIS development team is pleased to announce the release of PostGIS 2.4.0rc2.
Best served with [PostgreSQL 10rc1](https://www.postgresql.org/docs/10/static/index.html)
and [pgRouting 2.5.0](https://github.com/pgRouting/pgrouting/releases/tag/v2.5.0).
See the full list of changes in the [news file](https://git.osgeo.org/gogs/postgis/postgis/raw/2.4.0rc2/NEWS).

We encourage everyone to test and in particular package maintainers
to insure no surprises at final release time.

**IMPORTANT NOTES**

If you are upgrading from an existing PostGIS install, make sure after installing PostGIS binaries to do.

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you have additional postgishy extensions below upgrade them too

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
ALTER EXTENSION pgrouting UPDATE;
```



* [Source Download]({{< loc "postgis.release_source">}}/postgis-2.4.0rc2.tar.gz)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.4.0rc2.tar.gz) (this version includes Japanese, German, Portugese and Korean single html files)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.0rc2.pdf)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.0rc2.epub)
* pgsql help files for non-english languages: [ja]({{ postgis.dev_download }}/pgsql-help-2.4-ja.tar.gz), [de]({{ postgis.dev_download }}/pgsql-help-2.4-de.tar.gz), [br]({{ postgis.dev_download }}/pgsql-help-2.4-br.tar.gz), [ko_KR]({{ postgis.dev_download }}/pgsql-help-2.4-ko_KR.tar.gz)
* [ChangeLog](https://git.osgeo.org/gogs/postgis/postgis/raw/2.4.0rc2/ChangeLog)

<!--more-->

[Full News](https://git.osgeo.org/gogs/postgis/postgis/raw/2.4.0rc2/NEWS)

**Changes since PostGIS 2.4.0rc1 release**

- Numerous fixes to ST_AsMVT, ST_AsGeoBuf
- ST_AsTWKB speed and memory performance enhancments
- ST_RemoveRepeatedPoints speed improvements
- ST_ConcaveHull bug fix
