---
title: PostGIS 2.3.2 Released
layout: post
category: news
tags: [release, 2.3]
author: Regina Obe
thumbnail:
date: "2017-01-31"
---

The PostGIS development team is pleased to announce the release of PostGIS 2.3.2
As befits a patch release, the focus is on bugs and breakages.
Best served with [PostgreSQL 9.6.1+](https://www.postgresql.org/about/news/1712/)
and [pgRouting 2.3.2](https://lists.osgeo.org/pipermail/pgrouting-users/2017-January/002304.html).

* [Source Download]({{< loc "postgis.release_source">}}/postgis-2.3.2.tar.gz)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.3.2.tar.gz)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.3.2.pdf)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.3.2.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.3.2/ChangeLog)

<!--more-->

Please [report bugs](http://postgis.net/support/) that you find in this release.

**Bug Fixes and Improvements**

 * [3418](https://trac.osgeo.org/postgis/ticket/3418), KNN recheck in 9.5+ fails with index returned tuples in wrong order
 * [3675](https://trac.osgeo.org/postgis/ticket/3675), Relationship functions not using an index in some cases
 * [3680](https://trac.osgeo.org/postgis/ticket/3680), PostGIS upgrade scripts missing GRANT for views
 * [3683](https://trac.osgeo.org/postgis/ticket/3683), Unable to update postgis after postgres pg_upgrade going from < 9.5 to pg > 9.4
 * [3688](https://trac.osgeo.org/postgis/ticket/3688), `ST_AsLatLonText`: round minutes

See the full list of changes in the [news file](http://svn.osgeo.org/postgis/tags/2.3.2/NEWS) and please [report bugs](https://trac.osgeo.org/postgis) that you find in the release.
Binary packages will appear in [repositories](http://postgis.net/install) over the coming weeks as packagers roll out builds.

View all [closed tickets for 2.3.2][1].
  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.3.2&order=priority
