---
layout: post
category: news
title: PostGIS 1.4.2 Release
author: Paul Ramsey
tags: [release, 1.4, EOL]
date: "2010-03-11"
---

<p>The 1.4.2 release of PostGIS is <a href='{{< loc "postgis.release_source">}}'>now available</a>.
This is a minor release addressing a few issues that have been filed since the 1.4.1 release.</p>
<ul>
<li>Fix to allow dumping tables with invalid geometries</li>
<li>Fixes to install/uninstall scripts</li>
<li>Fixes to ST_AsSVG, ST_SetPoint, ST_AddPoint, ST_RemovePoint, ST_AsGML, and ST_Line_Substring</li>
</ul>

