---
layout: post
category: news
title: PostGIS 1.5.0 Release
author: Paul Ramsey
tags: [release, 1.5]
date: "2010-02-04"
---

<p>The 1.5.0 release of PostGIS is <a href='{{< loc "postgis.release_source">}}'>now available</a>.
This is a major release adding a significant number of new features including:</p>
<ul>
<li>New "geography" type for managing geodetic (lat/lon) data
<li>Performance-enhanced distance calculations
<li>GML and KML format readers
<li>Improved shape loading GUI
<li>And more!
</ul>

