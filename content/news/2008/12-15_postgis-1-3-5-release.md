---
layout: post
category: news
title: PostGIS 1.3.5 Release
author: Paul Ramsey
tags: [release, 1.3]
date: "2008-12-15"
---

<p>The 1.3.5 release of PostGIS is <a href='{{< loc "postgis.release_source">}}'>now available</a>.
This release is a small bug fix release, including a critical fix for using PostGIS with Mapserver and LINE layers.</p>

