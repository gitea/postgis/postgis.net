---
layout: post
category: news
title: PostGIS Improves Your Golf Game?
author: Paul Ramsey
date: "2008-03-14"
---

<p>We recently heard from the folks at <a href="http://www.westinsavannah.com/golf.asp">Savannah Harbor Golf Club & Spa</a> about how they used PostGIS to help map and report on their course. Check out the story: <a href="http://www.golfcoursenews.com/news/news.asp?ID=4012">here</a>
</p>
