---
layout: post
category: news
title: History of PostGIS
author: Paul Ramsey
date: "2008-02-09"
---

<p>There is a
<a href="http://www.refractions.net/products/postgis/history/">short history of PostGIS</a>, from conception to current state,
available now on the Refractions web site.  Have a look!
</p>
