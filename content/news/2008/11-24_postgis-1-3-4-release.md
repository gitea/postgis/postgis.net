---
layout: post
category: news
title: PostGIS 1.3.4 Release
author: Paul Ramsey
tags: [release, 1.3]
date: "2008-11-24"
---

<p>The 1.3.4 release of PostGIS is <a href='{{< loc "postgis.release_source">}}'>now available</a>.
This release includes many bug fixes, and some useful feature enhancements.</p>

<ul>
<li>ST_AsGeoJSON() function
<li>PostgreSQL 8.4 support
<li>Performance improvements with GEOS 3.1
<li>Memory leak fixes
<li>Documentation improvements
<li>More reliable handling of curve types
</ul>

<p><a href="/news/20081124/">Read more...</a></p>

