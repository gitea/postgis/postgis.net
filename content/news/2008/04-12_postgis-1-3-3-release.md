---
layout: post
category: news
title: PostGIS 1.3.3 Release
author: Paul Ramsey
tags: [release, 1.3]
date: "2008-04-12"
---

<p>The 1.3.3 release of PostGIS is <a href='{{< loc "postgis.release_source">}}'>now available</a>.  This release includes bug fixes and some minor feature enhancements.</p>

<ul>
<li>Improvements in the shp2pgsql/pgsql2shp utilities</li>
<ul>
<li>Better windows support</li>
<li>DBF-only loading support</li>
<li>Support for OS/X 10.5</li>
<li>Output for Date types in DBF</li>
</ul>
<li>New function ST_SimplifyPreserveTopology( geometry, float8) to simplify polygons without creating invalid geometry</li>
<li>KML support updates (from Eduin Carillo)</li>
<li>SVG support updates (from Marco Hugentobler)</li>
</ul>

