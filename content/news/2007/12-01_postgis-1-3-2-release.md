---
layout: post
category: news
title: PostGIS 1.3.2 Release
author: Paul Ramsey
tags: [release, 1.3]
date: "2007-12-01"
---

<p>The 1.3.2 release of PostGIS is <a href='{{< loc "postgis.release_source">}}'>now available</a>.  This release includes bug fixes and some minor feature enhancements.</p>

<ul>
<li>Improvements in the TIGER geocoder </li>
<li>Fix to ST_EndPoint() crasher bug </li>
<li>Modified ST_AsGML() improving v2 and adding v3 support </li>
<li>Fix to ensure ST_Envelope() returns valid geometries </li>
<li>Change JDBC build to use Ant </li>
<li>Fix for better OS/X support </li>
<li>Fix to WKB parser to do simple validity checks  </li>
</ul>

