---
layout: post
category: news
title: PostGIS 1.1.7 Release
author: Paul Ramsey
tags: [release, 1.1, EOL]
date: "2007-01-31"
---

<p>The 1.1.7 release of PostGIS is <a href='{{< loc "postgis.release_source">}}'>now available</a>.
This is a bug fix release, backporting improvements from the 1.2.x series
to the 1.1.x series.</p>

<ul>
<li>Fix for detection of GEOS versions.
<li>Fix for better cross platform support.
<li>Fix for JDBC compatibility with PostgreSQL 8.2.
<li>Fix for GCJ Java support.
<li>Fix for GiST index support with PostgreSQL 8.2.
</ul>


