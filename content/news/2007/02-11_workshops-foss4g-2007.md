---
layout: post
category: news
title: Workshops @ FOSS4G 2007
author: Paul Ramsey
tags: ["foss4g", "events"]
date: "2007-02-11"
---

<p>The <a href="http://www.foss4g2007.org/workshops.html">FOSS4G2007 Call for Workshops</a> is open! <a href="http://www.foss4g2007.org/">FOSS4G</a> is a great place to learn about how people are using PostGIS and other open source software to get their work done. Or to teach people the cool techniques you use.</p>

<ul>
<li>Paul Ramsey has submitted his "Introduction to PostGIS" workshop.
<li>Do you have a PostGIS-themed workshop idea?
</ul>

<p>Submit your workshop idea, or make plans to come to <a href="http://www.foss4g2007.org/">FOSS4G</a> and attend!
September 24-27, 2007 in Victoria, British Columbia.</p>

