---
title: PostGIS 2.2.4 Released
layout: post
category: news
tags: [release, 2.2]
author: Regina Obe
thumbnail:
date: "2016-11-26"
---

The PostGIS development team is pleased to announce the release of PostGIS 2.2.4
As befits a patch release, the focus is on bugs and breakages.

* [Download]({{< loc "postgis.release_source">}}/postgis-2.2.4.tar.gz)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.2.4.tar.gz)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.2.4.pdf)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.2.4/ChangeLog)

<!--more-->

If you are a Mac user using newer XCode 8, you might run into some compilation issues
as a result of recent changes in XCode.  Refer to [https://trac.osgeo.org/postgis/ticket/3643](https://trac.osgeo.org/postgis/ticket/3643)
for a patch you can use.


Please [report bugs](http://postgis.net/support/) that you find in this release.

**Bug Fixes and Improvements**

 * [1973](https://trac.osgeo.org/postgis/ticket/1973), st_concavehull() returns sometimes empty geometry collection. Fix from gde
 * [3656](https://trac.osgeo.org/postgis/ticket/3656), Fix upgrade of aggregates from 2.2 or lower version
 * [3501](https://trac.osgeo.org/postgis/ticket/3501), add raster constraint max extent exceeds array size limit for large tables, change to use ST_Extent
 * [3659](https://trac.osgeo.org/postgis/ticket/3659), Crash caused by raster GUC define after CREATE EXTENSION using wrong memory context. (manaeem)


See the full list of changes in the [news file](http://svn.osgeo.org/postgis/tags/2.2.4/NEWS) and please [report bugs](https://trac.osgeo.org/postgis) that you find in the release.
Binary packages will appear in [repositories](http://postgis.net/install) over the coming weeks as packagers roll out builds.

View all [closed tickets for 2.2.4][1].
  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.2.4&order=priority
