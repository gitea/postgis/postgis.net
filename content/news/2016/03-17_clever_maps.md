---
title: Clever Maps
layout: post
category: casestudy
tags: [web apps, cz]
author: Michal Zimmermann
date: "2016-03-17"
---

Clever°Maps' is a three years old startup based in the Czech Republic. We create web apps for four distinct market segments:

1. business/location inteligence - helping companies to make decisions based on data, not on feelings
2. farming - simplifying agenda and everyday work
3. road infrastructure administration - settlement of land property rights, treaty evidence, speeding up the whole administrative process
4. assets administration - treaty management, land purchases

<!--more-->

All our activities take advantage of PostgreSQL as a great DBMS and PostGIS as a tool to query data our apps need. These days we keep a complete monthly updated copy of the whole Czech cadastre together with LPIS (Land Parcel Identification System), that is more than 650 GB of data. We use PostGIS for much more than simple queries (although talking millions of parcel lots it is hard to tell them simple anymore):

1. it helped us identify potential issues related to land property rights in one of the regions in the Czech Republic
2. we relied on PostGIS heavily during the French LPIS update in 2015
3. it was used during the Czech LPIS controls in 2015 where tens of operators were accessing the database, commiting changes to vector layers
4. we use it to intersect LPIS lots with cadastral lots to find relations between those two datasets
5. we use it to parse the national cadastral topological data format (called VFK) - a pretty interesting task that was
6. thanks to continual data update we can watch for treaty/property changes and notify our customers as soon as possible
7. it notifies farmers who use our app of any violation of fertilizers usage on their land (e.g. some substances cannot be used near water sources)

Recently our BI team pushed the limits of PostgreSQL/PostGIS trying to use it on ~180M rows of spatial data. They eventually ended up using Amazon Redshift, but the whole geoprocessing is still done in PostGIS beforehand.

<http://clevermaps.cz>
