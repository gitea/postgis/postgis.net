---
title: PostGIS 2.2.2 Released
layout: post
category: news
tags: [release, 2.2]
author: Paul Ramsey
thumbnail:
date: "2016-03-22"
---

The PostGIS development team is happy to release patch for PostGIS 2.2, the 2.2.2 release. As befits a patch release, the focus is on bugs and breakages.

**Bug Fixes and Improvements**

  * [#3463](http://trac.osgeo.org/postgis/ticket/3463), Fix crash on face-collapsing edge change
  * [#3422](http://trac.osgeo.org/postgis/ticket/3422), Improve `ST_Split` robustness on standard precision double systems (arm64, ppc64el, s390c, powerpc, ...)
  * [#3427](http://trac.osgeo.org/postgis/ticket/3427), Update `spatial_ref_sys` to EPSG version 8.8
  * [#3433](http://trac.osgeo.org/postgis/ticket/3433), `ST_ClusterIntersecting` incorrect for MultiPoints
  * [#3435](http://trac.osgeo.org/postgis/ticket/3435), `ST_AsX3D` fix rendering of concave geometries
  * [#3436](http://trac.osgeo.org/postgis/ticket/3436), memory handling mistake in ptarray_clone_deep
  * [#3437](http://trac.osgeo.org/postgis/ticket/3437), `ST_Intersects` incorrect for MultiPoints
  * [#3461](http://trac.osgeo.org/postgis/ticket/3461), `ST_GeomFromKML` crashes Postgres when there are innerBoundaryIs and no outerBoundaryIs
  * [#3429](http://trac.osgeo.org/postgis/ticket/3429), upgrading to 2.3 or from 2.1 can cause loop/hang on some platforms
  * [#3460](http://trac.osgeo.org/postgis/ticket/3460), `ST_ClusterWithin` "Tolerance not defined" error after upgrade
  * [#3490](http://trac.osgeo.org/postgis/ticket/3490), Raster [data restore](http://postgis.net/docs/manual-2.2/RT_FAQ.html#faq_raster_data_not_restore) issues, materialized views. Added scripts `postgis_proc_set_search_path.sql`, `rtpostgis_proc_set_search_path.sql`
  * [#3426](http://trac.osgeo.org/postgis/ticket/3426), failing `POINT EMPTY` tests on fun architectures

See the full list of changes in the [news file](http://svn.osgeo.org/postgis/tags/2.2.2/NEWS) and please [report bugs](https://trac.osgeo.org/postgis) that you find in the release.

* [postgis-2.2.2.tar.gz]({{< loc "postgis.release_source">}}/postgis-2.2.2.tar.gz)
* [ChangeLog](https://svn.osgeo.org/postgis/tags/2.2.2/ChangeLog)

Binary packages will appear in [repositories](http://postgis.net/install) over the coming weeks as packagers roll out builds.

View all [closed tickets for 2.2.2][1].
  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.2.2&order=priority
