---
title: Plenario (Univeristy of Chicago)
layout: post
category: casestudy
tags: [university, urban research, us]
author: William Engler
date: "2016-03-29"
thumbnail: uccd-logo.png
---

The [Urban Center for Computation and Data (UrbanCCD)](http://www.urbanccd.org/#urbanccd) is a research initiative of Argonne National Laboratory and the Computation Institute of the University of Chicago. We create computational tools to better understand cities. One of these is [Plenario](http://plenar.io/), our hub for open geospatial data.
PostGIS makes the spatial operations at the heart of Plenario possible.

<!--more-->

# What is Plenario?

Over the last four years, government agencies have published more and more of their data on open data portals.
However, someone who wants to survey all of the data available for their city might have to dig through the websites of their city, county, state, and federal governments to bring together all the datasets they need.

Plenario collects datasets from disparate sources and lets users find all data available for their area of interest with a single spatial query. In addition to aiding data discovery, Plenario can offload some data preparation work by applying filters and aggregates to data before delivering it to the user.

# How do we use PostGIS?

1.    Ingestion: Users submit links to spatial datasets they want to see in Plenario. Those can be CSVs with events that have a location and time column (think crimes or building permits) or ESRI shapefiles (think neighborhood boundaries or bike lanes). Using tools from the GDAL suite, we ETL these datasets into our postgres database with a common spatial schema so that the datasets are ready to query.

2.    Query: A Python web application translates incoming API calls into SQL queries using SQLAlchemy (and its GeoAlchemy extension). Discovery queries accept spatial ranges formatted as GeoJSON to enable queries like “Please show me all available datasets for the South Side of Chicago between 2012 and 2014.” Other queries combine preloaded datasets to enable queries like "Give me all health inspection complaints filed in police district 12" or "Give me a count of filming permits per neighborhood of Chicago in the last year."

# How PostGIS fits into our organization

In addition to making Plenario technically possible, the free and open governance of PostGIS makes it an operational fit.
All of the tools we build at UrbanCCD are free and open source.
A vital part of our mission is to allow governments and citizens to use the tools we build at no or low cost in their own cities.
Because PostGIS is free as in beer, it doesn’t add licensing costs to cash-strapped cities. Because PostGIS is free as in speech, it fits our goals for greater transparency in government and science. But that does not imply a compromise in technical quality.
We can rely on PostGIS and successful client applications like the [City of Chicago’s OpenGrid](http://opengrid.io/) can rely on us.
