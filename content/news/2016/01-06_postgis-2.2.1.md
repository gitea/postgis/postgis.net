---
title: PostGIS 2.2.1 Released
layout: post
category: news
tags: [release, 2.2]
author: Regina Obe
thumbnail:
date: "2016-01-06"
---

The PostGIS development team is happy to release patch for PostGIS 2.2, the 2.2.1 release. As befits a patch release, the focus is on bugs, breakages, and performance issues. This release includes many fixes for topology, so topology users should give this release special focus.

**Bug Fixes and Improvements**

  * [#2232](http://trac.osgeo.org/postgis/ticket/2232), avoid accumulated error in SVG rounding
  * [#3321](http://trac.osgeo.org/postgis/ticket/3321), Fix performance regression in topology loading
  * [#3329](http://trac.osgeo.org/postgis/ticket/3329), Fix robustness regression in TopoGeo_addPoint
  * [#3349](http://trac.osgeo.org/postgis/ticket/3349), Fix installation path of `postgis_topology` scripts
  * [#3351](http://trac.osgeo.org/postgis/ticket/3351), set endnodes isolation on `ST_RemoveIsoEdge`
           (and `lwt_RemIsoEdge`)
  * [#3355](http://trac.osgeo.org/postgis/ticket/3355), geography `ST_Segmentize` has geometry bbox
  * [#3359](http://trac.osgeo.org/postgis/ticket/3359), Fix toTopoGeom loss of low-id primitives from
           TopoGeometry definition
  * [#3360](http://trac.osgeo.org/postgis/ticket/3360), `_raster_constraint_info_scale` invalid input syntax
  * [#3375](http://trac.osgeo.org/postgis/ticket/3375), crash in repeated point removal for collection(point)
  * [#3378](http://trac.osgeo.org/postgis/ticket/3378), Fix handling of hierarchical TopoGeometries
           in presence of multiple topologies
  * [#3380](http://trac.osgeo.org/postgis/ticket/3380), [#3402](http://trac.osgeo.org/postgis/ticket/3402), Decimate lines on topology load
  * [#3388](http://trac.osgeo.org/postgis/ticket/3388), [#3410](http://trac.osgeo.org/postgis/ticket/3410), Fix missing end-points in `ST_RemoveRepeatedPoints`
  * [#3389](http://trac.osgeo.org/postgis/ticket/3389), Buffer overflow in `lwgeom_to_geojson`
  * [#3390](http://trac.osgeo.org/postgis/ticket/3390), Compilation under Alpine Linux 3.2
  			gives an error when compiling the postgis and `postgis_topology` extension
  * [#3393](http://trac.osgeo.org/postgis/ticket/3393), ST_Area NaN for some polygons
  * [#3401](http://trac.osgeo.org/postgis/ticket/3401), Improve `ST_Split` robustness on 32bit systems
  * [#3404](http://trac.osgeo.org/postgis/ticket/3404), `ST_ClusterWithin` crashes backend
  * [#3407](http://trac.osgeo.org/postgis/ticket/3407), Fix crash on splitting a face or an edge
           defining multiple TopoGeometry objects
  * [#3411](http://trac.osgeo.org/postgis/ticket/3411), Clustering functions not using spatial index
  * [#3412](http://trac.osgeo.org/postgis/ticket/3412), Improve robustness of snapping step in `TopoGeo_addLinestring`
  * [#3415](http://trac.osgeo.org/postgis/ticket/3415), Fix OSX 10.9 build under pkgsrc
  * Fix memory leak in `lwt_ChangeEdgeGeom` [liblwgeom]

See the full list of changes in the [news file](http://svn.osgeo.org/postgis/tags/2.2.1/NEWS) and please [report bugs](https://trac.osgeo.org/postgis) that you find in the release.

* [postgis-2.2.1.tar.gz]({{< loc "postgis.release_source">}}/postgis-2.2.1.tar.gz)
* [ChangeLog](https://svn.osgeo.org/postgis/tags/2.2.1/ChangeLog)

Binary packages will appear in [repositories](http://postgis.net/install) over the coming weeks as packagers roll out builds.

View all [closed tickets for 2.2.1][1].
  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.2.1&order=priority
