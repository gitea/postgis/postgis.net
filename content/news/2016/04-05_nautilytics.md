---
title: Nautilytics Case Study
layout: post
category: casestudy
tags: [data visualization, government, agriculture, decision support, tile server, us, ghana, brazil]
author: Christopher Lanoue
date: "2016-04-05"
thumbnail: nautilytics.jpg
---

[Nautilytics](http://www.nautilytics.com/) is a small data visualization and GIS startup based out of Boston, MA.
We use PostGIS and PostgreSQL, among other open-source tools to build powerful web applications
for US government organizations, public, and private sector companies.

<!--more-->

# USAID

For USAID, we built a tool for agricultural infrastructure investors in
[Northern Ghana](http://fingap.nautilytics.com/) wherein they can immediately identify "hot spots" of corn and soybean growth potential
intersecting with areas lacking in the necessary infrastructure to bring the crops to market.
PostGIS provided us with the ability to create a dynamic [heatmap](http://www.nautilytics.com/case-studies/usaid-fingap.php)
grid of areas in Ghana that are close to electricity,
major roads, minor roads, and rivers. Within minutes, we created a grid of one-mile blocks
within the entire country of Ghana, which contained the distance of that grid block from the closest piece of each major infrastructure.
After doing it for the entire country, we realized how optimized the query had become and it now functioned
as a way for a user to click on any location in Ghana and immediately retrieve that location's distance to roads, rivers, and electricity.


# WalkJogRun

For [WalkJogRun](http://www.walkjogrun.net/), a popular run tracking application,
we use PostGIS to manage and serve over 1 million GPS-enabled running routes over a five-year period to a Mapnik-powered tile server.
The spatial indexing built into PostGIS, the optimization of Mapnik,
and the coverage provided by a content delivery network return the PNG tiles within milliseconds
while only costing a fraction of other paid tile services.
For those interested, we have documented the steps to replicate this work: [https://github.com/nautilytics/running-route-tileserver](https://github.com/nautilytics/running-route-tileserver)

# Brazil Grain Storage

Corn and soybean yields are increasing at a staggering rate throughout Brazil,
yet there are only 18,000 publicly documented corn and soybean grain silos to hold the growth.
Many US grain storage companies looking to expand into Brazil are unfamiliar
with the different regions and where growth is expected to increase,
so [we built a tool powered by PostGIS](http://nautilytics.com/case-studies/storage-investment-decision-tool.php)
to solve this problem. Once a user identifies an area of interest,
they can click on a storage facility to immediately identify how many storage facilities are within a 50km buffer,
as well as if the area is expected to experience a deficit or surplus of corn or soybeans in the next five years.
In the future, we are looking for this tool to become a content management system of sorts for investors into Brazil.
