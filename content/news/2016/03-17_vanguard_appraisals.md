---
title: Vanguard Appraisals
layout: post
category: casestudy
tags: [property appraisal, us]
author: Andy Colson
date: "2016-03-17"
---

[There is an Updated versionof this case](/2018/11/02/vanguard_appraisals/)


Vanguard Appraisals is new to the GIS world. In fact, we aren't really in the GIS world; we just kind of brush up against it.  We do mass property appraisal for entire county and city jurisdictions, and we develop software to collect, price and maintain values.  We also host assessment data online so that homeowners can search and find property information much simpler from the comfort of their own home.  Our software and websites are used in 7 states (IA, IL, MN, MO, NE, ND, SD).

<!--more-->


We were happy in our little world, doing parcel stuff, not really knowing about or using GIS, but then the landscape started changing, and GIS started popping up all over the place.  Our clients starting asking us if we could host their GIS data, as well as their parcel data.  Some of our clients are very small; there is one person in the Assessor's office with one computer, no server, and a very small internet pipe. Some of our clients are big with many users, multiple servers, and an internet pipe that makes me blush. :-)

We searched and found something that already worked with our favorite database: PostgreSQL.  PG is already hosting our parcel data, so it seemed like a good idea to let it host our GIS data too. Using PostGIS combined with MapServer, Perl and OpenLayers, we came up with online maps that fit the bill:

1. Great performance.
2. Sql:  the ability to write sql to join our existing parcel data with GIS data makes it simple to work with, powerful, and fast.
3. Free:  because we didn't pay for anything, we didn't charge anything.  Government Assessor's offices don't have to charge tax payers to get their GIS online.

PostGIS has been a great decision.  When one of our programmers came up with a crazy idea about doing a sales ratio analysis and highlighting all the properties on the map, not only was it possible but not that hard to do, and it has already been implemented because of PostGIS.

I also cannot stress enough how good and helpful the online community has been.  I went from knowing nothing about GIS to hosting maps only because of them and all the questions they helped with over the years.

Vanguard Appraisals:  <http://camavision.com/>

Assessment Data:  <http://iowaassessors.com/>

We host parcel data for all the yellow links, but we don't host the maps for all of them.  Some counties we host maps for are: Washington MN, Jasper IA, Van Buren IA, Iowa City IA.  Just over 50 counties so far.

You can find the map embedded into the parcel page:
<http://vanburen.iowaassessors.com/parcel.php?parcel=000600307304130>

or Full Page:
<http://maps.camavision.com/map/jasperia>
