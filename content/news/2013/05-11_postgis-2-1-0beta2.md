---
title: PostGIS 2.1.0beta2 Released
layout: post
category: news
tags: [release, 2.1]
author: Regina Obe
thumbnail:
date: "2013-05-11"
---

The 2.1.0beta2 release of PostGIS is [now available]({{< loc "postgis.release_source">}}/postgis-2.1.0beta2.tar.gz).

The PostGIS development team is proud to release a feature complete beta version of upcoming PostGIS 2.1.0.
As befits a minor release, the focus is on speed improvements, more features, and bug fixes.
While this beta release is feature complete, we expect some bugs and
we'd appreciate it if you test it before final release and report
back with any issues you run into so we can have a smooth release.

If you are currently using PostGIS 2.0+, you can go the soft upgrade path:
ALTER EXTENSION postgis UPDATE TO "2.1.0beta2";

Users of 1.5 and below will need to go the hard-upgrade path.

Best served with a bottle of GEOS 3.4.0dev and PostgreSQL 9.3beta1. (which will also be released soon)


<!--more-->

<{{< loc "postgis.release_source">}}/postgis-2.1.0beta2.tar.gz>

[html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.1.0beta2.tar.gz)
[pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.1.0beta2.pdf)
[epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.1.0beta2.epub)

This release contains a ton of speed improvements, function additions
, and super sexy new features.  It has been over a year in the making.

**Enhancements**

* documentation now also available in epub format
* Faster geography distance and intersects, more functions for geography
* index selectivity and stats improvements for both geometry and geography
  which means complicated queries should be planned more efficiently
* significantly faster raster operations particularly union and clipping
* multi-band union support and mapalgebra
* better raster out-db and non-georeferenced rasters (like pictures)
* bug fixes and some new functions for topology
* support for PostgreSQL 9.3
* tiger geocoder now can be installed with
  CREATE EXTENSION postgis\_tiger\_geocoder;

**New Features**

New functions itemized in http://postgis.net/docs/manual-dev/PostGIS\_Special\_Functions\_Index.html#NewFunctions_2_1

* more functions for geography
* more functions for geometry (need geos 3.4.0 to take advantage of many new ones)
* more functions for topology
* overflowing number of new functions for raster
* advanced 3D support via SFCGAL module (requires compilation with SFCGAL)
  Refer to matrix to see which functions utilize SFCGAL
  http://postgis.net/docs/manual-dev/PostGIS\_Special\_Functions\_Index.html#PostGIS_TypeFunctionMatrix
* tiger geocoder support for TIGER 2012
* tiger geocoder can now use pagc address standardizer extension
  for better normalization of addresses using pagc\_normalize\_address
  (requires installing pagc address_standardizer PostgreSQL extension)
  http://pagc.svn.sourceforge.net/viewvc/pagc/branches/sew-refactor/postgresql/?view=tar


Team PostGIS

View all [closed tickets][1].

  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.1.0&order=priority

