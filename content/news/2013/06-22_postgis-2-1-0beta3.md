---
title: PostGIS 2.1.0beta3 Released
layout: post
category: news
tags: [release, 2.1]
author: Regina Obe
thumbnail:
date: "2013-06-22"
---

The 2.1.0beta3 release of PostGIS is [now available]({{< loc "postgis.release_source">}}/postgis-2.1.0beta3.tar.gz).

The PostGIS development team is proud to release a feature complete beta version of upcoming PostGIS 2.1.0.
As befits a minor release, the focus is on speed improvements, more features, and bug fixes.
While this beta release is feature complete, we expect some bugs and
we'd appreciate it if you test it before final release and report
back with any issues you run into so we can have a smooth release.

If you are currently using PostGIS 2.0+, you can go the soft upgrade path:
ALTER EXTENSION postgis UPDATE TO "2.1.0beta3";

Users of 1.5 and below will need to go the hard-upgrade path.

Best served with a bottle of GEOS 3.4.0dev and PostgreSQL 9.3beta2. (which will also be released soon)


<!--more-->

<{{< loc "postgis.release_source">}}/postgis-2.1.0beta3.tar.gz>

[html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.1.0beta3.tar.gz)
[pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.1.0beta3.pdf)
[epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.1.0beta3.epub)

This release contains bug fixes completed since 2.1.0beta2 release:

**Bug Fixes**

*  #2185 	postgis: Geometry output functions crash server with invalid WKT (windows 64)
*  #2213 	postgis: postgis-2.1.so: undefined symbol: json_tokener_errors
*  #2336 	tiger geocoder: FIPS 20 causes wildcard expansion to wget all files
*  #2339 	postgis: PostgreSQL 9.3beta1 EDB regress failures
*  #2359 	raster: raster2pgsql - large collection of examples of different bugs in -l (overviews)
*  #1959 	build/upgrade/install: Upgrading with extensions getting a row is too big
*  #1850 	build/upgrade/install: upgrade scripts for postgis 2.0 and 2.1
*  #2356 	build/upgrade/install: PostGIS extension files no longer installing
*  #2334 	build/upgrade/install: Cannot upgrade from 2.0 to 2.1 (non-extension upgrade)
*  #2279 	build/upgrade/install: Can't upgrade PostGIS from 2.0
*  #2351 	postgis: Postgis 2.1 st_distance between geographies wrong
*  #2262 	postgis: autocast to box causing issues with removed functions
*  #2300 	postgis: Cleanup ePub formatting and generation
*  #2315 	postgis: geography_distance_uncached: variable 'tolerance' set but not used
*  #2165 	postgis: ST_NumPoints regression failure with CircularString
*  #2348 	raster: Provide 2.0 to 2.1 upgrade path for raster
*  #2346 	topology: Fix topology tests after GEOSSnap fixes
*  #2344 	buildbots: Changing in regress caused winnie to scream
*  #2332 	postgis: ST_GeomFromWKB crashes PostgreSQL server
*  #2329 	tiger geocoder: bug on backup
*  #2326 	documentation: [raster]: ST_ColorMap doesn't respect nodata value
*  #2323 	sfcgal: doc po builds missing sfcgal items

Team PostGIS

View all [closed tickets][1].

  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.1.0&order=priority

