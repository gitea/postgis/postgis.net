---
title: PostGIS Patch Releases
layout: post
category: news
tags: [release,3.5,3.4,3.3,3.2,3.1]
author: Regina Obe
date: "2024-12-23"
thumbnail:
---

The PostGIS development team is pleased to provide
[bug fix releases]({{< loc "postgis.release_source">}}) for
[3.5.1](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.5.1/NEWS),
[3.4.4](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.4.4/NEWS),
[3.3.8](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.8/NEWS),
[3.2.8](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.8/NEWS),
[3.1.12](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.12/NEWS)

Please refer to the links above for more information about the issues resolved by these releases.

<!--more-->

### Upgrading

After installing the binaries or after running pg_upgrade:

Do below which will upgrade all your postgis extensions.

```postgres
SELECT postgis_extensions_upgrade();
-- to confirm all is good
SELECT postgis_full_version();
```