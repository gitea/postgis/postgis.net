---
title: PostGIS Patch Releases
layout: post
category: news
tags: [release,3.4,3.3,3.2,3.1,3.0,2.5]
author: Paul Ramsey
date: "2024-02-08"
thumbnail:
---

The PostGIS development team is pleased to provide
[bug fix releases]({{< loc "postgis.release_source">}}) for
[3.4.2](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.4.2/NEWS),
[3.3.6](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.6/NEWS),
[3.2.7](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.7/NEWS),
[3.1.11](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.11/NEWS),
[3.0.11](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.11/NEWS), and
[2.5.11](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.11/NEWS).

Please refer to the links above for more information about the issues resolved by these releases.
