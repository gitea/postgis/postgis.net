---
title: PostGIS 3.5.0alpha2
layout: post
category: news
tags: [release, 3.5]
author: Regina Obe
thumbnail:
date: "2024-07-06"
---

The PostGIS Team is pleased to release PostGIS 3.5.0alpha2!
Best Served with [PostgreSQL 17 Beta2](https://www.postgresql.org/about/news/postgresql-17-beta-2-released-2885/)
and [GEOS 3.12.2](https://github.com/libgeos/geos/releases/tag/3.12.2).

This version requires PostgreSQL 12 - 17, GEOS 3.8 or higher, and Proj 6.1+.
To take advantage of all features, GEOS 3.12+ is needed.
SFCGAL 1.4-1.5 is needed to enable postgis_sfcgal support.
To take advantage of all SFCGAL features, SFCGAL 1.5 is needed.

### 3.5.0alpha2

* [source download]({{< loc "postgis.release_source">}}/postgis-3.5.0alpha2.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.5.0alpha2.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.5.0alpha2/NEWS)
* PDF docs: [en]({{< loc "postgis.release_docs">}}/postgis-3.5.0alpha2-en.pdf) [ja]({{< loc "postgis.release_docs">}}/postgis-3.5.0alpha2-ja.pdf), [zh_Hans]({{< loc "postgis.release_docs">}}/postgis-3.5.0alpha2-zh_Hans.pdf),  [fr]({{< loc "postgis.release_docs">}}/postgis-3.5.0alpha2-fr.pdf)

This release is an alpha of a major release, it includes bug fixes since PostGIS 3.4.2 and new features.

<!--more-->
###  Changes since PostGIS 3.5.0alpha2

- [#5760](https://trac.osgeo.org/postgis/ticket/5760), postgis_topology: Fix spelling errors (Bas Couwenberg)
- [#5759](https://trac.osgeo.org/postgis/ticket/5759), 3.5.0-alpha1 fails to build due to test failures (Sandro Santilli) 

Many thanks to our translation teams, in particular:

Dapeng Wang, Zuo Chenwei from HighGo (Chinese Team)
Teramoto Ikuhiro (Japanese Team)
Vincent Bre from Oslandia (French Team)

### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.4, 3.3, 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.5.0alpha2 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.5.0&milestone=PostGIS+3.4.2&milestone=PostGIS+3.3.6
