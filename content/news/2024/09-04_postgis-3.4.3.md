---
title: PostGIS 3.4.3
layout: post
category: news
tags: [release, 3.4]
author: Paul Ramsey
thumbnail:
date: "2024-09-04"
---

The PostGIS Team is pleased to release PostGIS 3.4.3!

This version requires PostgreSQL 12-17, GEOS 3.8+, and Proj 6.1+.
To take advantage of all features, GEOS 3.12+ is needed.
To take advantage of all SFCGAL features, SFCGAL 1.5+ is needed.

### 3.4.3

* [source download]({{< loc "postgis.release_source">}}/postgis-3.4.3.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.4.3.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.4.3/NEWS)
* PDF docs: [en]({{< loc "postgis.release_docs">}}/postgis-3.4.3-en.pdf), [ja]({{< loc "postgis.release_docs">}}/postgis-3.4.3-ja.pdf), [fr]({{< loc "postgis.release_docs">}}/postgis-3.4.3-fr.pdf)

<!--more-->

### Changes

 - [#5766](https://trac.osgeo.org/postgis/ticket/5766), Always report invalid non-null MBR of universal face (Sandro Santilli)
 - [#5709](https://trac.osgeo.org/postgis/ticket/5709), Fix loose mbr in topology.face on ST_ChangeEdgeGeom (Sandro Santilli)
 - [#5698](https://trac.osgeo.org/postgis/ticket/5698), Fix robustness issue splitting line by vertex very close to endpoints,
          affecting topology population functions (Sandro Santilli)
 - [#5649](https://trac.osgeo.org/postgis/ticket/5649), ST_Value should return NULL on missing band (Paul Ramsey)
 - [#5677](https://trac.osgeo.org/postgis/ticket/5677), ST_Union(geom[]) should unary union single entry arrays (Paul Ramsey)
 - [#5679](https://trac.osgeo.org/postgis/ticket/5679), Remove spurious COMMIT statements from sfcgal script
          (Sandro Santilli, Loïc Bartoletti)
 - [#5680](https://trac.osgeo.org/postgis/ticket/5680), Fix populate_topology_layer with standard_conforming_strings
          set to off (Sandro Santilli)
 - [#5589](https://trac.osgeo.org/postgis/ticket/5589), ST_3DDistance error for shared first point (Paul Ramsey)
 - [#5686](https://trac.osgeo.org/postgis/ticket/5686), ST_NumInteriorRings and Triangle crash (Paul Ramsey)
 - [#5666](https://trac.osgeo.org/postgis/ticket/5666), Build reproducibility: timestamps
          in extension upgrade SQL scripts (James Addison)
 - [#5671](https://trac.osgeo.org/postgis/ticket/5671), Bug in ST_Area function with use_spheroid=false
          (Paul Ramsey, Regina Obe)
 - [#5687](https://trac.osgeo.org/postgis/ticket/5687), Don't rely on search_path to determine postgis schema
           Fix for PG17 security change (Regina Obe)
 - [#5695](https://trac.osgeo.org/postgis/ticket/5695), [address_standardizer_data_us] standardize_address
          incorrect handling of directionals (Regina Obe)
 - [#5653](https://trac.osgeo.org/postgis/ticket/5653), Do not simplify away points when linestring doubles back on
          itself (Paul Ramsey)
 - [#5720](https://trac.osgeo.org/postgis/ticket/5720), Correctly mangle special column names in shp2pgsql (Paul Ramsey)
 - [#5734](https://trac.osgeo.org/postgis/ticket/5734), Estimate geography extent more correctly (Paul Ramsey)
 - [#5752](https://trac.osgeo.org/postgis/ticket/5752), ST_ClosestPoint(geography) error (Paul Ramsey)
 - [#5740](https://trac.osgeo.org/postgis/ticket/5740), ST_DistanceSpheroid(geometry) incorrectly handles polygons (Paul Ramsey)
 - [#5765](https://trac.osgeo.org/postgis/ticket/5765), Handle nearly co-linear edges with slightly less slop (Paul Ramsey)
 - [#5745](https://trac.osgeo.org/postgis/ticket/5745), St_AsLatLonText rounding errors (Paul Ramsey)


### Upgrading

After installing the binaries or after running `pg_upgrade`:

For PostGIS 3.4, 3.3, 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our
[ticket tracker](https://trac.osgeo.org/postgis) or
[mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.4.3 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.4.3
