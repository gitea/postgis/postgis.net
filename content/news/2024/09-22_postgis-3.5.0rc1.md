---
title: PostGIS 3.5.0rc1
layout: post
category: news
tags: [release, 3.5]
author: Regina Obe
thumbnail:
date: "2024-09-16"
---

The PostGIS Team is pleased to release PostGIS 3.5.0rc1!
Best Served with [PostgreSQL 17 RC1](https://www.postgresql.org/about/news/postgresql-17-rc1-released-2926/)
and [GEOS 3.13.0](https://github.com/libgeos/geos/releases/tag/3.13.0).

This version requires PostgreSQL 12 - 17, GEOS 3.8 or higher, and Proj 6.1+.
To take advantage of all features, GEOS 3.12+ is needed.
SFCGAL 1.4+ is needed to enable postgis_sfcgal support.
To take advantage of all SFCGAL features, SFCGAL 1.5 is needed.

### 3.5.0rc1

* [source download]({{< loc "postgis.release_source">}}/postgis-3.5.0rc1.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.5.0rc1.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.5.0rc1/NEWS)
* PDF docs: [en]({{< loc "postgis.release_docs">}}/postgis-3.5.0rc1-en.pdf) [ja]({{< loc "postgis.release_docs">}}/postgis-3.5.0rc1-ja.pdf), [zh_Hans]({{< loc "postgis.release_docs">}}/postgis-3.5.0rc1-zh_Hans.pdf),  [fr]({{< loc "postgis.release_docs">}}/postgis-3.5.0rc1-fr.pdf)

This release is a release candidate of a major release, it includes bug fixes since PostGIS 3.4.3 and new features.

Changes since 3.5.0beta1 are as follows:

 - [#5779](https://trac.osgeo.org/postgis/ticket/5779) Failures building in parallel mode (Sandro Santilli)
 - [#5778](https://trac.osgeo.org/postgis/ticket/5778), Sections missing in What's new (Regina Obe)

<!--more-->
Planned release for PostGIS 3.5.0 will be sometime this week assuming no other major issues.

Many thanks to our translation teams, in particular:

Dapeng Wang, Zuo Chenwei from HighGo (Chinese Team)
Teramoto Ikuhiro (Japanese Team)
Vincent Bre from Oslandia (French Team)

### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.4, 3.3, 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.5.0rc1 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.5.0&milestone=PostGIS+3.4.2&milestone=PostGIS+3.3.6
