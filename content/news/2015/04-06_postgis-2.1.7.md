---
title: PostGIS 2.0.7 and 2.1.7 Released
layout: post
category: news
tags: [release, 2.1, 2.0, EOL]
author: Paul Ramsey
date: "2015-04-06"
thumbnail:
---

Due to a critical bug in GeoJSON ingestion we have made an early release of versions 2.0.7 and 2.1.7. If you are running an earlier version on a public site and accepting incoming GeoJSON, we recommend that you update as soon as possible.

* [Download 2.1.7]({{< loc "postgis.release_source">}}/postgis-2.1.7.tar.gz)
* [Download 2.0.7]({{< loc "postgis.release_source">}}/postgis-2.0.7.tar.gz)

View all [closed tickets for 2.0.7](http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.0.7&order=priority).

