---
title: 2.2.0 Released!
layout: post
category: news
tags: [release, 2.2]
author: Paul Ramsey
date: "2015-10-07"
thumbnail:
---

PostGIS 2.2.0 is released! Over the last two years a number of interesting new features have been added, such as:

* True nearest-neighbor searching for all geometry and geography types
* New volumetric geometry support, including [ST\_3DDifference](http://postgis.net/docs/manual-2.2/ST_3DDifference.html), [ST\_3DUnion](http://postgis.net/docs/manual-2.2/ST_3DUnion.html) and more
* Temporal data model support and functions like [ST\_ClosestPointOfApproach](http://postgis.net/docs/manual-2.2/ST_ClosestPointOfApproach.html) to support temporal query
* Spatial clustering functions [ST\_ClusterIntersecting](http://postgis.net/docs/manual-2.2/ST_ClusterIntersecting.html) and [ST\_ClusterWithin](http://postgis.net/docs/manual-2.2/ST_ClusterWithin.html)
* Subdividing large geometries with [ST\_Subdivide](http://postgis.net/docs/manual-2.2/ST_Subdivide.html)
* Fast box clipping with [ST\_ClipByBox2D](http://postgis.net/docs/manual-2.2/ST_ClipByBox2D.html)
* In-database raster processing with [ST\_Retile](http://postgis.net/docs/manual-2.2/RT_Retile.html) and [ST\_CreateOverview.html](http://postgis.net/docs/manual-2.2/RT_CreateOverview.html)
* New high-speed native code address standardizer
* Visvalingam-Whyatt geometry simplification with [ST\_SimplifyVW](http://postgis.net/docs/manual-2.2/ST_SimplifyVW.html)
* Support for compressed "tiny well-known binary" format with [ST\_AsTWKB](http://postgis.net/docs/manual-2.2/ST_AsTWKB.html) and [ST\_GeomFromTWKB](http://postgis.net/docs/manual-2.2/ST_GeomFromTWKB.html)

See the full list of changes in the [news file](http://svn.osgeo.org/postgis/tags/2.2.0/NEWS) and please [report bugs](https://trac.osgeo.org/postgis) that you find in the release.

* [postgis-2.2.0.tar.gz]({{< loc "postgis.release_source">}}/postgis-2.2.0.tar.gz)
* [ChangeLog](https://svn.osgeo.org/postgis/tags/2.2.0/ChangeLog)

Binary packages will appear in [repositories](http://postgis.net/install) over the coming weeks as packagers roll out builds.

View all [closed tickets for 2.2.0][1].
  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.2.0&order=priority
