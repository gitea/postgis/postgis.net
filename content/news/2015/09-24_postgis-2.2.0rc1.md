---
title: 2.2.0rc1 Available
layout: post
category: news
tags: [release, 2.2]
author: Paul Ramsey
date: "2015-09-24"
thumbnail:
---

This is it, PostGIS 2.2 is almost out the door, so we're looking for testing and feedback! Please give the release candidate a try and report back any issues you encounter.

* [Download]({{< loc "postgis.release_source">}}/postgis-2.2.0rc1.tar.gz)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.2.0rc1/ChangeLog)

View all [closed tickets for 2.2.0][1].
  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.2.0&order=priority
