---
title: 2.1.8 Released
layout: post
category: news
tags: [release, 2.1]
author: Paul Ramsey
date: "2015-07-07"
thumbnail:
---

Due to a number of bugs capable of crashing a back-end, we have released 2.1.8. If you are running an earlier version on a public site, we recommend that you update as soon as possible.

* [Download]({{< loc "postgis.release_source">}}/postgis-2.1.8.tar.gz)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.1.8/ChangeLog)

View all [closed tickets for 2.1.8][1].
  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.1.8&order=priority
