---
title: PostGIS 2.1.6 Released
layout: post
category: news
tags: [release, 2.1]
author: Paul Ramsey
date: "2015-03-20"
thumbnail:
---
The 2.1.6 release of PostGIS is [now available]({{< loc "postgis.release_source">}}/postgis-2.1.6.tar.gz).

The PostGIS development team is happy to release patch for PostGIS 2.1, the 2.1.6 release. As befits a patch release, the focus is on bugs, breakages, and performance issues. Users with large tables of points will want to priorize this patch, for substantial (~50%) disk space savings.

* [Download 2.1.6]({{< loc "postgis.release_source">}}/postgis-2.1.6.tar.gz)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.1.6/ChangeLog)

<!--more-->

[html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.1.6.tar.gz)
[pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.1.6.pdf)

**Enhancements**

* [#3000](http://trac.osgeo.org/postgis/ticket/3000), Ensure edge splitting and healing algorithms use indexes
* [#3048](http://trac.osgeo.org/postgis/ticket/3048), Speed up geometry simplification (J.Santana @ CartoDB)
* [#3050](http://trac.osgeo.org/postgis/ticket/3050), Speep up geometry type reading (J.Santana @ CartoDB)

**Bug Fixes**

* [#2941](http://trac.osgeo.org/postgis/ticket/2941), allow geography columns with SRID other than 4326
* [#3069](http://trac.osgeo.org/postgis/ticket/3069), small objects getting inappropriately fluffed up w/ boxes
* [#3068](http://trac.osgeo.org/postgis/ticket/3068), Have postgis\_typmod\_dims return NULL for unconstrained dims
* [#3061](http://trac.osgeo.org/postgis/ticket/3061), Allow duplicate points in JSON, GML, GML ST\_GeomFrom* functions
* [#3058](http://trac.osgeo.org/postgis/ticket/3058), Fix ND-GiST picksplit method to split on the best plane
* [#3052](http://trac.osgeo.org/postgis/ticket/3052), Make operators &lt;-&gt; and &lt;#&gt; available for PostgreSQL &lt; 9.1
* [#3045](http://trac.osgeo.org/postgis/ticket/3045), Fix dimensionality confusion in &amp;&amp;&amp; operator
* [#3016](http://trac.osgeo.org/postgis/ticket/3016), Allow unregistering layers of corrupted topologies
* [#3015](http://trac.osgeo.org/postgis/ticket/3015), Avoid exceptions from TopologySummary
* [#3020](http://trac.osgeo.org/postgis/ticket/3020), ST\_AddBand out-db bug where height using width value
* [#3031](http://trac.osgeo.org/postgis/ticket/3031), Allow restore of Geometry(Point) tables dumped with empties in them

View all [closed tickets][1].

  [1]:http://trac.osgeo.org/postgis/query?status=closed&groupdesc=1&group=priority&milestone=PostGIS+2.1.6&order=priority

