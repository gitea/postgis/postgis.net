---
title: PostGIS Patch Releases
layout: post
category: news
tags: [release,3.4,3.3,3.2,3.1,3.0]
author: Regina Obe
date: "2023-11-20"
thumbnail:
---

The PostGIS development team is pleased to provide
bug fix and performance enhancements
 3.4.1, 3.3.5, 3.2.6, 3.1.10, 3.0.10  for the 3.4, 3.3, 3.2, 3.1, 3.0 stable branches.

<!--more-->

### 3.4.1

This version works with versions PostgreSQL 12-16, GEOS 3.6 or higher, and Proj 6.1+.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.4.1.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.4.1.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.4.1/NEWS)
* PDF docs: [en]({{< loc "postgis.release_docs">}}/postgis-3.4.1-en.pdf)
* HTML Online [en]({{< loc "site.root">}}docs/manual-3.4/en/) [ja]({{< loc "site.root">}}docs/manual-3.4/ja/) [fr]({{< loc "site.root">}}docs/manual-3.4/fr/) [de]({{< loc "site.root">}}docs/manual-3.4/de/) [ko_KR]({{< loc "site.root">}}docs/manual-3.4/ko_KR/)
* Cheat Sheets:
  * postgis: [en]({{< loc "site.root">}}docs/manual-3.4/postgis_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.4/postgis_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.4/postgis_cheatsheet-fr.html)
  * postgis_raster: [en]({{< loc "site.root">}}docs/manual-3.4/raster_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.4/raster_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.4/raster_cheatsheet-fr.html)
  * postgis_topology: [en]({{< loc "site.root">}}docs/manual-3.4/topology_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.4/topology_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.4/topology_cheatsheet-fr.html)
  * postgis_sfcgal: [en]({{< loc "site.root">}}docs/manual-3.4/sfcgal_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.4/sfcgal_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.4/sfcgal_cheatsheet-fr.html)
  * address standardizer, postgis_tiger_geocoder: [en]({{< loc "site.root">}}docs/manual-3.4/tiger_geocoder_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.4/tiger_geocoder_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.4/tiger_geocoder_cheatsheet-fr.html)


### 3.3.5

This release supports PostgreSQL 11-16, GEOS 3.6 and higher, and proj 5.2 and higher.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.5.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.5/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.3.5.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.3.5.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.3/) [de]({{< loc "site.root">}}docs/manual-3.3/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-3.3/postgis-ja.html) [ko_KR]({{< loc "site.root">}}docs/manual-3.3/postgis-ko_KR.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.3.5.epub)

### 3.2.6

This release works for PostgreSQL 9.6-15.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.6.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.2.6.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.6/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.2.6.pdf)

### 3.1.10

This release supports PostgreSQL 9.6-14.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.10.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.10/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.10.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.1.10.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.1)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.1.10.epub)

### 3.0.10

This release works with PostgreSQL 9.5-13  and GEOS >= 3.6
Designed to take advantage of features in PostgreSQL 12+ and Proj 6+

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.10.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.10/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.10.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.10.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.0)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.10.epub)

View all [closed tickets for 3.3.5, 3.2.6, 3.1.10, 3.0.10][1].

After installing the binaries or after running pg_upgrade:

For PostGIS > 3 do below which will upgrade all your postgis extensions.

```postgres
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

For PostGIS < 3:
```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
```

[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.2.3&milestone=PostGIS+3.1.7&milestone=PostGIS+3.0.7&milestone=PostGIS+2.5.8&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
