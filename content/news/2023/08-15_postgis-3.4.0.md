---
title: PostGIS 3.4.0
layout: post
category: news
tags: [release, 3.4]
author: Regina Obe
thumbnail:
date: "2023-08-15"
---

The PostGIS Team is pleased to release PostGIS 3.4.0!
This version works with versions PostgreSQL 12-16, GEOS 3.6 or higher, and Proj 6.1+.
To take advantage of all features, [GEOS 3.12+](https://libgeos.org/posts/2023-06-27-geos-3-12-released/) is needed.
To take advantage of all SFCGAL features,
[SFCGAL](https://sfcgal.org) 1.4.1+ is needed.

### 3.4.0

* [source download]({{< loc "postgis.release_source">}}/postgis-3.4.0.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.4.0.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.4.0/NEWS)
* PDF docs: [en]({{< loc "postgis.release_docs">}}/postgis-3.4.0-en.pdf) [ja]({{< loc "postgis.release_docs">}}/postgis-3.4.0-ja.pdf) [fr]({{< loc "postgis.release_docs">}}/postgis-3.4.0-fr.pdf)
* HTML Online [en]({{< loc "site.root">}}docs/manual-3.4/en/) [ja]({{< loc "site.root">}}docs/manual-3.4/ja/) [fr]({{< loc "site.root">}}docs/manual-3.4/fr/) [de]({{< loc "site.root">}}docs/manual-3.4/de/) [ko_KR]({{< loc "site.root">}}docs/manual-3.4/ko_KR/)
* Cheat Sheets:
  * postgis: [en]({{< loc "site.root">}}docs/manual-3.4/postgis_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.4/postgis_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.4/postgis_cheatsheet-fr.html)
  * postgis_raster: [en]({{< loc "site.root">}}docs/manual-3.4/raster_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.4/raster_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.4/raster_cheatsheet-fr.html)
  * postgis_topology: [en]({{< loc "site.root">}}docs/manual-3.4/topology_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.4/topology_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.4/topology_cheatsheet-fr.html)
  * postgis_sfcgal: [en]({{< loc "site.root">}}docs/manual-3.4/sfcgal_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.4/sfcgal_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.4/sfcgal_cheatsheet-fr.html)
  * address standardizer, postgis_tiger_geocoder: [en]({{< loc "site.root">}}docs/manual-3.4/tiger_geocoder_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.4/tiger_geocoder_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.4/tiger_geocoder_cheatsheet-fr.html)


This release is a major release, it includes bug fixes since PostGIS 3.3.4 and new features.

<!--more-->

Many thanks to our translation teams, in particular:

Teramoto Ikuhiro (Japanese Team)
Vincent Bre (French Team)

There are 2 new ./configure switches:

 -  --disable-extension-upgrades-install, will skip installing all the extension upgrade scripts except for the ANY--currentversion. If you use this, you can install select upgrades using the postgis commandline tool
 -  --without-pgconfig, will build just the commandline tools
    raster2pgsql and shp2pgsql even if PostgreSQL is not installed

### New features
- [#5055](https://trac.osgeo.org/postgis/ticket/5055), complete manual internationalization (Sandro Santilli)
- [#5052](https://trac.osgeo.org/postgis/ticket/5052), target version support in postgis_extensions_upgrade
  (Sandro Santilli)
- [#5306](https://trac.osgeo.org/postgis/ticket/5306), expose version of GEOS at compile time (Sandro Santilli)
- New install-extension-upgrades command in postgis script (Sandro Santilli)
- [#5257](https://trac.osgeo.org/postgis/ticket/5257), [#5261](https://trac.osgeo.org/postgis/ticket/5261), [#5277](https://trac.osgeo.org/postgis/ticket/5277), Support changes for PostgreSQL 16 (Regina Obe)
- [#5006](https://trac.osgeo.org/postgis/ticket/5006), [GH705](https://github.com/postgis/postgis/pull/705), ST_Transform: Support PROJ pipelines (Robert Coup, Koordinates)
- [#5283](https://trac.osgeo.org/postgis/ticket/5283), [postgis_topology] RenameTopology (Sandro Santilli)
- [#5286](https://trac.osgeo.org/postgis/ticket/5286), [postgis_topology] RenameTopoGeometryColumn (Sandro Santilli)
- [GH703](https://github.com/postgis/postgis/pull/703), [postgis_raster] Add min/max resampling as options (Christian Schroeder)
- [#5336](https://trac.osgeo.org/postgis/ticket/5336), [postgis_topology] topogeometry cast to topoelement support (Regina Obe)
- Allow singleton geometry to be inserted into Geometry(Multi*) columns (Paul Ramsey)
- [GH721](https://github.com/postgis/postgis/pull/721), New window-based ST_ClusterWithinWin and ST_ClusterIntersectingWin (Paul Ramsey)
- [#5397](https://trac.osgeo.org/postgis/ticket/5397), [address_standardizer] debug_standardize_address function (Regina Obe)
- [#5373](https://trac.osgeo.org/postgis/ticket/5373), ST_LargestEmptyCircle, exposes extra semantics on circle finding
  Requires GEOS 3.9+ (Martin Davis)
- [#5267](https://trac.osgeo.org/postgis/ticket/5267), ST_Project signature for geometry, and two-point signature (Paul Ramsey)
- [#5267](https://trac.osgeo.org/postgis/ticket/5267), ST_LineExtend for extending linestrings (Paul Ramsey)

### Enhancements

- [#5194](https://trac.osgeo.org/postgis/ticket/5194), do not update system catalogs from postgis_extensions_upgrade (Sandro Santilli)
- [#5092](https://trac.osgeo.org/postgis/ticket/5092), reduce number of upgrade paths installed on system (Sandro Santilli)
- [#635](https://trac.osgeo.org/postgis/ticket/635), honour --bindir (and --prefix) configure switch for executables (Sandro Santilli)
- Honour --mandir (and --prefix) configure switch for man pages install path (Sandro Santilli)
- Honour --htmldir (and --docdir and --prefix) configure switch for
  html pages install path (Sandro Santilli)
- [postgis_topology] Speed up check of topology faces without edges (Sandro Santilli)
- [postgis_topology] Speed up coincident nodes check in topology validation  (Sandro Santilli)
- [GH718](https://github.com/postgis/postgis/pull/718), ST_QuantizeCoordinates(): speed-up implementation (Even Rouault)
- Repair spatial planner stats to use computed selectivity for contains/within queries (Paul Ramsey)
- [GH734](https://github.com/postgis/postgis/pull/734), Additional metadata on Proj installation in postgis_proj_version (Paul Ramsey)
- [#5177](https://trac.osgeo.org/postgis/ticket/5177), allow building tools without PostgreSQL server headers (Sandro Santilli)
- ST_Project signature for geometry, and two-point signature (Paul Ramsey)
- [#4913](https://trac.osgeo.org/postgis/ticket/4913), ST_AsSVG support for curve types CircularString, CompoundCurve, MultiCurve,
         and MultiSurface (Regina Obe)
- [#5266](https://trac.osgeo.org/postgis/ticket/5266), ST_ClosestPoint, ST_ShortestLine support for geography type
         (MobilityDB Esteban Zimanyi, Paul Ramsey)

### Breaking Changes

- [#5229](https://trac.osgeo.org/postgis/ticket/5229), Drop support for Proj < 6.1 and PG 11 (Regina Obe)
- [#5306](https://trac.osgeo.org/postgis/ticket/5306), [GH734](https://github.com/postgis/postgis/pull/734), postgis_full_version() and postgis_proj_version()
  now output more information about proj network configuration
  and data paths. GEOS compile-time version
  also shown if different from run-time (Paul Ramsey, Sandro Santilli)

 - [#5447](https://trac.osgeo.org/postgis/ticket/5447), postgis_restore.pl renamed to postgis_restore
 (Sandro Santilli)

- Utilities now installed in OS bin or
   user specified --bindir and --prefix
   instead of postgresql bin
   and extension stripped except on windows
   (postgis, postgis_restore, shp2pgsql, raster2pgsql, pgsql2shp,
   pgtopo_import, pgtopo_export)


### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.3, 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.4.0 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.4.0&milestone=PostGIS+3.3.3&milestone=PostGIS+3.3.4&milestone=PostGIS+3.2.5&milestone=PostGIS+3.1.9
