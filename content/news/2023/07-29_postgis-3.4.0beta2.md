---
title: PostGIS 3.4.0beta2
layout: post
category: news
tags: [release, 3.4]
author: Regina Obe
thumbnail:
date: "2023-07-29"
---

The PostGIS Team is pleased to release PostGIS 3.4.0beta2!
Best Served with [PostgreSQL 16 Beta2](https://www.postgresql.org/about/news/postgresql-16-beta-2-released-2665/)
and [GEOS 3.12.0](https://libgeos.org/posts/2023-06-27-geos-3-12-released/).

This version requires PostgreSQL 12 or higher, GEOS 3.6 or higher, and Proj 6.1+.
To take advantage of all features, GEOS 3.12+ is needed.
To take advantage of all SFCGAL features, SFCGAL 1.4.1+ is needed.

### 3.4.0beta2

* [source download]({{< loc "postgis.release_source">}}/postgis-3.4.0beta2.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.4.0beta2.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.4.0beta2/NEWS)
* html online [en]({{< loc "site.root">}}docs/manual-3.4/) [de]({{< loc "site.root">}}docs/manual-3.4/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-3.4/postgis-ja.html) [ko_KR]({{< loc "site.root">}}docs/manual-3.4/postgis-ko_KR.html)

This release is a beta of a major release, it includes bug fixes since PostGIS 3.3.4 and new features.

<!--more-->

View all [closed tickets for 3.4.0beta2][1].

### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.3, 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.4.0beta2 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.4.0&milestone=PostGIS+3.3.4
