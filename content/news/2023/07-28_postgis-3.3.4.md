---
title: PostGIS 3.3.4 Patch Release
layout: post
category: news
tags: [release,3.3]
author: Sandro Santilli
thumbnail:
date: "2023-07-28"
---

The PostGIS development team is pleased to announce
bug fix release 3.3.4, mostly focused on Topology fixes.

<!--more-->
### 3.3.4

This release supports PostgreSQL 11-16beta1, GEOS 3.6 and higher, and proj 5.2 and higher.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.4.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.4/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.3.4.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.3.4.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.3/) [de]({{< loc "site.root">}}docs/manual-3.3/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-3.3/postgis-ja.html) [ko_KR]({{< loc "site.root">}}docs/manual-3.3/postgis-ko_KR.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.3.4.epub)

View all [closed tickets for 3.3.4][1].

After installing the binaries, remember to upgrade your databases
by running:
```postgres
SELECT postgis_extensions_upgrade();
```

[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.3.4&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
