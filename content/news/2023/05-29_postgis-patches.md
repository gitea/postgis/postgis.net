---
title: PostGIS 3.3.3, 3.2.5, 3.1.9, 3.0.9 Patch Releases
layout: post
category: news
tags: [release,3.3,3.2,3.1,3.0]
author: Regina Obe
thumbnail:
date: "2023-05-29"
---

The PostGIS development team is pleased to provide
bug fixes and performance enhancements
3.3.3, 3.2.5, 3.1.9 and 3.0.9 for the 3.3, 3.2, 3.1, and 3.0 stable branches.

<!--more-->
### 3.3.3

This release supports PostgreSQL 11-16beta1, GEOS 3.6 and higher, and proj 5.2 and higher.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.3.3.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.3.3/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.3.3.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.3.3.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.3/) [de]({{< loc "site.root">}}docs/manual-3.3/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-3.3/postgis-ja.html) [ko_KR]({{< loc "site.root">}}docs/manual-3.3/postgis-ko_KR.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.3.3.epub)

### 3.2.5

This release works for PostgreSQL 9.6-15.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.2.5.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.2.5.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.2.5/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.2.5.pdf)

### 3.1.9

This release supports PostgreSQL 9.6-14.

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.9.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.9/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.9.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.1.9.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.1)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.1.9.epub)

### 3.0.9

This release works with PostgreSQL 9.5-13  and GEOS >= 3.6
Designed to take advantage of features in PostgreSQL 12+ and Proj 6+

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.9.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.9/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.9.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.9.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.9.epub)

View all [closed tickets for 3.3.3, 3.2.5, 3.1.9, 3.0.9][1].

After installing the binaries or after running pg_upgrade:

For PostGIS 3.3, 3.2, 3.1, 3.0, 2.5 do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.4 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.

[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.3.3&milestone=PostGIS+3.2.5&milestone=PostGIS+3.1.9&milestone=PostGIS+3.0.9&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
