---
title: PostGIS 3.0.3
layout: post
category: news
tags: [release, "3.0"]
author: Regina Obe
date: "2020-11-20"
thumbnail:
---

The PostGIS Team is pleased to release PostGIS 3.0.3.


Best served with [PostgreSQL 13.1](https://www.postgresql.org/docs/13/release-13-1.html),
and [GEOS 3.8.1](https://geos.osgeo.org)

[pgRouting 3.1.1](https://lists.osgeo.org/pipermail/pgrouting-dev/2020-November/002149.html)

<!--more-->

**PostGIS 3.0.3**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.3.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.3/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.0.3.pdf)


Details of this release can be found in NEWS file:
https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.0.3/NEWS
which is also included in source tar ball and in this email.

If you come across any issues, feel free to report via our ticket tracker
https://trac.osgeo.org/postgis or mailing list with details as described
here - https://postgis.net/support/. For security issues, send reports to
security@postgis.net.

View all [tickets for 3.0.3 milestone][1].


PostGIS 3.0.3
2020/11/20

**Bug Fixes and Enchanements**

  * #4742 tiger geocoder reverted to 2018 version on tiger upgrade
  * #4757, Handle line collapse due to snap-to-existing node (Sandro Santilli)
  * #4758, Improve topology noding robustness (Sandro Santilli)
  * #4719, Fail fast when srids don't match ST_Intersection(geometry,raster)
           Also schema qualify calls in function. (Regina Obe)
  * #4739, Ensure all functions using postgis_oid initialize the internal cache (Raúl Marín)
  * #4767, #4768, #4771, #4772, Fix segfault when parsing invalid WKB (Raúl Marín)
  * #4769, Fix segfault in st_addband (Raúl Marín)
  * #4748, Fix incorrect axis swapping in polar stereographic (Paul Ramsey)
  * #4727, Fix bug in geocentrix box computation (Paul Ramsey)
  * #4790, Fix ST_3dintersects calculations with identical vertices (Nicklas Avén)


[1]: https://trac.osgeo.org/postgis/query?status=closed&milestone=PostGIS+3.0.3
