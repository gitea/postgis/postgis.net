---
title: PostGIS 3.1.0alpha2
layout: post
category: news
tags: [release, 3.1]
author: Regina Obe
date: "2020-07-18"
thumbnail:
---

The PostGIS Team is pleased to release the second alpha of upcoming PostGIS
3.1.0 release.

Best served with

[PostgreSQL 13beta2](https://www.postgresql.org/about/news/2047/),
GEOS 3.7 or higher is recommended.

`ST_MaximumInscribedCircle` requires compilation with [GEOS 3.9.0 in development](https://git.osgeo.org/gitea/geos/geos) to be enabled.

[pgRouting 3.1.0](https://lists.osgeo.org/pipermail/pgrouting-dev/2020-July/002105.html) which will also be released soon.

<!--more-->

**3.1.0alpha2**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.0alpha2.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.0alpha2/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.0alpha2.pdf) [de]({{< loc "postgis.release_docs">}}/postgis-3.1.0alpha2-de.pdf)

Details of this release can be found in NEWS file:
https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.1.0alpha2/NEWS
Which is also included in source tar ball and in this email.

If you come across any issues, feel free to report via our ticket tracker
https://trac.osgeo.org/postgis  or mailing list with details as described
here - https://postgis.net/support/

View all [tickets for 3.1.0 milestone][1].

**New Features**

* #4656, Cast a geojson_text::geometry for implicit GeoJSON ingestion (Raúl Marín)
* #4687, Expose GEOS MaximumInscribedCircle (Paul Ramsey)
* #4710, ST_ClusterKMeans now works with 3D geometries (Darafei Praliaskouski)

**Enhancements**

* #4675, topology.GetRingEdges now implemented in C (Sandro Santilli)
* #4681, ST_GetFaceGeometry: print corruption information (Sandro Santilli)
* #4651: ST_Simplify: Don't copy if nothing is removed (Raúl Marín)
* #4657: Avoid De-TOASTing where possible (Paul Ramsey)
* #4490, Tweak function costs (Raúl Marín)
* #4672, Cache getSRSbySRID and getSRIDbySRS (Raúl Marín)
* #4676, Avoid decompressing toasted geometries to read only the header (Raúl Marín)
Optimize cast to Postgresql point type (Raúl Marín)
* #4620, Update internal wagyu to 0.5.0 (Raúl Marín)
* #4623, Optimize varlena returning functions (Raúl Marín)
* #4677, Share gserialized objects between different cache types (Raúl Marín)
* Fix compilation with MSVC compiler / Standardize shebangs (Loïc Bartoletti)

**Bug fixes**

* #4652, Fix several memory related bugs in ST_GeomFromGML (Raúl Marín)
* #4661, Fix access to spatial_ref_sys with a non default schema (Raúl Marín)
* #4670, ST_AddPoint: Fix bug when a positive position is requested (Raúl Marín)
* #4699, crash on null input to ST_Union(raster, otherarg) (Jaime Casanova, 2ndQuadrant)
* #4716, Fix several issues with pkg-config in the configure script (Raúl Marín)

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.1.0
