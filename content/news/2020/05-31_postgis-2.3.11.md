---
title: PostGIS 2.3.11
layout: post
category: news
tags: [release, 2.3, "EOL"]
author: Regina Obe
date: "2020-05-31"
thumbnail:
---

The PostGIS Team is pleased to release PostGIS 2.3.11.
This is the last bug fix release of the PostGIS 2.3 series.
Please upgrade to 2.4 or higher if you want to continue receiving bug fixes.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.3.11.tar.gz)
* [NEWS]({{< loc "postgis.release_notes">}}/2.3.11/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-2.3.11.pdf)



If you come across any issues, feel free to report via our ticket tracker
https://trac.osgeo.org/postgis or mailing list with details as described
[here](https://postgis.net/support/). For security issues, send reports to
security@postgis.net.

<!--more-->

**Bug Fixes and Enchantments**

* 4475, Avoid reading into empty ptarray (Paul Ramsey)
* 4518, Fix `geometry_columns` and raster views
        to not use `pg_constraints.consrc` which was removed in PG 12 (Regina Obe)
* 4492, Fix ST_Simplify ignoring the value of the 3rd parameter (Raúl Marín)
* 4494, Fix ST_Simplify output having an outdated bbox (Raúl Marín)
* 4493, Fix ST_RemoveRepeatedPoints output having an outdated bbox (Raúl Marín)
* 4495, Fix ST_SnapToGrid output having an outdated bbox (Raúl Marín)
* 4498, Restrict build for PgSQL <= 9.6
* 4519, Fix getSRIDbySRS crash (Raúl Marín)
* 4534, Fix leak in `lwcurvepoly_from_wkb_state` (Raúl Marín)
* 4536, Fix leak in `lwcollection_from_wkb_state` (Raúl Marín)
* 4537, Fix leak in WKT collection parser (Raúl Marín)
* 4547, Fix AddRasterConstraints handling of empty tables (Sandro Santilli)
* 4549, Fix schema qualification of internal types (Raúl Marín)
* 4546, Fix PLPGSQL functions missing the schema qualification (Raúl Marín)
* 4621, Prevent stack overflow when parsing WKB (Raúl Marín)
* 4652, Fix several memory related bugs in ST_GeomFromGML (Raúl Marín)

