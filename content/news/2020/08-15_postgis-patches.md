---
title: PostGIS 3.0.2, 2.5.5, 2.4.9 Released
layout: post
category: news
tags: [release,3.0,2.5,2.4]
author: Regina Obe
date: "2020-08-15"
thumbnail:
---

The PostGIS development team is pleased to provide
bug fix and performance enhancements
 3.0.2, 2.5.5, 2.4.9 for the 3.0, 2.5, and 2.4 stable branches.

<!--more-->

**3.0.2**
This release works with PostgreSQL 9.5-13beta2  and GEOS >= 3.6
Designed to take advantage of features in PostgreSQL 12+ and Proj 6+

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.2.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.2/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.2.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.2.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.2.epub)

**2.5.5**
This release supports PostgreSQL 9.3-12
You are encouraged to use the PostGIS 3.0.2 with PostgreSQL 12 instead of 2.5
because 3.0 has features specifically designed to take
advantage of features new in PostgreSQL 12.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.5.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.5/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.5.5.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.5.5.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-2.5) [de]({{< loc "site.root">}}docs/manual-2.5/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-2.5/postgis-ja.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.5.5.epub)

**2.4.9**
This release supports PostgreSQL 9.3-10.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.4.9.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.4.9/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.4.9.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.4.9.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-2.4)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.9.epub)


View all [closed tickets for 3.0.2, 2.5.5, 2.4.9][1].

After installing the binaries or after running pg_upgrade:

For PostGIS 2.5 and 3.0 do:
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.4 do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+2.4.9&milestone=PostGIS+2.5.5&milestone=PostGIS+3.0.2&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
