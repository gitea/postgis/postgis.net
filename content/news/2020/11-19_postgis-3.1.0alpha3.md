---
title: PostGIS 3.1.0alpha3
layout: post
category: news
tags: [release, 3.1]
author: Regina Obe
date: "2020-11-19"
thumbnail:
---

The PostGIS Team is pleased to release the third alpha of upcoming PostGIS
3.1.0 release. This version is exposes some of
the new performance and feature enhancements in not yet relesed GEOS 3.9 as well as numerous speed enhancements not requiring newer GEOS.
Requires GEOS 3.6+ and PostgreSQL 9.6+. To use MVT you will need protobuf-c 1.1. or higher.

Best served with

[PostgreSQL 13.1](https://www.postgresql.org/about/news/postgresql-131-125-1110-1015-9620-and-9524-released-2111/),
GEOS 3.7 or higher is recommended.

[pgRouting 3.1.1](https://lists.osgeo.org/pipermail/pgrouting-dev/2020-November/002149.html)

<!--more-->

**3.1.0alpha3**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.0alpha3.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.0alpha3/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.0alpha3.pdf)

Details of this release can be found in NEWS file:
https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.1.0alpha3/NEWS
Which is also included in source tar ball and in this email.

If you come across any issues, feel free to report via our ticket tracker
https://trac.osgeo.org/postgis  or mailing list with details as described
here - https://postgis.net/support/

View all [tickets for 3.1.0 milestone][1].

**Breaking changes**

  * #4737, Bump minimum protobuf-c requirement to 1.1.0 (Raúl Marín)
           The configure step will now fail if the requirement isn't
           met or explicitly disabled (--without-protobuf)
  * #4258, Untangle postgis_sfcgal from postgis
           into its own lib file (Regina Obe)



**New Features**

* #4698, Add a precision parameter to `ST_AsEWKT` (Raúl Marín)
* Add a gridSize optional parameter to `ST_Union`, `ST_UnaryUnion`,
	`ST_Difference`, `ST_Intersection`, `ST_SymDifference`, `ST_Subdivide`
	Requires GEOS 3.9 (Sandro Santilli)


**Enhancements**

* #4789, Speed up TopoJSON output for areal TopoGeometry with
many holes (Sandro Santilli)
* #4758, Improve topology noding robustness (Sandro Santilli)
- Make `ST_Subdivide` interruptable (Sandro Santilli)
* #4660, Changes in double / coordinate printing (Raúl Marín)
	- Use the shortest representation (enough to guarantee roundtrip).
	- Uses scientific notation for absolute numbers smaller than 1e-8.
	The previous behaviour was to output 0 for absolute values smaller than
	1e-12 and fixed notation for anything bigger than that.
	- Uses scientific notation for absolute numbers greater than 1e+15 (same behaviour).
	- The precision parameter now also affects the scientific notation (before it was fixed [5-8]).
	- All output functions now respect the requested precision (without any limits).
	- The default precision is the same (9 for GeoJSON, 15 for everything else).
* #4729, WKT/KML: Print doubles directly into stringbuffers (Raúl Marín)
* #4533, Use the standard coordinate printing system for box types (Raúl Marín)
* #4686, Avoid decompressing geographies when possible (Raúl Marín)
	Affects ANALYZE, `_ST_PointOutside`, postgis_geobbox, `ST_CombineBbox(box2d, geometry)`,
	`ST_ClipByBox2D` when the geometry is fully inside or outside the bbox and
	`ST_BoundingDiagonal`.
* #4741, Don't use `ST_PointInsideCircle` if you need indexes, use `ST_DWithin` instead.
Documentation adjusted (Darafei Praliaskouski)
* #4737, Improve performance and reduce memory usage in `ST_AsMVT`, especially in
queries involving parallelism (Raúl Marín)
* #4746, Micro optimizations to the serialization process (Raúl Marín)
* #4719, Fail fast when srids don't match `ST_Intersection(geometry,raster)`
 	Also schema qualify calls in function. (Regina Obe)
* #4784, Add `ST_CollectionExtract(geometry)` with default behaviour of
extracting the components of highest coordinate dimension. (Paul Ramsey)


**Bug fixes**

* #4691, Fix segfault during gist index creation with empty geometries (Raúl Marín)
* Fix handling of bad WKB inputs (Oracle types) and unit tests for
	malformed WKB. Remove memory leaks in malformed WKB cases. (Paul Ramsey)
* #4740, Round values in geography_distance_tree
				 as we do on geography_distance (Raúl Marín, Paul Ramsey, Regina Obe)
* #4739, Ensure all functions using postgis_oid initialize the internal cache (Raúl Marín)
* #4767, #4768, #4771, #4772, Fix segfault when parsing invalid WKB (Raúl Marín)
* #4769, Fix segfault in `ST_AddBand` (Raúl Marín)
* #4790, Fix `ST_3dintersects` calculations with identical vertices (Nicklas Avén)
* #4742, tiger geocoder reverted to 2018 version on tiger upgrade (Regina Obe)
* #3372, TopoElementArray cannot be null - change domain constraint (Regina Obe)


[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.1.0
