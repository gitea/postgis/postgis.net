---
title: PostGIS 3.1.0
layout: post
category: news
tags: [release, 3.1]
author: Paul Ramsey
date: "2020-12-18"
thumbnail:
---

The PostGIS Team is pleased to release the release of PostGIS 3.1.0!

This version exposes the new features of [GEOS 3.9](https://lists.osgeo.org/pipermail/geos-devel/2020-December/010003.html) as well as numerous core performance enhancements for spatial joins, large object access, text format output and more.

[Performance](http://blog.cleverelephant.ca/2020/12/waiting-postgis-31-1.html) is a key feature of this release, with improvements to spatial joins, [text outputs](https://rmr.ninja/2020-12-06-waiting-for-postgis-3-1-output/), [large object reads](https://rmr.ninja/2020-12-14-waiting-for-postgis-3-1-large_geometries/), [vector tile output](https://rmr.ninja/2020-11-19-waiting-for-postgis-3-1-mvt/), and a host of smaller tweaks.

The [k-means clustering](https://postgis.net/docs/ST_ClusterKMeans.html) code has been enhanced to support weighting and higher dimensional clusters.

Geometry generators to create [hexagonal and square tilings](http://blog.cleverelephant.ca/2020/12/waiting-postgis-31-2.html) have been added, for simpler in-the-database summarization queries.

Finally, PostGIS exposes the [latest enhancements](http://blog.cleverelephant.ca/2020/12/waiting-postgis-31-3.html) in the GEOS geometry library 3.9 version. The new overlay engine (aka "[OverlayNG](https://lin-ear-th-inking.blogspot.com/2020/10/overlayng-lands-in-jts-master.html)") provides [more robust](https://lin-ear-th-inking.blogspot.com/2020/06/jts-overlayng-tolerant-topology.html) handling of difficult input geometries, using a set of [new noding strategies](https://lin-ear-th-inking.blogspot.com/2020/06/jts-overlayng-noding-strategies.html) to process geometry. For the end user, this should mean no more "topology exceptions" when using the union, difference, intersection or symmetric difference functions. PostGIS also exposes the new fixed precision overlay capability via an additional grid-size parameter on [ST_Intersection](http://postgis.net/docs/manual-dev/ST_Intersection.html) and the other overlay functions.

<!--more-->

Best served with:

* [PostgreSQL 13.1](https://www.postgresql.org/about/news/postgresql-131-125-1110-1015-9620-and-9524-released-2111/)
* [GEOS 3.9](https://lists.osgeo.org/pipermail/geos-devel/2020-December/010003.html)
* [pgRouting 3.1.1](https://lists.osgeo.org/pipermail/pgrouting-dev/2020-November/002149.html)
* [GDAL 3.2](https://gdal.org/download.html#current-releases)
* Requires GEOS 3.6+ and PostgreSQL 9.6+.
* MVT support requires protobuf-c 1.1+.

**3.1.0**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.0.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.0/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.0.pdf)

Details of this release can be found in the NEWS file:

  https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.1.0/NEWS

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.1.0 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.1.0
