---
title: PostGIS 3.0.1
layout: post
category: news
tags: [release, 3.0]
author: Darafei Praliaskouski
date: "2020-02-20"
thumbnail:
---

The PostGIS Team is pleased to release PostGIS 3.0.1.


Best served with [PostgreSQL 12.2](https://www.postgresql.org/docs/12/release-12-2.html),
GEOS 3.8.0, SFCGAL 1.3.7, GDAL 3.0.4, PROJ 6.3.1, protobuf-c 1.3.3, json-c 0.13.1.

<!--more-->

**PostGIS 3.0.1**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.1/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-3.0.1.pdf)

This is the first release of PostGIS done after our move to Git and that
supports PostgreSQL 13 development release

Details of this release can be found in NEWS file:
https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.0.1/NEWS
which is also included in source tar ball and in this email.

If you come across any issues, feel free to report via our ticket tracker
https://trac.osgeo.org/postgis or mailing list with details as described
here - https://postgis.net/support/. For security issues, send reports to
security@postgis.net.


**Breaking Changes**

  * #4637 svn number replaced by git hash in version output
    (Sandro Santilli, Regina Obe, Bas Couwenberg)

**New Features**

  * #4617, Add configure switch `--without-phony-revision` (Raúl Marín)
          Use to prevent postgis_revision.h from rebuilding
          (when building from tar ball) and building in a gitted folder.

**Bug Fixes and Enchantments**

  * #4558, Fix oversimplification of polygon inner rings (Raúl Marín)
  * #4588, Fix update when ST_Union(geometry) doesn't exist (Raúl Marín)
  * #4590, Fix pg_upgrade issue with ST_LineCrossingDirection (Raúl Marín)
  * #4599, ST_AddPoint: Accept -1 as a valid position (Raúl Marín)
  * #4600, Improve precision of ST_TileEnvelope (Raúl Marín)
  * #4605, Fix postgis_upgrade.pl with PostgreSQL 12 (Matti Linnanvuori)
  * #4606, Respect user's LDFLAGS in postgis module build (Sandro Santilli)
  * #4610, Add in logic to install postgis_tiger_geocoder from unpackaged (Regina Obe)
  * #4581, Installing postgis_raster from unpackaged tries to use wrong schema (Regina Obe)
  * #4596, The script to generate nation_script_load.sh is missing
           a trailing quote (Bill Mill)
  * #4608, PG12: Fix several bugs in the index support function (Raúl Marín)
  * #4621, Prevent stack overflow when parsing WKB (Raúl Marín)
  * #4626, Support pkg-config for libxml2 (Bas Couwenberg)
  * #4632, Allow building with PostgreSQL 13 (Raúl Marín)
  * #4570, Include raster2pgsql in windows packaging (Regina Obe)
