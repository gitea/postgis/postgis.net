---
title: PostGIS 2.5.4
layout: post
category: news
tags: [release, 2.5]
author: Paul Ramsey
thumbnail:
---

The PostGIS Team is pleased to release PostGIS 2.5.4.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.4.tar.gz)
* [NEWS]({{< loc "postgis.release_notes">}}/2.5.4/NEWS)
* [PDF docs en]({{< loc "postgis.release_docs">}}/postgis-2.5.4.pdf)

<!--more-->

If you come across any issues, feel free to report via our ticket tracker
https://trac.osgeo.org/postgis or mailing list with details as described
[here](https://postgis.net/support/). For security issues, send reports to
security@postgis.net.

**Bug Fixes and Enchantments**

* [4480](https://trac.osgeo.org/postgis/ticket/4480), Geography Distance inconsistent with Intersects (Paul Ramsey)
* [4481](https://trac.osgeo.org/postgis/ticket/4481), Improve libprotobuf detection for old systems (Paul Ramsey)
* [4475](https://trac.osgeo.org/postgis/ticket/4475), Avoid reading into empty ptarray (Paul Ramsey)
* [4492](https://trac.osgeo.org/postgis/ticket/4492), Fix ST\_Simplify ignoring the value of the 3rd parameter (Raúl Marín)
* [4494](https://trac.osgeo.org/postgis/ticket/4494), Fix ST\_Simplify output having an outdated bbox (Raúl Marín)
* [4493](https://trac.osgeo.org/postgis/ticket/4493), Fix ST\_RemoveRepeatedPoints output having an outdated bbox (Raúl Marín)
* [4495](https://trac.osgeo.org/postgis/ticket/4495), Fix ST\_SnapToGrid output having an outdated bbox (Raúl Marín)
* [4496](https://trac.osgeo.org/postgis/ticket/4496), Make ST\_Simplify(TRIANGLE) collapse if requested (Raúl Marín)
* [4506](https://trac.osgeo.org/postgis/ticket/4506), Remove tolerance in point-in-ring tests (Paul Ramsey)
* [4338](https://trac.osgeo.org/postgis/ticket/4338), Fix Census block level data (tabblock table) loading (Regina Obe)
* [4519](https://trac.osgeo.org/postgis/ticket/4519), Fix getSRIDbySRS crash (Raúl Marín)
* [4517](https://trac.osgeo.org/postgis/ticket/4517), Documentation error ST\_EndPoint (Samuel Spurling)
* [4530](https://trac.osgeo.org/postgis/ticket/4530), Avoid bogus AddRasterConstraint in upgrade tests (Sandro Santilli)
* [4534](https://trac.osgeo.org/postgis/ticket/4534), Fix leak in lwcurvepoly_from_wkb_state (Raúl Marín)
* [4536](https://trac.osgeo.org/postgis/ticket/4536), Fix leak in lwcollection_from_wkb_state (Raúl Marín)
* [4537](https://trac.osgeo.org/postgis/ticket/4537), Fix leak in WKT collection parser (Raúl Marín)
* [4547](https://trac.osgeo.org/postgis/ticket/4547), Fix AddRasterConstraints handling of empty tables (Sandro Santilli)
* [4549](https://trac.osgeo.org/postgis/ticket/4549), Fix schema qualification of internal types (Raúl Marín)
* [4546](https://trac.osgeo.org/postgis/ticket/4546), Fix PLPGSQL functions missing the schema qualification (Raúl Marín)
* [4588](https://trac.osgeo.org/postgis/ticket/4588), Fix update when ST\_union(geometry) doesn't exist (Raúl Marín)
* [4599](https://trac.osgeo.org/postgis/ticket/4599), ST\_AddPoint: Accept -1 as a valid position (Raúl Marín)
* [4605](https://trac.osgeo.org/postgis/ticket/4605), Fix postgis_upgrade.pl with PostgreSQL 12 (Matti Linnanvuori)
* [4621](https://trac.osgeo.org/postgis/ticket/4621), Prevent stack overflow when parsing WKB (Raúl Marín)
* [4626](https://trac.osgeo.org/postgis/ticket/4626), Support pkg-config for libxml2 (Bas Couwenberg)
* [4646](https://trac.osgeo.org/postgis/ticket/4646), Fix gserialized_cmp incorrect comparison (dkvash)

