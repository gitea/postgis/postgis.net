---
title: PostGIS 3.1.0beta1
layout: post
category: news
tags: [release, 3.1]
author: Paul Ramsey
date: "2020-12-09"
thumbnail:
---

The PostGIS Team is pleased to release the first beta of the upcoming PostGIS
3.1.0 release. This version is exposes some of
the new performance and feature enhancements in not yet relesed GEOS 3.9 as well as numerous speed enhancements not requiring newer GEOS.
Requires GEOS 3.6+ and PostgreSQL 9.6+. To use MVT you will need protobuf-c 1.1. or higher.

Best served with:

* [PostgreSQL 13.1](https://www.postgresql.org/about/news/postgresql-131-125-1110-1015-9620-and-9524-released-2111/),
GEOS 3.7 or higher is recommended.
* [pgRouting 3.1.1](https://lists.osgeo.org/pipermail/pgrouting-dev/2020-November/002149.html)

<!--more-->

**3.1.beta1**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.0beta1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.0beta1/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.0beta1.pdf)

Details of this release can be found in the NEWS file:
https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.1.0beta1/NEWS

If you come across any issues, feel free to report via our ticket tracker
https://trac.osgeo.org/postgis or mailing list with details as described
here - https://postgis.net/support/

View all [tickets for 3.1.0 milestone][1].


**Breaking changes**

  * #4214, Deprecated ST_Count(tablename,...), ST_ApproxCount(tablename, ...)
           ST_SummaryStats(tablename, ..),
           ST_Histogram(tablename, ...), ST_ApproxHistogram(tablename, ...),
           ST_Quantile(tablename, ...), ST_ApproxQuantile(tablename, ...) removed.
           (Darafei Praliaskouski)

**Enhancements**

  * #4801, ST_ClusterKMeans supports weights in POINT[Z]M geometries
  		   (Darafei Praliaskouski)
  * #4804, ST_ReducePrecision (GEOS 3.9+) allows valid precision
           reduction (Paul Ramsey)
  * #4805, _ST_SortableHash exposed to work around parallel soring performance issue
           in Postgres. If your table is huge, use ORDER BY _ST_SortableHash(geom)
           instead of ORDER BY geom to make parallel sort faster (Darafei Praliaskouski)
  * #4625, Correlation statistics now calculated.
           Run ANALYZE for BRIN indexes to start kicking in.
           (Darafei Praliaskouski)
  * Fix axis order issue with urn:ogc:def:crs:EPSG in ST_GeomFromGML()
           (Even Roualt)


[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.1.0
