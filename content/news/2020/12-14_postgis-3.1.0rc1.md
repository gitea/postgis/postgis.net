---
title: PostGIS 3.1.0rc1
layout: post
category: news
tags: [release, 3.1]
author: Paul Ramsey
date: "2020-12-14"
thumbnail:
---

The PostGIS Team is pleased to release the release candidate of the upcoming PostGIS 3.1.0 release. This version is exposes some of the new performance and feature enhancements in GEOS 3.9 as well as numerous speed enhancements not requiring newer GEOS.

Best served with:

* [PostgreSQL 13.1](https://www.postgresql.org/about/news/postgresql-131-125-1110-1015-9620-and-9524-released-2111/)
* [GEOS 3.9](https://lists.osgeo.org/pipermail/geos-devel/2020-December/010003.html)
* [pgRouting 3.1.1](https://lists.osgeo.org/pipermail/pgrouting-dev/2020-November/002149.html)

<!--more-->

* Requires GEOS 3.6+ and PostgreSQL 9.6+.
* MVT support requires protobuf-c 1.1 or higher.

**3.1.rc1**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.1.0rc1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.1.0rc1/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.1.0rc1.pdf)

Details of this release can be found in the NEWS file:
https://git.osgeo.org/gitea/postgis/postgis/src/tag/3.1.0rc1/NEWS

If you come across any issues, feel free to report via our ticket tracker
https://trac.osgeo.org/postgis or mailing list with details as described
here - https://postgis.net/support/

View all [tickets for 3.1.0 milestone][1].


[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.1.0
