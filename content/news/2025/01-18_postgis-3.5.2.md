---
title: PostGIS 3.5.2
layout: post
category: news
tags: [release, 3.5]
author: Regina Obe
thumbnail:
date: "2025-01-18"
---

The PostGIS Team is pleased to release PostGIS 3.5.2.

This version requires PostgreSQL 12 - 17, GEOS 3.8 or higher, and Proj 6.1+.
To take advantage of all features, GEOS 3.12+ is needed.
SFCGAL 1.4+ is needed to enable postgis_sfcgal support.
To take advantage of all SFCGAL features, SFCGAL 1.5+ is needed.

### 3.5.2

* [source download]({{< loc "postgis.release_source">}}/postgis-3.5.2.tar.gz) [md5]({{< loc "postgis.dev_download">}}/postgis-3.5.2.tar.gz.md5)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.5.2/NEWS)
* PDF docs: [en]({{< loc "postgis.release_docs">}}/postgis-3.5.2-en.pdf) [ja]({{< loc "postgis.release_docs">}}/postgis-3.5.2-ja.pdf), [fr]({{< loc "postgis.release_docs">}}/postgis-3.5.2-fr.pdf), [zh_Hans]({{< loc "postgis.release_docs">}}/postgis-3.5.2-zh_Hans.pdf)

* HTML Online [en]({{< loc "site.root">}}docs/manual-3.5/en/) [ja]({{< loc "site.root">}}docs/manual-3.5/ja/) [fr]({{< loc "site.root">}}docs/manual-3.5/fr/) [zh_Hans]({{< loc "site.root">}}docs/manual-3.5/zh_Hans/)

* Cheat Sheets:
  * postgis: [en]({{< loc "site.root">}}docs/manual-3.5/postgis_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.5/postgis_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.5/postgis_cheatsheet-fr.html) [zh_Hans]({{< loc "site.root">}}docs/manual-3.5/postgis_cheatsheet-zh_Hans.html) 
  * postgis_raster: [en]({{< loc "site.root">}}docs/manual-3.5/raster_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.5/raster_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.5/raster_cheatsheet-fr.html) [zh_Hans]({{< loc "site.root">}}docs/manual-3.5/raster_cheatsheet-zh_Hans.html)
  * postgis_topology: [en]({{< loc "site.root">}}docs/manual-3.5/topology_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.5/topology_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.5/topology_cheatsheet-fr.html) [zh_Hans]({{< loc "site.root">}}docs/manual-3.5/topology_cheatsheet-zh_Hans.html)
  * postgis_sfcgal: [en]({{< loc "site.root">}}docs/manual-3.5/sfcgal_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.5/sfcgal_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.5/sfcgal_cheatsheet-fr.html) [zh_Hans]({{< loc "site.root">}}docs/manual-3.5/sfcgal_cheatsheet-zh_Hans.html) 
  * address standardizer, postgis_tiger_geocoder: [en]({{< loc "site.root">}}docs/manual-3.5/tiger_geocoder_cheatsheet-en.html) [ja]({{< loc "site.root">}}docs/manual-3.5/tiger_geocoder_cheatsheet-ja.html) [fr]({{< loc "site.root">}}docs/manual-3.5/tiger_geocoder_cheatsheet-fr.html) [zh_Hans]({{< loc "site.root">}}docs/manual-3.5/tiger_geocoder_cheatsheet-zh_Hans.html)


This release is a bug fix release that includes bug fixes since PostGIS 3.5.1.


<!--more-->

### Bug fixes

- [#5677](https://trac.osgeo.org/postgis/ticket/5677), Retain SRID during unary union (Paul Ramsey)
- [#5833](https://trac.osgeo.org/postgis/ticket/5833), pg_upgrade fix for postgis_sfcgal (Regina Obe)
- [#5564](https://trac.osgeo.org/postgis/ticket/5564), BRIN crash fix and support for parallel in PG17+
         (Paul Ramsey, Regina Obe)

### Upgrading

After installing the binaries or after running pg_upgrade:

For PostGIS 3.4, 3.3, 3.2, 3.1, 3.0
do below which will upgrade all your postgis extensions.
```postgres
SELECT postgis_extensions_upgrade();
```


For PostGIS 2.5 and below do:

```postgres
ALTER EXTENSION postgis UPDATE;
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```

If you come across any issues, feel free to report via our [ticket tracker](https://trac.osgeo.org/postgis)
or [mailing list](https://lists.osgeo.org/listinfo/postgis-users) with details

View all [tickets for 3.5.2 milestone][1].

[1]: https://trac.osgeo.org/postgis/query?status=assigned&status=closed&milestone=PostGIS+3.5.2&milestone=PostGIS+3.4.3&milestone=PostGIS+3.4.3&milestone=PostGIS+3.3.8
