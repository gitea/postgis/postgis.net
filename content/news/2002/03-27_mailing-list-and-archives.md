---
layout: post
category: news
title: Mailing List and Archives
author: Paul Ramsey
date: "2002-03-27"
---

<p>
The [postgis-users] mailing list has been moved onto the main
PostGIS site,along with the online list archives. Subscription
information, archives, and searching are all available from the
<a href='http://lists.osgeo.org/mailman/listinfo/postgis-users/'>postgis-users page</a>.
</p>
