---
layout: post
category: news
title: PostGIS in GeoWorld Magazine
author: Paul Ramsey
date: "2002-08-01"
---

<p>
The August 2002 issue of
<A TARGET='gss' HREF='http://www.geoplace.com/gw'>GeoWorld</A>
magazine includes a
<A TARGET='gss' HREF='http://www.geoplace.com/gw/2002/0208/0208gis.asp'>feature article</A>
by Paul Ramsey reviewing open source GIS technology, specifically
PostGIS/PostgreSQL and UMN Mapserver.
</p>