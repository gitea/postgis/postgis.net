---
layout: post
category: news
title: GeoTec 2002 Presentation
author: Paul Ramsey
date: "2002-05-15"
---

<p>
On April 9, 2002, Paul Ramsey gave a presentation at GeoTec2002 on open-source relational
databases in a session on open-source GIS technologies. The presentation
slides are available
(<A HREF='/files/osdb-geotec2002.ppt'>PPT</A>|<A HREF='/files/osdb-geotec2002jpn.ppt'>PPT Japanese</A>).
</p>
