---
layout: post
category: news
title: PostGIS in Geospatial Solutions Magazine
author: Paul Ramsey
date: "2002-06-10"
---

<p>
The June 2002 issue of
<A TARGET='gss' HREF='http://www.geospatial-online.com'>Geospatial Solutions</A>
magazine includes a
<A TARGET='gss' HREF='http://www.geospatial-online.com/geospatialsolutions/article/articleDetail.jsp?id=19932'>column</A>
by Jonathan Lowe reviewing open source GIS technology, with some
great references to UMN Mapserver and PostGIS/PostgreSQL.
</p>