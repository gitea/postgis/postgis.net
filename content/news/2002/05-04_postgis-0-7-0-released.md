---
layout: post
category: news
title: PostGIS 0.7.0 Released
author: Paul Ramsey
tags: ["release", "0.7", "0.7.0"]
date: "2002-05-04"
---

<p>
Version 0.7.0 includes two major improvements over the last release:
</p>
<ul>
<li>Support for PostgreSQL 7.2</li>
<li>Support for coordinate reprojection inside the database</li>
</ul>
<p>
In addition there have been numerous bug fixes and minor improvements.
</p>