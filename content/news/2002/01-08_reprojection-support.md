---
layout: post
category: news
title: Reprojection Support
tags: ["proj"]
author: Paul Ramsey
date: "2002-01-08"
---

<p>
The CVS version currently is way ahead of the release version in feature
richness. Apologies, but this is because features are being added for
consulting projects and we do not have time to do a release and all the
accompanying documentation etc.
</p>
<p>
Reprojection is supported via the PROJ4 library. The SQL function name
is <tt>transform(&lt;geom&gt;,&lt;srid&gt;)</tt>. Now you have a reason to populate that
SPATIAL_REF_SYS table like you've always wanted. Note however that the
SPATIAL_REF_SYS table now requires a PROJ4TEXT column containing the
PROJ4 definition string of the spatial reference system.
</p>