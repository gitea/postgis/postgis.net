---
layout: post
category: news
title: PostgreSQL 7.2 Index Project
author: Paul Ramsey
date: "2002-05-25"
---

<p>
The first group-funded PostGIS development project has committed results
to the CVS!
<A HREF='http://www.intevation.de'>Intevation</A>,
Imagelinks and
Rod Anders of <A HREF='http://www.acm.org'>ACM</A> all contributed to the
funding of the 7.2 index support project. Chris Hodgson and Dave Blasby of
<A HREF='http://www.refractions.net'>Refractions Research</A> did the work.
</p>
<P>
The goal of the project was to
expedite the support of PostgreSQL 7.2 GiST indexing in PostGIS. The new
indexes are more robust (null safe) and allow PostGIS users to take
advantage of all the other
PostgreSQL 7.2 enhancements
in their spatial databases.
<P>The project also included funding to bring out a documented tarball release
including the 7.2 indexes and all other enhancements placed in CVS since the
0.6.2 release. The full 0.7 release should be out by May 3rd.
</p>
