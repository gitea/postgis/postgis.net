---
layout: post
category: news
title: PostGIS 0.7.2 Released
author: Paul Ramsey
tags: [release, "0.7"]
date: "2002-09-01"
---

<p>
Version 0.7.2 includes a number of fixes to the pgsql2shp and shp2pgsql
support programs. Some fixes in obscure functions and a little bit of
preparation for PgSQL 7.3 are also included.
</p>