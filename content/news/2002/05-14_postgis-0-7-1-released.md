---
layout: post
category: news
title: PostGIS 0.7.1 Released
author: Paul Ramsey
tags: ["release", "0.7"]
date: "2002-05-14"
---

<p>
Version 0.7.1 includes an important bug fix to the spatial indexing
and fixes to report compilation problems.
</p>