---
layout: post
category: news
title: Mapserver Users Meeting Presentations
author: Paul Ramsey
date: "2003-06-10"
---

<p>
David Blasby and Chris Hodgson, gave a presentation and workshop on
using Mapserver and PostGIS to create web mapping sites which embed
interesting analytical results not normally producable using flat file
data.  The talk materials are available online: a
<A HREF="/files/mum-presentation2003.ppt">presentation</A>, a
<A HREF="/files/mum-workshop2003.doc">workshop guide</A>, and an
<A HREF="/files/mum-mapfile.map">example mapfile</A>.
</p>
