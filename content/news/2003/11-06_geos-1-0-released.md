---
layout: post
category: news
title: GEOS 1.0 Released!
author: Paul Ramsey
date: "2003-11-06"
---

<p>
The first release of GEOS (geometry engine, open source) has been released
at <A HREF='https://libgeos.org/'>https://libgeos.org/</A>.
Using the GEOS library, it is possible for PostGIS to perform all the
operations in the OpenGIS Simple Features for SQL Specification.
</p>
<p>
That means that operations like Touches(), Contains(), Intersection(),
Buffer(), Centroid(), Union() can all be supported.  To try out
PostGIS-with-GEOS, download the <A HREF='{{< loc "postgis.release_source">}}/download/'>CVS snapshot of PostGIS</A>
 and follow the directions in the README file carefully.
</p>
