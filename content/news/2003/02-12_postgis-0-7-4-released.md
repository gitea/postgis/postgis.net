---
layout: post
category: news
title: PostGIS 0.7.4 Released
author: Paul Ramsey
tags: [release, 0.7]
date: "2003-02-12"
---

<p>
PostGIS 0.7.4 has been released. This release is a maintenance release, including some
important bug fixes to the <code>shp2pgsql</code> data loader and miscellaneous
fixes and code scrubbing throughout the system. Version 7.3.X of
<A HREF='http://www.postgresql.org'>PostgreSQL</A> is now supported, although explicit support
for "schemas" has not been added yet.
</p>
<p>
The next release of PostGIS will include support for spatial predicates
(Relate(), Contains(), Touches(), Disjoint(), etc), based on the
<A HREF='https://libgeos.org/'>GEOS</A> library of spatial functions.
Early releases of this functionality will be available in CVS shortly and will
be announced here and on the <A HREF='http://lists.osgeo.org/mailman/listinfo/postgis-users'>users mailing list</A>.
</p>
