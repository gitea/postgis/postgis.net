---
layout: post
category: news
title: PostGIS 0.8.0 Released!
author: Paul Ramsey
tags: [release, 0.8]
date: "2003-11-24"
---

<p>
We are very happy to announce the release of PostGIS 0.8.0,
the first version of PostGIS that supports every function in the OpenGIS
"Simple Features for SQL" specification document.  The PostGIS 0.8.0
source code includes the OpenGIS SFSQL conformance test suite, so give
it a try and see how we do!
</p>
<p>
Version 0.8 includes such useful SFSQL functions as Within(),
Disjoint(), Touches(), GeomUnion(), Intersection(), Buffer(), and many
many more! We also include some spatial aggregate functions, GeomUnion()
and Collect(), that can roll up spatial result sets into resultants
based on attribute grouping.
</p>
<p>
Version 0.8.0 also includes support for the newly released
<a HREF='http://www.postgresql.org'>PostgreSQL</a> version 7.4. Version 7.4
has numerous performance improvements over 7.3 so it may well be worth
an upgrade.
</p>