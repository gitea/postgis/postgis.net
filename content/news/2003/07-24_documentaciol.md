---
layout: post
category: news
title: Documentaci&#243;n de PostGIS en espa&#241;ol
author: Paul Ramsey
date: "2003-07-24"
---

<p>
El manual de PostGIS fue traducido al espa&#241;ol por Manuel Mart&#237;n Mart&#237;n.
Puede descargarlo <A HREF=postgis-spanish.pdf>aqu&#237;</A>.
</p>