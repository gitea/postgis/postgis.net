---
layout: post
category: news
title: PostGIS 0.7.5 Released, PostGIS 0.8 Coming Soon
author: Paul Ramsey
tags: [release, 0.7]
date: "2003-04-08"
---

<p>
We have released a 0.7.5 version with all the latest bug fixes from the CVS,
mostly concentrated in obscure cases in the <CODE>shp2pgsql</CODE> loader.
The CVS version is now in flux pending the release of the 0.8 series,
which will feature the first spatial predicates and operators, via a link
to the <A HREF='https://libgeos.org'>GEOS</A> spatial topology
library. The 0.8 series will be out relatively soon, once bugs in the
GEOS library have been removed and the new 0.8 PostGIS given some
rigorous testing.
</p>
