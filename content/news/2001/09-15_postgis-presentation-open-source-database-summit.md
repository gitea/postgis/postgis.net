---
layout: post
category: news
title: PostGIS Presentation @ Open Source Database Summit
author: Paul Ramsey
date: "2001-09-15"
---

<p>
Dave Blasby will be giving a presentation on spatial databases in
general and PostGIS in particular at the Open Source Database
Summit in Providence, Rhode Island on September 25, 2001.
</p>