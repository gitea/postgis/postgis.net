---
layout: post
category: news
title: Pre-Release 0.1
tags: ["release", "0.1"]
author: Paul Ramsey
date: "2001-05-31"
---

<p>
This release is largely untested and
does not include data loading. The JDBC and documentation are pretty primitive
as well. Please try it and send feedback to the mailing list.
</p>