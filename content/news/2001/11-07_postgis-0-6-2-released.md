---
layout: post
category: news
title: PostGIS 0.6.2 Released
author: Paul Ramsey
tags: ["release", "0.6"]
date: "2001-11-07"
---

<p>
Version 0.6.2 is mostly a bug fix release with fixes for
the Shape file loader. There is also a SQL file full of spatial reference
definitions to load into the SPATIAL_REF_SYS table.
</p>
<p>
Astute observers will note the addition of the <code>postgis_chip.c</code>
file. This is the start of support for raster datatypes. There is no
documentation on this because it is still in development and completely
unstable.
</p>
<p>
The basic idea is that large rasters can be subdivided into
smaller chips for storage. The chips will be small enough to fit within
the PostgreSQL page size (about 8kb). The chips will be indexable
(eventually) and there will be client functions available to retrieve
sets of chips and aggregate them into a single image. The result will be
random access raster coverages.
</p>
<p>
Documentation and relatively stable raster support will be part of another
0.6 minor version release in the future.
</p>