---
layout: post
category: news
title: "About PostgreSQL 7.2B"
author: Paul Ramsey
date: "2001-12-17"
---

<p>
PostgreSQL 7.2 is currently in beta, and people might be tempted to try
to use it with PostGIS 0.6.2. Unfortunately, due to a change in the API
hooks for the GiST index, PostGIS 0.6.2 <em>will <b>not</b> work</em> with
PostgreSQL 7.2. When PostgreSQL 7.2 is officially released, work on converting
PostGIS to support the new indexes will begin. We might drop support
for the 7.1 series at that time.
</p>