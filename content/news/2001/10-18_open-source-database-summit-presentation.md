---
layout: post
category: news
title: Open Source Database Summit Presentation
tags: ["osdb2", "events"]
author: Paul Ramsey
date: "2001-10-18"
---

<p>
The slideshow Dave used for his
OSDB2 presentation is
available (<A HREF="/files/OSDB2_PostGIS_Presentation.pdf">PDF</A>|<A HREF="/files/OSDB2_PostGIS_Presentation.ppt">PPT</A>|<A HREF="/files/OSDB2_PostGIS_Presentation_Japanese.zip">Japanese PPT</A>).
Without his inimitable delivery, it is not nearly the same. :)
</p>
