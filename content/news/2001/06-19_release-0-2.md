---
layout: post
category: news
title: Release 0.2
tags: ["release", "0.2"]
author: Paul Ramsey
date: "2001-06-19"
---

<p>
Several new functions have been added,
most notably support for Well-Known Binary. Documentation is getting better
and there is a WKB example application. Some testing has been done and
retrieval speed on a million-tuple table with a GiST index for a small query
frame is under 0.1 seconds on our test machine. Looking very good!
</p>