---
layout: post
category: news
title: New Web Site and Release 0.6.1
tags: ["release", "0.6"]
author: Paul Ramsey
date: "2001-10-15"
---

<p>
We have a new, hopefully more aesthetically pleasing, site design now! At
a minimum, the multiple-page approach gives more space for adding information.
</p>
<p>
Also, version <A HREF="{{< loc "postgis.release_source">}}/postgis-0.6.1.tar.gz">0.6.1</A> has been released,
with improvements to the loader/dumper and some cygwin bug fixes.
</p>
