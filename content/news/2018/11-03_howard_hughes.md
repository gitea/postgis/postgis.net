---
title: Howard Hughes Medical Institute
layout: post
category: casestudy
tags: [bioinformatics, neuroscience, research, us]
author: Tom Kazimiers
date: "2018-11-03"
---

As a software engineer at the Howard Hughes Medical Institute, I work on a collaborative neuron reconstruction and analysis software called CATMAID [1] [2] (screenshot: [3]), which is used for neuroscience research. We use PostGIS to represent neurons in a 3D space.
<!--more-->

They consist of 3D points that reference their parent nodes or are the root [=soma of neuron] if they have no parent). Together with synapses, point clouds and TIN meshes for modeling compartments in a dataset, they model the spatial aspects of our neuroscience world. Users create those neuron reconstructions manually in a collaborative fashion plus segmentation programs can be used as additional data source. Using its spatial indices, PostGIS helps us to quickly query neurons in a particular field of view. The space of a single project contains sometimes 100s of millions of interconnected individual points. We also do bounding box intersection queries between neurons and compartment meshes, which then refine in the front-end by doing more precise intersection tests.

This software is used by quite a few research labs and as far as I know they all do their own hosting with a dedicated server and this is what we do as well. The reason being mainly that wth larger datasets, we benefit from machines with a lot of RAM (>256G), fast SSD/NVMe drives and many CPUs as well as fast local data access for e.g. image data.

Thanks so much for making PostGIS work well in non-GIS contexts too---it makes my life much easier!

[1]: https://www.catmaid.org
[2]: https://github.com/catmaid/CATMAID
[3]: https://twitter.com/tomkazimiers/status/1057657843174772737
