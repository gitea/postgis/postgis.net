---
title: Vanguard Appraisals
layout: post
category: casestudy
tags: ["property appraisal", us]
author: Andy Colson
date: "2018-11-02"
---

Vanguard Appraisals is new to the GIS world. In fact, we aren't really in the GIS world; we just kind of brush up against it.  We do mass property appraisal for entire county and city jurisdictions, and we develop software to collect, price and maintain values.  We also host assessment data online so that homeowners can search and find property information much simpler from the comfort of their own home.  Our software and websites are used in 7 states (IA, IL, MN, MO, NE, ND, SD).

<!--more-->


We were happy in our little world, doing parcel stuff, not really knowing about or using GIS, but then the landscape started changing, and GIS started popping up all over the place.  Our clients starting asking us if we could host their GIS data, as well as their parcel data.  Some of our clients are very small; there is one person in the Assessor's office with one computer, no server, and a very small internet pipe. Some of our clients are big with many users, multiple servers, and an internet pipe that makes me blush. :-)

We searched and found something that already worked with our favorite database: PostgreSQL.  PG is already hosting our parcel data, so it seemed like a good idea to let it host our GIS data too. Using PostGIS combined with MapServer, Perl and OpenLayers, we came up with online maps that fit the bill:

1. Great performance.
2. Sql:  the ability to write sql to join our existing parcel data with GIS data makes it simple to work with, powerful, and fast.
3. Free:  because we didn't pay for anything, we didn't charge anything.  Government Assessor's offices don't have to charge tax payers to get their GIS online.

PostGIS has been a great decision.

When one of our programmers came up with a crazy idea about doing a sales ratio analysis and highlighting all the properties on the map, not only was it possible but not that hard to do, and it has already been implemented because of PostGIS.

Sometimes we get data w/out measurements, so when we are routing and lot sizing a map, sometimes I've gone low level to add measurements. (I break up each line of a parcel, measure it, and add an annotation.  All in sql, thousands of parcels all at once.)

The gis parcel data for a lot/parcel may not be correct when we get it, but once measured we can compare the gis data to the parcel data to know if we should remeasure when we visit the property.  It gives us a starting point, and helps organize and prepare the data.

I think the biggest thing is the ease of joining other data to the gis maps.  Wanna combine the gis data with parcel data and show res acres vs agland acres.  Easy.  Color the map with parcel zoning?  Tax districts?  Easy and Easy.

I have not used ESRI software very much, so maybe I'm wrong about it, but it seems painful to join maps and databases.  We've had to add view's on top of the database just so ESRI could work with it.  There are some file formats it works ok with, and some that are very painful.

Not PostGIS, anything your twisted mind can work up, it'll do.  I'd bet I can write a perl stored proc that'll download a webpage, joins that to a redis fdw, join that to a csv, join that to active directory, and join that to Tiger data.

Another thing we do on the maps is find all parcels 100 feet from this point.  Or 200 feet from this parcel.

We also do heat maps for land pricing.  Plot all the land rates for an area or subdivision to make sure nobody is too different.  Similar pricing of similar land is important.  Similar area <> similar land, but its a good start.

I also cannot stress enough how good and helpful the online community has been.  I went from knowing nothing about GIS to hosting maps only because of them and all the questions they helped with over the years.

Vanguard Appraisals:  <http://camavision.com/>

Assessment Data:  <http://iowaassessors.com/>

We host parcel data for all the yellow links, but we don't host the maps for all of them.  Some counties we host maps for are: Washington MN, Jasper IA, Van Buren IA, Iowa City IA.  Just over 50 counties so far.

You can find the map embedded into the parcel page:
<https://vanburen.iowaassessors.com/parcel.php?parcel=000600307304130>

or Full Page:
<https://maps.camavision.com/map/jasperia>
