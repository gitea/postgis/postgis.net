---
title: PostGIS 2.5.0rc1
layout: post
category: news
tags: [release,2.5]
author: Regina Obe
date: "2018-08-19"
thumbnail:
---

The PostGIS development team is pleased to release PostGIS 2.5.0rc1.

This release is a work in progress.
Remaining time will be focused on bug fixes and documentation until PostGIS 2.5.0 release.
Although this release will work for PostgreSQL 9.4 and above, to take full advantage of
what PostGIS 2.5 offers, you should be running PostgreSQL 11beta3+ and GEOS 3.7.0rc1
which were released recently.

Best served with PostgreSQL 11beta3 which was recently released.

Changes since PostGIS 2.5.0beta2 release are as follows:

* 4146, Fix compilation error against Postgres 12 (Raúl Marín).
* 4147, 4148, Honor SOURCE_DATE_EPOCH when present (Christoph Berg).


View all [closed tickets for 2.5.0][1].

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.

**2.5.0rc1**

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.0rc1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.0rc1/NEWS)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.5.0rc1.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.5.0rc1.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.5.0rc1.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.5.0rc1/ChangeLog)

[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+2.5.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
