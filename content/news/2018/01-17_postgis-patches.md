---
title: PostGIS Patch Releases 2.3.6 and 2.4.3
layout: post
category: news
tags: [release, 2.3, 2.4]
author: Regina Obe
date: "2018-01-17"
thumbnail:
---

The PostGIS development team is pleased to provide
bug fix release 2.3.6 and 2.4.3
for the 2.3 and 2.4 stable branches.

Key fixes in these releases are Brin upgrade,
ST_Transform schema qualification to fix issues with restore, foreign table,
and materialized view use, ClusterKMeans and encoded polyline fixes.

View all [closed tickets for 2.4.3 and 2.3.6][1].

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.

**2.3.6**

* [source download]({{< loc "postgis.release_source">}}/postgis-2.3.6.tar.gz)
* [NEWS](https://svn.osgeo.org/postgis/tags/2.3.6/NEWS)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.3.6.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.3.6.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.3.6.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.3.6/ChangeLog)

**2.4.3**

* [source download]({{< loc "postgis.release_source">}}/postgis-2.4.3.tar.gz)
* [NEWS](https://svn.osgeo.org/postgis/tags/2.4.3/NEWS)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.3.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.4.3.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.3.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.4.3/ChangeLog)

[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+2.4.3&milestone=PostGIS+2.3.6&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
