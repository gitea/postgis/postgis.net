---
title: PostGIS Patch Release 2.4.5
layout: post
category: news
tags: [release, 2.4]
author: Paul Ramsey
date: "2018-09-12"
thumbnail:
---

The PostGIS development team is pleased to provide
bug fix 2.4.5 for the 2.4 stable branch.

View all [closed tickets for 2.4.5][1].

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.

**2.4.5**

* [source download]({{< loc "postgis.release_source">}}/postgis-2.4.5.tar.gz)
* [NEWS](https://svn.osgeo.org/postgis/tags/2.4.5/NEWS)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.5.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.4.5.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.5.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.4.5/ChangeLog)

[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+2.4.5&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
[2]: https://trac.osgeo.org/postgis/wiki/UsersWikiPostgreSQLPostGIS
