---
title: PostGIS 2.5.0rc2
layout: post
category: news
tags: [release,2.5]
author: Regina Obe
date: "2018-09-16"
thumbnail:
---

The PostGIS development team is pleased to release PostGIS 2.5.0rc2.

Although this release will work for PostgreSQL 9.4 and above, to take full advantage of
what PostGIS 2.5 offers, you should be running PostgreSQL 11beta3+ and GEOS 3.7.0
which were released recently.

Best served with PostgreSQL 11beta3.

WARNING: If compiling with PostgreSQL 11+JIT, LLVM >= 6 is required

**2.5.0rc2**

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.0rc2.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.0rc2/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.5.0rc2.pdf), [de]({{< loc "postgis.release_docs">}}/postgis-2.5.0rc2-de.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.5.0rc2.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.5.0rc2.epub)
* pgsql help files for non-english languages: [de]({{< loc "postgis.release_docs">}}/pgsql-help-2.5-de.tar.gz), [ja]({{< loc "postgis.release_docs">}}/pgsql-help-2.5-ja.tar.gz),[kr]({{< loc "postgis.release_docs">}}/pgsql-help-2.5-kr.tar.gz), [br]({{< loc "postgis.release_docs">}}/pgsql-help-2.5-br.tar.gz), [es]({{< loc "postgis.release_docs">}}/pgsql-help-2.5-es.tar.gz)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.5.0rc2/ChangeLog)

Changes since PostGIS 2.5.0rc1 release are as follows:

* 4162, ST_DWithin documentation examples for storing geometry and
radius in table (Darafei Praliaskouski, github user Boscop).
* 4163, MVT: Fix resource leak when the first geometry is NULL (Raúl Marín)
* 4172, Fix memory leak in lwgeom_offsetcurve (Raúl Marín)
* 4164, Parse error on incorrectly nested GeoJSON input (Paul Ramsey)
* 4176, ST_Intersects supports GEOMETRYCOLLECTION (Darafei Praliaskouski)
* 4177, Postgres 12 disallows variable length arrays in C (Laurenz Albe)
* 4160, Use qualified names in topology extension install (Raúl Marín)
* 4180, installed liblwgeom includes sometimes getting used
	   instead of source ones (Regina Obe)


View all [closed tickets for 2.5.0][1].

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+2.5.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
