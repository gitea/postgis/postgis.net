---
title: PostGIS 2.5.0beta1
layout: post
category: news
tags: [release,2.5]
author: Regina Obe
date: "2018-07-03"
thumbnail:
---

The PostGIS development team is pleased to release PostGIS 2.5.0beta1.

This release is a work in progress. No more api changes will be made from this point on before 2.5.0 release.
Remaining time will be focused on bug fixes and documentation for the new functionality and performance enhancements under the covers.
Although this release will work for PostgreSQL 9.4 and above, to take full advantage of
what PostGIS 2.5 will offer, you should be running PostgreSQL 11beta2+ and GEOS 3.7.0beta1
which were released recently.

Best served with PostgreSQL 11beta2 which was recently released.

View all [closed tickets for 2.5.0][1].

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.

**2.5.0beta1**

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.0beta1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.0beta1/NEWS)
* [pdf doc download]({{< loc "postgis.release_docs">}}/postgis-2.5.0beta1.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.5.0beta1.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.5.0beta1.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.5.0beta1/ChangeLog)

[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+2.5.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
