---
title: PostGIS 2.5.0
layout: post
category: news
tags: [release,2.5]
author: Regina Obe
date: "2018-09-23"
thumbnail:
---

The PostGIS development team is pleased to release PostGIS 2.5.0.

Although this release will work for PostgreSQL 9.4 and above, to take full advantage of
what PostGIS 2.5 offers, you should be running PostgreSQL 11beta4+ and GEOS 3.7.0
which were released recently.

Best served with [PostgreSQL 11 beta4](https://www.postgresql.org/docs/11/static/index.html)
and [pgRouting 2.6.1](https://github.com/pgRouting/pgrouting/releases/tag/v2.6.1).

WARNING: If compiling with PostgreSQL+JIT, LLVM >= 6 is required
Supported PostgreSQL versions for this release are:
PostgreSQL 9.4 - PostgreSQL 12 (in development)
GEOS >= 3.5

**2.5.0**

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.0.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.0/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.5.0.pdf), [de]({{< loc "postgis.release_docs">}}/postgis-2.5.0-de.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.5.0.tar.gz)
* html online [en]({{ site.root }}docs/manual-2.5), [de]({{ site.root }}docs/manual-2.5/postgis-de.html), [ko_KR]({{ site.root }}docs/manual-2.5/postgis-ko_KR.html), [ja]({{ site.root }}docs/manual-2.5/postgis-ja.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.5.0.epub)
* pgsql help files for non-english languages: [de]({{< loc "postgis.release_docs">}}/pgsql-help-2.5-de.tar.gz), [ja]({{< loc "postgis.release_docs">}}/pgsql-help-2.5-ja.tar.gz),[kr]({{< loc "postgis.release_docs">}}/pgsql-help-2.5-kr.tar.gz), [br]({{< loc "postgis.release_docs">}}/pgsql-help-2.5-br.tar.gz), [es]({{< loc "postgis.release_docs">}}/pgsql-help-2.5-es.tar.gz)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.5.0/ChangeLog)

<!--more-->

**New Features**

* 1847, spgist 2d and 3d support for PG 11+
    (Esteban Zimányi and Arthur Lesuisse from Université Libre de Bruxelles (ULB),
      Darafei Praliaskouski)
* 4056, ST_FilterByM (Nicklas Avén)
* 4050, ST_ChaikinSmoothing (Nicklas Avén)
* 3989, ST_Buffer single sided option (Stephen Knox)
* 3876, ST_Angle function (Rémi Cura)
* 3564, ST_LineInterpolatePoints (Dan Baston)
* 3896, PostGIS_Extensions_Upgrade() (Regina Obe)
* 3913, Upgrade when creating extension from unpackaged (Sandro Santilli)
* 2256, _postgis_index_extent() for extent from index (Paul Ramsey)
* 3176, Add ST_OrientedEnvelope (Dan Baston)
* 4029, Add ST_QuantizeCoordinates (Dan Baston)
* 4063, Optional false origin point for ST_Scale (Paul Ramsey)
* 4082, Add ST_BandFileSize and ST_BandFileTimestamp,
           extend ST_BandMetadata (Even Rouault)
* 2597, Add ST_Grayscale (Bborie Park)
* 4007, Add ST_SetBandPath (Bborie Park)
* 4008, Add ST_SetBandIndex (Bborie Park)

**Breaking Changes**

* 4054, ST_SimplifyVW changed from > tolerance to >= tolerance
* 3885, version number removed from address_standardize lib file
          (Regina Obe)
* 3893, raster support functions can only be loaded in the same schema
           with core PostGIS functions. (Sandro Santilli)
* 4035, remove dummy pgis_abs type from aggregate/collect routines.
           (Paul Ramsey)
* 4069, drop support for GEOS < 3.5 and PostgreSQL < 9.4 (Regina Obe)
* 4082, ST_BandMetaData extended to include filesize, timestamp (Even Rouault)

**Enhancements and Fixes**

* Upgrade scripts from multiple old versions are now all symlinks
    to a single upgrade script (Sandro Santilli)
* 3944, Update to EPSG register v9.2 (Even Rouault)
* 3927, Parallel implementation of ST_AsMVT
* 3925, Simplify geometry using map grid cell size before generating MVT
* 3899, BTree sort order is now defined on collections of EMPTY and
           same-prefix geometries (Darafei Praliaskouski)
* 3864, Performance improvement for sorting POINT geometries
           (Darafei Praliaskouski)
* 3900, GCC warnings fixed, make -j is now working (Darafei Praliaskouski)
  - TopoGeo_addLinestring robustness improvements (Sandro Santilli)
    #1855, #1946, #3718, #3838
* 3234, Do not accept EMPTY points as topology nodes (Sandro Santilli)
* 1014, Hashable geometry, allowing direct use in CTE signatures (Paul Ramsey)
* 3097, Really allow MULTILINESTRING blades in ST_Split() (Paul Ramsey)
* 3942, geojson: Do not include private header for json-c >= 0.13 (Björn Esser)
* 3954, ST_GeometricMedian now supports point weights (Darafei Praliaskouski)
* 3965, #3971, #3977, #4071 ST_ClusterKMeans rewritten: better initialization,
           faster convergence, K=2 even faster (Darafei Praliaskouski)
* 3982, ST_AsEncodedPolyline supports LINESTRING EMPTY and MULTIPOINT EMPTY
           (Darafei Praliaskouski)
* 3986, ST_AsText now has second argument to limit decimal digits
           (Marc Ducobu, Darafei Praliaskouski)
* 4020, Casting from box3d to geometry now returns correctly connected
           PolyhedralSurface (Matthias Bay)
* 2508, ST_OffsetCurve now works with collections (Darafei Praliaskouski)
* 4006, ST_GeomFromGeoJSON support for json and jsonb as input
           (Paul Ramsey, Regina Obe)
* 4038, ST_Subdivide now selects pivot for geometry split that reuses input
           vertices. (Darafei Praliaskouski)
* 4025, #4032 Fixed precision issue in ST_ClosestPointOfApproach,
           ST_DistanceCPA, and ST_CPAWithin (Paul Ramsey, Darafei Praliaskouski)
* 4076, Reduce use of GEOS in topology implementation (Björn Harrtell)
* 4080, Add external raster band index to ST_BandMetaData
  - Add Raster Tips section to Documentation for information about
    Raster behavior (e.g. Out-DB performance, maximum open files)
* 4084: Fixed wrong code-comment regarding front/back of BOX3D (Matthias Bay)
* 4060, #4094, PostgreSQL JIT support (Raúl Marín, Laurenz Albe)
* 3960, ST_Centroid now uses lwgeom_centroid (Darafei Praliaskouski)
* 4027, Remove duplicated code in lwgeom_geos (Darafei Praliaskouski,
           Daniel Baston)
* 4115, Fix a bug that created MVTs with incorrect property values under
    parallel plans (Raúl Marín).
* 4120, ST_AsMVTGeom: Clip using tile coordinates (Raúl Marín).
* 4132, ST_Intersection on Raster now works without throwing TopologyException
          (Vinícius A.B. Schmidt, Darafei Praliaskouski)
* 4177, #4180 Support for PostgreSQL 12 dev branch (Laurenz Albe, Raúl Marín)
* 4156, ST_ChaikinSmoothing: also smooth start/end point of
  polygon by default (Darafei Praliaskouski)


View all [closed tickets for 2.5.0][1].

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+2.5.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
