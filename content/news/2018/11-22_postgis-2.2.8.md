---
title: PostGIS 2.2.8
layout: post
category: news
tags: [release,2.2,EOL]
author: Regina Obe
date: "2018-11-22"
thumbnail:
---

The PostGIS development team is pleased to provide
bug fix 2.2.8 for the 2.2 stable branch.

This is the End-Of-Life and final release for PostGIS 2.2 series.

We encourage you to upgrade to a newer minor PostGIS version.
Refer to our [Version compatibility and EOL Policy](/eol_policy)
for details on versions you can upgrade to.

This release supports PostgreSQL 9.1-9.6.

**2.2.8**

* [source download]({{< loc "postgis.release_source">}}/postgis-2.2.8.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.2.8/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.2.8.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.2.8.tar.gz)
* html online [en]({{ site.root }}docs/manual-2.2)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.2.8.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.2.8/ChangeLog)

<!--more-->

**Bug fixes**

* 2985, Avoid array overflow in ANALYZE (Paul Ramsey)
* 4160, Use qualified names in topology extension install (Raúl Marín)
* 4189, Fix undefined behaviour in SADFWrite (Raúl Marín)
* 4191, Fix undefined behaviour in ptarray_clone_deep (Raúl Marín)
* 4020, Fix leftovers in topology upgrade from 2.1 (Sandro Santilli)
* 4203, Fix build with GNU Make 4.1 (Sandro Santilli)
* 4206, Replace getrelid with rt_fetch
		(getrelid has been deprecated since PostgreSQL 9.1) (Laurenz Albe)
* 3457, Fix raster envelope shortcut in ST_Clip (Sai-bot)
* 4326, Allocate enough memory in gidx_to_string (Raúl Marín)
* 4249, Fix undefined behaviour in raster intersection (Raúl Marín)



View all [closed tickets for 2.2.8][1].

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+2.2.8&milestone=PostGIS+2.4.6&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
