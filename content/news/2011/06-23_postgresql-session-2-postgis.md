---
layout: post
category: news
title: "PostgreSQL Session #2: PostGIS"
tags: ["events", "pgsessions"]
author: Paul Ramsey
date: "2011-06-23"
---

A <a href="http://www.postgresql-sessions.org/en/2/start">free, 1-day conference</a> in Paris, France, focusing on PostGIS.

