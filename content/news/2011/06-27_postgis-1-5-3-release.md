---
layout: post
category: news
title: PostGIS 1.5.3 Release
author: Paul Ramsey
tags: [release, 1.5]
date: "2011-06-27"
---

<p>The PostGIS team is proud to announce the release of version <href='{{< loc "postgis.release_source">}}'>1.5.3</a>.
This is a minor release addressing a few issues that have been filed since the 1.5.2 release in addition to fixes that allow it to compile against PostgreSQL 9.1beta.</p>

<p><a href="/news/20110625/">Read more...</a></p>

