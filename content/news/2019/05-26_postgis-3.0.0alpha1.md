---
title: PostGIS 3.0.0alpha1
layout: post
category: news
tags: [release,3.0]
author: Regina Obe
date: "2019-05-26"
thumbnail:
---

The PostGIS development team is pleased to release PostGIS 3.0.0alpha1.

This release works with PostgreSQL 9.5-12beta1  and GEOS >= 3.6

Best served with PostgreSQL 12beta1.

<!--more-->

**3.0.0alpha1**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.0alpha1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.0alpha1/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.0alpha1.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.0alpha1.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.0alpha1.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/3.0.0alpha1/ChangeLog)

View all [closed tickets for 3.0.0][1].

Note that a major change in 3.0 is that the raster functionality has been broken out as a separate extension.
If you are upgrading from a previous edition, do the following:

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
-- this next step repackages raster in its own extension
-- and upgrades all your other related postgis extension
SELECT PostGIS_Extensions_Upgrade();

--if you don't use raster, you can do
DROP EXTENSION postgis_raster;
```

-- New users
```postgres
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_raster;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION postgis_tiger_geocoder;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.0.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
