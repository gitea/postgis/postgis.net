---
title: PostGIS 2.5.2, 2.4.7, 2.3.9 Released
layout: post
category: news
tags: [release,2.5,2.4,2.3]
author: Regina Obe
date: "2019-03-11"
thumbnail:
---

The PostGIS development team is pleased to provide
bug fix 2.5.2, 2.4.7, and 2.3.9 for the 2.5, 2.4, and 2.3 stable branches.

These are the first versions to be able to compile against [Proj 6.0.0](https://proj4.org/download.html),
You must upgrade to these if you are using Proj 6.

<!--more-->
**2.5.2**
This release supports PostgreSQL 9.3-11
(will compile against PostgreSQL 12, but not pass tests. Use only for pg_upgrade.
You are encouraged to use the PostGIS 3.0 unreleased branch with PostgreSQL 12
, which has features specifically designed to take advantage of features new in PostgreSQL 12).

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.2.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.2/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.5.2.pdf) [de]({{< loc "postgis.release_docs">}}/postgis-2.5.2-de.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.5.2.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-2.5) [de]({{< loc "site.root">}}docs/manual-2.5/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-2.5/postgis-ja.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.5.2.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.5.2/ChangeLog)

**2.4.7**
This release supports PostgreSQL 9.3-10.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.4.7.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.4.7/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.4.7.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.4.7.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-2.4)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.7.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.4.7/ChangeLog)

**2.3.9**

This release supports PostgreSQL 9.2-10.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.3.9.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.3.9/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.3.9.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.3.9.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-2.3)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.3.9.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.3.9/ChangeLog)


View all [closed tickets for 2.5.2, 2.4.7, 2.3.9][1].

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+2.3.9&milestone=PostGIS+2.4.7&milestone=PostGIS+2.5.2&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
