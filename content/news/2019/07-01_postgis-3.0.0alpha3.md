---
title: PostGIS 3.0.0alpha3
layout: post
category: news
tags: [release,3.0]
author: Regina Obe
date: "2019-07-01"
thumbnail:
---

The PostGIS development team is pleased to release PostGIS 3.0.0alpha3.

This release works with PostgreSQL 9.5-12beta2  and GEOS >= 3.6

Best served with [PostgreSQL 12beta2](https://www.postgresql.org/developer/beta/).

<!--more-->

**3.0.0alpha3**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.0alpha3.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.0alpha3/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.0alpha3.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.0alpha3.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.0alpha3.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/3.0.0alpha3/ChangeLog)

View all [closed tickets for 3.0.0][1].

Main changes since PostGIS 3.0.0alpha2 release are as follows:

* 4438, Update serialization to support extended flags area (Paul Ramsey)
* 3437, Speed up ST_Intersects with Points (Raúl Marín)
* 4352, Use CREATE OR REPLACE AGGREGATE for PG12+ (Raúl Marín)
* 4441, Make GiST penalty friendly to multi-column indexes and build single-column
         ones faster. (Darafei Praliaskouski)
* 4372, PROJ6: Speed improvements (Raúl Marín)
* 4414, Include version number in address_standardizer lib (Raúl Marín)
* 4334, Fix upgrade issues related to renamed parameters (Raúl Marín)
* 4388, AddRasterConstraints: Ignore NULLs when generating constraints (Raúl Marín)
* 4327, Avoid pfree'ing the result of getenv (Raúl Marín)
* 4406, Throw on invalid characters when decoding geohash (Raúl Marín)
* 4429, Avoid resource leaks with PROJ6 (Raúl Marín)
* 4443, Fix wagyu configure dropping CPPFLAGS (Raúl Marín)
* 4440, Type lookups in FDW fail (Paul Ramsey)
* 4442, raster2pgsql now skips NODATA tiles. Use -k option if you still want
         them in database for some reason. (Darafei Praliaskouski)

Note that a major change in 3.0 is that the raster functionality has been broken out as a separate extension.
If you are upgrading from a previous edition, do the following:

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
-- this next step repackages raster in its own extension
-- and upgrades all your other related postgis extension
SELECT PostGIS_Extensions_Upgrade();

--if you don't use raster, you can do
DROP EXTENSION postgis_raster;
```

-- New users
```postgres
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_raster;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION postgis_tiger_geocoder;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.0.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
