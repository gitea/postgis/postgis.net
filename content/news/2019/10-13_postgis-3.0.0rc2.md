---
title: PostGIS 3.0.0rc2
layout: post
category: news
tags: [release,3.0]
author: Regina Obe
date: "2019-10-13"
thumbnail:
---

The PostGIS development team is pleased to release PostGIS 3.0.0rc2.
This will be the final RC before release.

This release works with PostgreSQL 9.5-12  and GEOS >= 3.6

Best served with [PostgreSQL 12](https://www.postgresql.org/about/news/1976/)
, [GEOS 3.8.0](https://lists.osgeo.org/pipermail/geos-devel/2019-October/009342.html)
and [pgRouting 3.0.0-alpha](https://github.com/pgRouting/pgrouting/releases/tag/v3.0.0-alpha).


<!--more-->

**3.0.0rc2**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.0rc2.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.0rc2/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.0rc2.pdf) [de]({{< loc "postgis.release_docs">}}/postgis-3.0.0rc2-de.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.0rc2.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.0), [de]({{< loc "site.root">}}docs/manual-3.0/postgis-de.html), [ko_KR]({{< loc "site.root">}}docs/manual-3.0/postgis-ko_KR.html), [ja]({{< loc "site.root">}}docs/manual-3.0/postgis-ja.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.0rc2.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/3.0.0rc2/ChangeLog)

View all [closed tickets for 3.0.0][1].

Main changes since PostGIS 3.0.0rc1 release are as follows:

* 4534, Fix leak in lwcurvepoly_from_wkb_state (Raúl Marín)
* 4536, Fix leak in lwcollection_from_wkb_state (Raúl Marín)
* 4537, Fix leak in WKT collection parser (Raúl Marín)
* 4535, WKB: Avoid buffer overflow (Raúl Marín)

Note that a major change in 3.0 is that the raster functionality has been broken out as a separate extension.
If you are upgrading from a previous edition, do the following:

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
-* this next step repackages raster in its own extension
-* and upgrades all your other related postgis extension
SELECT PostGIS_Extensions_Upgrade();

--if you don't use raster, you can do
DROP EXTENSION postgis_raster;
```

-* New users
```postgres
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_raster;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION postgis_tiger_geocoder;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.0.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
