---
title: PostGIS 3.0.0
layout: post
category: news
tags: [release,3.0]
author: Regina Obe
date: "2019-10-20"
thumbnail:
---

The PostGIS development team is pleased to release PostGIS 3.0.0.

This release works with PostgreSQL 9.5-12  and GEOS >= 3.6.

If you are using postgis_sfcgal extension, you need to compile against SFCGAL 1.3.1 or higher.

Best served with [PostgreSQL 12](https://www.postgresql.org/about/news/1976/)
, [GEOS 3.8.0](https://lists.osgeo.org/pipermail/geos-devel/2019-October/009342.html)
and [pgRouting 3.0.0-beta](https://github.com/pgRouting/pgrouting/releases/tag/v3.0.0-beta).

<!--more-->

**3.0.0**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.0.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.0/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.0.pdf) [de]({{< loc "postgis.release_docs">}}/postgis-3.0.0-de.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.0.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-3.0), [de]({{< loc "site.root">}}docs/manual-3.0/postgis-de.html), [ko_KR]({{< loc "site.root">}}docs/manual-3.0/postgis-ko_KR.html), [ja]({{< loc "site.root">}}docs/manual-3.0/postgis-ja.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.0.epub)
* [Changelog](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.0/ChangeLog)

View all [closed tickets for 3.0.0][1].

Note that a major change in 3.0 is that the raster functionality has been broken out as a separate extension.
If you are upgrading from a previous edition, do the following:

-- Upgrading

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
-- this next step repackages raster in its own extension
-- and upgrades all your other related postgis extensions
SELECT PostGIS_Extensions_Upgrade();

-- if you don't use raster, you can do below
-- after the upgrade step
DROP EXTENSION postgis_raster;
```

-- Fresh install
```postgres
-- installs geometry and geography support
CREATE EXTENSION postgis;
-- install these if you need them
CREATE EXTENSION postgis_raster;
CREATE EXTENSION postgis_topology;
-- 3d and advanced processing
CREATE EXTENSION postgis_sfcgal;
-- street address normalization
CREATE EXTENSION address_standardizer;
-- geocoder/reverse_geocoder for US census data
CREATE EXTENSION postgis_tiger_geocoder CASCADE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.

**Key Breaking Changes**

* 3888, Raster support now in a separate extension - postgis_raster
         (Sandro Santilli)
* 3807, Extension library files no longer include the minor version.
         If you need the old behavior,
         use the new configure switch `--with-library-minor-version`.
         This change is intended to smooth future pg_upgrade since lib file names
         won't change between 3.0, 3.1, 3.* releases (Regina Obe)
* 4230, ND box operators (overlaps, contains, within, equals) now don't look on
         dimensions that aren't present in both operands.
         Please REINDEX your ND indexes after upgrade. (Darafei Praliaskouski)

* 4433, 32-bit hash fix (requires reindexing hash(geometry) indexes) (Raúl Marín)
* 3383, Sorting now uses Hilbert curve and Postgres Abbreviated Compare.
        You need to REINDEX your btree indexes if you had them.
        (Darafei Praliaskouski)

**New Features**

* 2902, postgis_geos_noop (Sandro Santilli)
* 4128, ST_AsMVT support for Feature ID (Stepan Kuzmin)
* 4230, SP-GiST and GiST support for ND box operators overlaps, contains,
         within, equals (Esteban Zimányi and Arthur Lesuisse from Université
         Libre de Bruxelles (ULB), Darafei Praliaskouski)
* 4171, ST_3DLineInterpolatePoint (Julien Cabieces, Vincent Mora)
* 4311, Introduce WAGYU to validate MVT polygons. This option requires a C++11
         compiler and will use CXXFLAGS (not CFLAGS). Add `--without-wagyu`
         to disable this option and keep the behaviour from 2.5 (Raúl Marín)
* 1833, ST_AsGeoJSON(row) generates full GeoJSON Features (Joe Conway)
* 3687, Casts json(geometry) and jsonb(geometry) for implicit GeoJSON
         generation (Paul Ramsey)
* 4198, Add ST_ConstrainedDelaunayTriangles SFCGAL function (Darafei
         Praliaskouski)

**Enhancements**

* 4341, Using "support function" API in PgSQL 12+ to replace SQL inlining
as the mechanism for providing index support under ST_Intersects, et al
* 4330, postgis_restore OOM when output piped to an intermediate process
(Hugh Ranalli)
* 4322, Support for Proj 6+ API, bringing more accurate datum transforms
and support for WKT projections
* 4153, ST_Segmentize now splits segments proportionally (Darafei
Praliaskouski).
* 4162, ST_DWithin documentation examples for storing geometry and
radius in table (Darafei Praliaskouski, github user Boscop).
* 4161 and #4294, ST_AsMVTGeom: Shortcut geometries smaller than the
resolution (Raúl Marín)
* 4176, ST_Intersects supports GEOMETRYCOLLECTION (Darafei Praliaskouski)
* 4181, ST_AsMVTGeom: Avoid type changes due to validation (Raúl Marín)
* 4183, ST_AsMVTGeom: Drop invalid geometries after simplification (Raúl Marín)
* 4196, Have postgis_extensions_upgrade() package unpackaged extensions
(Sandro Santilli)
* 4215, Use floating point compare in ST_DumpAsPolygons (Darafei Praliaskouski)
* 4155, Support for GEOMETRYCOLLECTION, POLYGON, TIN, TRIANGLE in
ST_LocateBetween and ST_LocateBetweenElevations (Darafei Praliaskouski)
* 2767, Documentation for AddRasterConstraint optional parameters (Sunveer Singh)
* 4244, Avoid unaligned memory access in BOX2D_out (Raúl Marín)
* 4139, Make mixed-dimension ND index build tree correctly (Darafei Praliaskouski,
Arthur Lesuisse, Andrew Gierth, Raúl Marín)
* 4262, Document MULTISURFACE compatibility of ST_LineToCurve (Steven Ottens)
* 4276, ST_AsGeoJSON documentation refresh (Darafei Praliaskouski)
* 4292, ST_AsMVT: parse JSON numeric values with decimals as doubles (Raúl Marín)
* 4300, ST_AsMVTGeom: Always return the simplest geometry (Raúl Marín)
* 4301, ST_Subdivide: fix endless loop on coordinates near coincident to bounds
(Darafei Praliaskouski)
* 4289, ST_AsMVTGeom: Transform coordinates space before clipping (Raúl Marín)
* 4272, Improve notice message when unable to compute stats (Raúl Marín)
* 4313, #4307, PostgreSQL 12 compatibility (Laurenz Albe, Raúl Marín)
* 4299, #4304, ST_GeneratePoints is now VOLATILE. IMMUTABLE version with
seed parameter added. (Mike Taves)
* 4278, ST_3DDistance and ST_3DIntersects now support Solid TIN and Solid
POLYHEDRALSURFACE (Darafei Praliaskouski)
* 4348, ST_AsMVTGeom (GEOS): Enforce validation at all times (Raúl Marín)
* 4295, Allow GEOMETRYCOLLECTION in ST_Overlaps, ST_Contains, ST_ContainsProperly,
ST_Covers, ST_CoveredBy, ST_Crosses, ST_Touches, ST_Disjoint, ST_Relate,
ST_Equals (Esteban Zimányi)
* 4340, ST_Union aggregate now can handle more than 1 GB of geometries
(Darafei Praliaskouski)
* 4378, Allow passing TINs as input to GEOS-backed functions (Darafei
Praliaskouski)
* 4368, Reorder LWGEOM struct members to minimize extra padding (Raúl Marín)
* 4141, Use uint64 to handle row counts in the topology extension (Raúl Marín)
* 4225, Upgrade tiger to use tiger 2018 census files
* 4412, Support ingesting rasters with NODATA=NaN (Darafei Praliaskouski)
* 4413, Raster tile size follows GeoTIFF block size on raster2pgsql -t auto
(Darafei Praliaskouski)
* 4422, Modernize Python 2 code to get ready for Python 3 (Christian Clauss)
* 4352, Use CREATE OR REPLACE AGGREGATE for PG12+ (Raúl Marín)
* 4394, Allow FULL OUTER JOIN on geometry equality operator (Darafei Praliaskouski)
* 4441, Make GiST penalty friendly to multi-column indexes and build single-column
ones faster. (Darafei Praliaskouski)
* 4403, Support for shp2pgsql ability to reproject with copy mode (-D) (Regina Obe)
* 4410, More descriptive error messages about SRID mismatch (Darafei Praliaskouski)
* 4399, TIN and Triangle output support in all output functions (Darafei
Praliaskouski)
* 3719, Impose minimum number of segments per arc during linearization
(Dan Baston / City of Helsinki, Raúl Marín)
* 4277, ST_GeomFromGeoJSON now marks SRID=4326 by default as per RFC7946,
ST_AsGeoJSON sets SRID in JSON output if it differs from 4326.
(Darafei Praliaskouski)
* 3979, postgis_sfcgal_noop() round trip function (Lucas C. Villa Real)
* 4328, ST_3DIntersects for 2D TINs. (Darafei Praliaskouski)
* 4509, Update geocoder for tiger 2019 (Regina Obe)


**Minor Breaking Changes**

* 4267, Bump minimum GEOS version to 3.6 (Regina Obe, Darafei Praliaskouski)
* 4229, Dropped support for PostgreSQL < 9.5. (Darafei Praliaskouski)
* 4260, liblwgeom headers are not installed anymore.
         If your project depends on them available, please use
         librttopo instead. (Darafei Praliaskouski)
* 4258, Remove SFCGAL support for `ST_Area`, `ST_Distance`, `ST_Intersection`,
         `ST_Difference`, `ST_Union`, `ST_Intersects`, `ST_3DIntersects`, `ST_3DDistance`
         and `postgis.backend` switch (Darafei Praliaskouski)
* 4267, Enable Proj 6 deprecated APIs (Darafei Praliaskouski, Raúl Marín)
* 4268, Bump minimum SFCGAL version to 1.3.1 (Darafei Praliaskouski)
* 4331, ST_3DMakeBox now returns error instead of a miniscule box (Regina Obe)
* 4342, Removed "versioned" variants of `ST_AsGeoJSON` and `ST_AsKML` (Paul Ramsey)
* 4356, ST_Accum removed. Use array_agg instead. (Darafei Praliaskouski)
* 4414, Include version number in address_standardizer lib (Raúl Marín)
* 4334, Fix upgrade issues related to renamed function parameters (Raúl Marín)
* 4442, raster2pgsql now skips NODATA tiles. Use -k option if you still want
         them in database for some reason. (Darafei Praliaskouski)



[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.0.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
