---
title: PostGIS 3.0.0rc1
layout: post
category: news
tags: [release,3.0]
author: Regina Obe
date: "2019-10-08"
thumbnail:
---

The PostGIS development team is pleased to release PostGIS 3.0.0rc1.

This release works with PostgreSQL 9.5-12  and GEOS >= 3.6

Best served with [PostgreSQL 12](https://www.postgresql.org/about/news/1976/)
, [GEOS 3.8.0rc2](https://lists.osgeo.org/pipermail/geos-devel/2019-October/009288.html)
and [pgRouting 3.0.0-alpha](https://github.com/pgRouting/pgrouting/releases/tag/v3.0.0-alpha).

<!--more-->

**3.0.0rc1**

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.0rc1.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.0rc1/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.0rc1.pdf) [de]({{< loc "postgis.release_docs">}}/postgis-3.0.0rc1-de.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.0rc1.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-dev), [de]({{< loc "site.root">}}docs/manual-dev/postgis-de.html), [ko_KR]({{< loc "site.root">}}docs/manual-dev/postgis-ko_KR.html), [ja]({{< loc "site.root">}}docs/manual-dev/postgis-ja.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.0rc1.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/3.0.0rc1/ChangeLog)

View all [closed tickets for 3.0.0][1].

Main changes since PostGIS 3.0.0beta1 release are as follows:

* #4519, Fix getSRIDbySRS crash (Raúl Marín)
* #4520, Use a clean environment when detecting C++ libraries (Raúl Marín)
* Restore `ST_Union` aggregate signature so drop agg not required and re-work
  performance/size enhancement to continue to avoid
  using Array type during `ST_Union`, hopefully
  avoiding Array size limitations. (Paul Ramsey)

Note that a major change in 3.0 is that the raster functionality has been broken out as a separate extension.
If you are upgrading from a previous edition, do the following:

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
-* this next step repackages raster in its own extension
-* and upgrades all your other related postgis extension
SELECT PostGIS_Extensions_Upgrade();

--if you don't use raster, you can do
DROP EXTENSION postgis_raster;
```

-* New users
```postgres
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_raster;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION postgis_tiger_geocoder;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+3.0.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
