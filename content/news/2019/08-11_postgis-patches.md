---
title: PostGIS 3.0.0alpha4, 2.5.3, 2.4.8, 2.3.10 Released
layout: post
category: news
tags: [release,3.0, 2.5,2.4,2.3]
author: Regina Obe
date: "2019-08-11"
thumbnail:
---

The PostGIS development team is pleased to provide
enhancements/features 3.0.0alpha4 for 3.0 feature major branch
bug fix 2.5.3, 2.4.8, and 2.3.10 for the 2.5, 2.4, and 2.3 stable branches.

<!--more-->
**3.0.0alpha4**
This release works with PostgreSQL 9.5-12beta3  and GEOS >= 3.6

Best served with [PostgreSQL 12beta3](https://www.postgresql.org/developer/beta/).
Designed to take advantage of features in PostgreSQL 12 and Proj 6

* [source download]({{< loc "postgis.release_source">}}/postgis-3.0.0alpha4.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/3.0.0alpha4/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-3.0.0alpha4.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-3.0.0alpha4.tar.gz)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-3.0.0alpha4.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/3.0.0alpha4/ChangeLog)

**2.5.3**
This release supports PostgreSQL 9.3-12
You are encouraged to use the PostGIS 3.0 unreleased branch with PostgreSQL 12
, which has features specifically designed to take advantage of features new in PostgreSQL 12.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.5.3.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.5.3/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.5.3.pdf) [de]({{< loc "postgis.release_docs">}}/postgis-2.5.3-de.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.5.3.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-2.5) [de]({{< loc "site.root">}}docs/manual-2.5/postgis-de.html) [ja]({{< loc "site.root">}}docs/manual-2.5/postgis-ja.html)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.5.3.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.5.3/ChangeLog)

**2.4.8**
This release supports PostgreSQL 9.3-10.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.4.8.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.4.8/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.4.8.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.4.8.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-2.4)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.4.8.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.4.8/ChangeLog)

**2.3.10**

This release supports PostgreSQL 9.2-10.

* [source download]({{< loc "postgis.release_source">}}/postgis-2.3.10.tar.gz)
* [NEWS](https://git.osgeo.org/gitea/postgis/postgis/raw/tag/2.3.10/NEWS)
* PDF docs [en]({{< loc "postgis.release_docs">}}/postgis-2.3.10.pdf)
* [html doc download]({{< loc "postgis.release_docs">}}/doc-html-2.3.10.tar.gz)
* html online [en]({{< loc "site.root">}}docs/manual-2.3)
* [epub doc download]({{< loc "postgis.release_docs">}}/postgis-2.3.10.epub)
* [Changelog](https://svn.osgeo.org/postgis/tags/2.3.10/ChangeLog)


View all [closed tickets for 3.0.0, 2.5.3, 2.4.8, 2.3.10][1].

After installing the binaries or after running pg_upgrade, make sure to do:

```postgres
ALTER EXTENSION postgis UPDATE;
```

-- if you use the other extensions packaged with postgis
-- make sure to upgrade those as well

```postgres
ALTER EXTENSION postgis_sfcgal UPDATE;
ALTER EXTENSION postgis_topology UPDATE;
ALTER EXTENSION postgis_tiger_geocoder UPDATE;
```

If you use legacy.sql or legacy_minimal.sql,
make sure to rerun the version packaged with these releases.


[1]: https://trac.osgeo.org/postgis/query?status=closed&resolution=fixed&milestone=PostGIS+2.3.10&milestone=PostGIS+2.4.8&milestone=PostGIS+2.5.3&milestone=PostGIS+3.0.0&col=id&col=summary&col=milestone&col=status&col=type&col=priority&col=component&order=priority
