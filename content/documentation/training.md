---
title: Training Materials
date: 2022-02-01
description: "In depth learning tools like workshops and books."
weight: 100
geekdocHidden: false
---

## Workshop

The "**Introduction to PostGIS**" workshop is a hands-on step-by-step walk through PostGIS basics. Loading data, creating tables, writing queries, and more.

* **[English](/workshops/postgis-intro/)**
* [Japanese](/workshops/ja/postgis-intro/)
* [Chinese (Simplified)](/workshops/zh_Hans/postgis-intro/)
* [Spanish](/workshops/es/postgis-intro/)

[Welcome to PostGIS workbook](https://postgis.gishub.org), accompanying  [videos](https://www.youtube.com/watch?v=LhKj-_-CCfY&list=PLAxJ4-o7ZoPcvp0ETujkLVCmKIGj-YvlG) (Quisheng Wu) - spatial data management with PostgreSQL, PostGIS, and Jupyter Python Notebook.
This workbook also uses some data from [Introduction to PostGIS](/workshops/postgis-intro/) workshop.

[PostGIS Baby Steps](https://www.youtube.com/watch?v=ruTP-4yRl1A&list=PLj5uPTt8jS8zzXfWvtqaT8pYhKWnPRFox), [Grace Amondi](https://grace-amondi.github.io/) - training series covering basics of PostGIS, loading data, using pgAdmin4, and querying with QGIS.
This training series uses some data from [Introduction to PostGIS](/workshops/postgis-intro/) workshop.

## Books

* [Spatial SQL](https://locatepress.com/book/spatial-sql)
	* Matthew Forrest
	* English (529 pages)
	* PostGIS, PostgreSQL, h3, pgRouting, pySAL
	* Released November 2023

* [PostGIS - Tous les ingrédients pour concocter un SIG sur de bonnes bases](https://book.postgis.app)
	* Arthur Bazin
	* French (550 pages)
	* PostGIS 3.1-3.3 and PostgreSQL 13-15
	* Released March 2023

* [Mastering PostGIS and OpenStreetMap](https://blog.rustprooflabs.com/2022/10/announce-mastering-postgis-openstreemap)
	* Ryan Lambert
	* English, 4 chapters on pgRouting
	* Released Oct 1st, 2022

* [PostGIS In Action](https://postgis.us), 3rd Edition
	* Regina Obe & Leo Hsu
	* English (664 pages)
	* PostGIS 2.5-3.2, PostgreSQL 11-13
	* Released August 2021

* [PostGIS: Análisis Espacial Avanzado](https://cartosig.webs.upv.es/postgis-analisis-espacial-avanzado-edicion-2-revision-4-agosto-2020/), 2nd Revision
	* José Carlos Martínez Llario
	* Spanish
	* PostGIS 3, PostgreSQL 13
	* Released Sept 2020

* [PostGIS Cookbook](https://www.packtpub.com/application-development/postgis-cookbook-second-edition), 2nd Edition
	* Paolo Corti, Stephen Vincent Mather & Thomas Kraft
	* English
	* PostgreSQL 9.6
	* Released March 2018


## Web Sites

* [GIS Stack Exchange](http://gis.stackexchange.com/questions/tagged/postgis) may already have your answer: search before you ask!

* [Crunchy Data Interactive PostGIS Learning Portal](https://learn.crunchydata.com/postgis)
