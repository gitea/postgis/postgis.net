---
title: Official Manual
date: 2022-02-01
description: "Most queries can be answered in the reference material of the official manual."
weight: 60
geekdocHidden: false
---

This documentation is for the released branches and will contain corrections and errata as they are available.

{{< doclinks "stable">}}


## Development

This documentation is the unreleased version currently under development, and is frequently updated and changed.

Work is in progress for other languages: Italian, French, Portuguese, Japanese, German, Korean, Spanish, Polish and more coming. To help translate the manual into your language of choice, please [sign up](https://weblate.osgeo.org/projects/postgis)!

{{< doclinks "dev">}}

