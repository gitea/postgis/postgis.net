---
title: Enabling PostGIS raster drivers and out-of-db raster support
slug: "tip_enable_raster_drivers"
layout: post
category: tips
tags: [newbie, raster]
author: Regina Obe
date: "2015-05-02"
thumbnail:
geekdocHidden: true
geekdocHiddenTocTree: false
weight: 50
---

I've been asked this question in some shape or form at least 3 times, mostly from people puzzled why they get this error.
The last iteration went something like this:

<em>I can't use `ST_AsPNG` when doing something like</em>

```postgres
SELECT ST_AsPNG(rast)
    FROM  sometable;
```

Gives error: Warning: pg_query(): Query failed: ERROR: rt\_raster\_to\_gdal: Could not load the output GDAL driver.

<!--more-->

So why does this happen?  This has been an issue since our release of 2.0.6/2.13 PostGIS security patches that by default disabled all raster drivers and out-of-db raster storage. Raster is much less useful if you don't have any raster drivers enabled, so you need to renable them or the selective ones you need.

**Determine which drivers you have enabled**
The first step is to figure out which ones you do have enabled if any.  You can determine that by using the  <a href="/docs/manual-dev/RT_ST_GDALDrivers.html" target="_blank">```ST_GDALDrivers```</a>  function:

```postgres
SELECT short_name
 FROM ST_GDALDrivers();
```


**Enabling drivers pre-PostGIS 2.2**
If you get no records back, then you have the default configuration of all drivers being disabled.  You need to set some environment variables either in your postgres startup script or system environment.  The process varies a lot from platform to platform and how you installed PostgreSQL. This is detailed a bit in the docs.

In order to enable all or the ones you need, refer to: <a href="/docs/postgis_installation.html#install_short_version" target="_blank">Installation Short version</a>.
For windows users, who are using the installer, the installer at the end has prompts asking you if you want to enable drivers and enables the most common ones.

**Coming in PostGIS 2.2 Raster GUCS**
In the not yet released version of PostGIS 2.2 which we hope to ship come August or September 2015, some PostgreSQL PostGIS-specific GUCs have been introduced which allow you to do this in a
cross-platform way using PostgreSQL GUC machinery.  If you have PostgreSQL 9.4, you can use the new ALTER SYSTEM construct to set this system whide instead of in a specific database or session.

The new GUCS are detailed in: <a href="/docs/manual-dev/postgis_gdal_enabled_drivers.html" target="_blank">`postgis.gdal_enabled_drivers`</a>  and <a href="/docs/manual-dev/postgis_enable_outdb_rasters.html" target="_blank">`postgis.enable_outdb_rasters`</a>
