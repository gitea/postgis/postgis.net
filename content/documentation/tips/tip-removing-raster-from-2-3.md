---
title: Fixing a broken postgis raster install
slug: tip-removing-raster-from-2-3
layout: post
category: tips
tags: [newbie, upgrade]
author: Regina Obe
thumbnail:
date: "2023-02-03"
geekdocHidden: true
geekdocHiddenTocTree: false
weight: 30
---

As of PostGIS 3.0, the PostGIS raster support is no longer part of the
`postgis` extension, but instead spun off into a PostGIS extension called `postgis_raster`.

[Upgrading raster from 2.* to 3.*](/2022/01/14/tip-upgrading-raster-from-2-3) covers the proper
way of upgrading your install from PostGIS 2+ to PostGIS 3+.

Unfortunately many people in a heat of panic when seeing the notice "UNPACKAGED"
in their check of

```SELECT postgis_full_extension()```

tried to do something like drop raster type with cascade. Dropping the raster type *DOES NOT* remove
all the raster functions, but *DOES* destroy all your raster data if you had any. Do not do that.
But if you did happen to do that, this solution is for you.

The only solution after breaking your raster install is to remove the remaining bits of the postgis raster functionality.
At that point, you can reinstall using `CREATE EXENSION postgis_raster`, or not bother if you don't need
raster support.

<!--more-->

How do you know your raster install is broken?

If you run:

```postgres
ALTER EXTENSION postgis UPDATE;

-- yes run it twice
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```
and then check

```postgres
SELECT postgis_full_version();
```

and you still see the message
```RASTER (raster lib from "2.5.3 r17699" need upgrade) [UNPACKAGED!]```

You've broken your raster functionality so badly, it can't be upgraded.

First before doing anything, confirm you have no raster tables.
The below query should either error out or return **0**:

```postgres
SELECT count(1) FROM raster_columns;
```

To fix this issue, run the script [*uninstall_rtpostgis.sql*](/stuff/uninstall_rtpostgis.sql).
It should remove even older versions of postgis raster functions, since the function sets of older versions are generally a subset of the 2.5.

Now to confirm it is all gone, run the below again.

```postgres
SELECT postgis_full_version();
```

You should now see no mention of *RASTER* or *GDAL*.

