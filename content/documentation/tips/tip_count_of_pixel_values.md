---
title: "Getting distinct pixel values and pixel value counts of a raster"
slug: "tip_count_of_pixel_values"
layout: post
category: tips
tags: ["newbie","raster","ST_ValueCount","ST_Value"]
author: Regina Obe
thumbnail:
date: "2014-09-26"
geekdocHidden: true
geekdocHiddenTocTree: false
weight: 50
---

PostGIS raster has so so many functions and probably at least 10 ways of doing something some much much slower than others. Suppose
you have a raster, or you have a raster area of interest -- say elevation raster for example, and you want to know the distinct pixel values in the area.
The temptation is to reach for <a href="/docs/manual-dev/RT_ST_Value.html" target="_blank">```ST_Value```</a> function in raster, but there is a much much more efficient function to use, and that is the  <a href="/docs/manual-dev/RT_ST_ValueCount.html" target="_blank">```ST_ValueCount```</a> function.

```ST_ValueCount``` function is one of many statistical raster functions available with PostGIS 2.0+.  It is a set returning function that returns 2 values for each row: a pixel value (value), and a count of pixels (count) in the raster that have that value.  It also has variants that allow you to filter for certain pixel values.

This tip was prompted by the question on stackexchange <a href="http://gis.stackexchange.com/questions/115161/how-can-i-extract-all-distinct-values-from-a-postgis-raster/115245" target="_blank">How can I extract all distinct values from a PostGIS Raster?</a>

<!--more-->

**Examples**

*Get a distinct pixel value list for band 1 of all raster tiles*

Specifying the band number is optional and defaults to 1 if not specified.
I like to specify it just cause I work a lot with multi-band rasters. In this case I just have digital elevation data table called dem.

```postgres
SELECT DISTINCT (pvc).value
 FROM (SELECT ST_ValueCount(dem.rast,1) As pvc
   FROM dem) As f
 ORDER BY (pvc).value;
```

Now if you are using PostgreSQL 9.3+, you can make this even shorter by using the LATERAL clause
(since LATERAL keyword is optional in most cases, you can skip saying LATERAL and write it like this)

```postgres
SELECT DISTINCT (pvc).value
 FROM dem, ST_ValueCount(dem.rast,1) As pvc
 ORDER BY (pvc).value;
```



*Get a pixel value list for band 1 and total pixels for an area of interest*

Now if you have a huge coverage, chances are you only care about a particular area, not the 3 million tiles you have.
You might want to also know
how many times the value appears just for your area of interest
So you really want to combine your arsenal with <a href="/docs/manual-dev/RT_ST_Clip.html" target="_blank">```ST_Clip```</a> and <a href="/docs/manual-dev/RT_ST_Intersects.html" target="_blank">```ST_Intersects```</a>.
Here I am using the 9.3 LATERAL short-hand

```postgres
SELECT  (pvc).value, SUM((pvc).count) As tot_pix
 FROM dem
  INNER JOIN
ST_GeomFromText('LINESTRING(-87.627 41.8819,
 -87.629 41.8830)'
 , 4326) As geom
  ON ST_Intersects(dem.rast, geom),
    ST_ValueCount(ST_Clip(dem.rast,geom),1) As pvc
  GROUP BY (pvc).value
 ORDER BY (pvc).value;
```
