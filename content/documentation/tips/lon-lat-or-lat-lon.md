---
title: "Is it Lon/Lat or Lat/Lon?"
layout: post
category: tips
tags: [newbie, geometry, geography, ST_FlipCoordinates]
author: "Regina Obe"
date: "2013-08-18"
weight: 20
geekdocHidden: true
geekdocHiddenTocTree: false
---

In web mapping APIs like Google Maps, spatial coordinates are often in order of latitude then longitude.

In spatial databases like PostGIS and SQL Server, spatial coordinates are in longitude and then latitude. Mixing the two up can lead to questions like "why is this European city showing up in Africa on my map?"

If you accidentally loaded data in latitude/longitude order, you can easily fix the mistake
by using the [ST_FlipCoordinates](/docs/ST_FlipCoordinates.html) function.

An example use for geometry:

```postgres
ALTER TABLE sometable
  ALTER COLUMN geom
    TYPE geometry(LineString,4326)
    USING ST_FlipCoordinates(geom);
```

For geography:

```postgres
ALTER TABLE sometable
  ALTER COLUMN geog
    TYPE geography(LineString,4326)
    USING geography(ST_FlipCoordinates(geometry(geom)));
```
