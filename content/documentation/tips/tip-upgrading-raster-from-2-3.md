---
title: Upgrading raster from 2.* to 3.*
slug: tip-upgrading-raster-from-2-3
layout: post
category: tips
tags: [newbie, upgrade]
author: Regina Obe
thumbnail:
date: "2022-01-14"
geekdocHidden: true
geekdocHiddenTocTree: false
weight: 30
---

As of PostGIS 3.0, the PostGIS raster support is no longer part of the
`postgis` extension, but instead spun off into a new PostGIS extension called `postgis_raster`.

The two main reasons for this break were:

* Raster functionality in PostGIS is fat with over 150 functions and several types. Wading through these extra functions frustrated many who had no use for rasters.

* Raster gdal dependency is very big and many dreamed of having postgis extension without the big raster dependency.

While repackaging raster as it's own extension resolved many complaints, it meant a slightly more complicated upgrade process going from PostGIS 2.something to 3.something that even experienced PostGIS users managed to screw up.

I will detail the proper way of upgrading PostGIS raster, from a PostGIS 2.* install to 3.* install.

You can run these steps using psql or pgAdmin or any other PostgreSQL tool you want.

Regardless which version of PostGIS you are coming from, you should install the PostGIS 3.* binaries first.

<!--more-->

```postgres
-- this step only needed for PostGIS < 2.5.4
ALTER EXTENSION postgis UPDATE;

-- Do for all PostGIS 2.*
SELECT postgis_extensions_upgrade();
SELECT postgis_extensions_upgrade();
```


If you have no need for raster support and have no tables currently
with rasters, you can then drop the extension as follows:
```postgres
DROP EXTENSION postgis_raster;
```

So you may be wondering, the oddity of running twice:

```postgres
SELECT postgis_extensions_upgrade();
```

This is because, the first run, unbundles the raster support from the `postgis` extension making it a loose baggage of functons and types.

The second call rebundles the loose bits into a new extension `postgis_raster`. Unfortunately we were unable to achieve this feat with a single function call.
I forget the reasons.  Something along the lines of an extension upgrade/install can't birth a new extension within the same transaction.

In PostgreSQL 13, PostgreSQL removed support for `CREATE EXTENSION ... FROM unpackaged`,  which added some complications. As such, you should try upgrading to a PostGIS 3.* before you upgrade to PostgreSQL 13 or above and you should definitely switch to using PostGIS extension instead of the install scripts before attempting a PostgreSQL 13+ upgrade.
