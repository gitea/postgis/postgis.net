---
title: "Are geography calculations done on the spheroid?"
date: 2022-02-01
draft: false
geekdocHidden: true
geekdocHiddenTocTree:
tags: [geography]
---

To a first approximation, **yes**. If you calculate the distance between two geography points, that calculation will be based on the spheroid defined by the SRID attached to the points, which if you do not specify it will be [WGS84](https://en.wikipedia.org/wiki/World_Geodetic_System#WGS_84).

However, in general PostGIS uses a combination of spherical and spheroidal calculations for geography.

For example, if you are calculating the distance between more complex objects, like the distance between two polygons, the algorithm has a couple steps:

* First it has to cycle through the edges of the polygons, to find out which edges are the nearest edges.
* Then it takes the two points on those edges that are closest and calculates the distance between them.

For the first step, finding the closest edges, PostGIS uses distances on the **sphere**. If we used calculations on the spheroid, we would be doing a large number of very expensive calculations to ultimately just discard edges, edges that a simple spherical calculation would also discard.

For the second step, having identified the closest points (which are either two vertices, or a vertex and a projected point on one of the edges) PostGIS calculates the distance between them using a spheroidal distance.

In general, calculating distances between complex shapes using only spheroidal calculations only changes the results in a very small number of cases, and only by very small amounts.

