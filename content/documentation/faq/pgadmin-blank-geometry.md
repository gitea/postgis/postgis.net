---
title: "When I view a geometry column in PgAdmin it is blank, why?"
date: 2022-02-01
draft: false
geekdocHidden: true
geekdocHiddenTocTree: false
tags: [geometry,geography, pgadmin]
---

Compared to most data types, geometries are quite large. For some versions of PgAdmin, there is a maximum amount of data that can be show in a cell, and if the record is larger than that, the cell simply appears blank.

* You can use [ST_AsText()](/docs/ST_AsText.html) to get a human readable form of the geometry, but that might still be too large.
* You can use a simplifying function, like [ST_Centroid()](/docs/ST_Centroid.html) to get a geometry that is small enough to visualize.
* You can use a summary function, like [ST_NPoints()](/docs/ST_NPoints.html) to replace the geometry with a count of the number of vertices in the geometry.
* In more recent versions of PgAdmin, you can click the "eye" icon in the cell to get a map visualization of the geometry.

Finally, it's possible that your column does doesn't have any geometry in it for some reason!

```sql
SELECT Count(*) FROM mytable WHERE geom IS NOT NULL
```

