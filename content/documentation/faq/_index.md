---
title: FAQ
date: 2022-02-01
description: "Some questions that come up a lot."
weight: 120
geekdocHidden: false
---

Some answers to frequently asked questions (FAQ) about PostGIS.

{{< toc-tree >}}
