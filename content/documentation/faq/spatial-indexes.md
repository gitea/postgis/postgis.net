---
title: "How do I use spatial indexes?"
date: 2022-02-01
draft: false
geekdocHidden: true
geekdocHiddenTocTree: false
tags: [geometry,geography, performance]
---

Spatial indexes are hugely important for getting good performance on spatial SQL queries! You need to do two things to use a spatial index:

* Create the spatial index, and;
* Use a function that is "spatial index aware".

To **create** a spatial index, use the `CREATE INDEX` command, but specify the `USING GIST` access method, like so:

```postgres
CREATE INDEX mytable_geom_x ON mytable USING GIST (geom)
```

When bound to the `GIST` access method, a the default index for a geometry is an [R-Tree](https://en.wikipedia.org/wiki/R-tree). If you forget to include the `USING GIST` part, you will get a standard PostgreSQL "B-Tree" index, which is a non-spatial index and will not help your queries.

There are a limited set of functions that can make use of a spatial index as a pre-filter, to speed up queries, and here they are:

* [ST_LineCrossingDirection](/docs/ST_LineCrossingDirection.html)
* [ST_DWithin](/docs/ST_DWithin.html)
* [ST_Touches](/docs/ST_Touches.html)
* [ST_Intersects](/docs/ST_Intersects.html)
* [ST_Crosses](/docs/ST_Crosses.html)
* [ST_Contains](/docs/ST_Contains.html)
* [ST_ContainsProperly](/docs/ST_ContainsProperly.html)
* [ST_Within](/docs/ST_Within.html)
* [ST_Covers](/docs/ST_Covers.html)
* [ST_CoveredBy](/docs/ST_CoveredBy.html)
* [ST_Overlaps](/docs/ST_Overlaps.html)
* [ST_DFullyWithin](/docs/ST_DFullyWithin.html)
* [ST_3DDWithin](/docs/ST_3DDWithin.html)
* [ST_3DDFullyWithin](/docs/ST_3DDFullyWithin.html)
* [ST_3DIntersects](/docs/ST_3DIntersects.html)
* [ST_OrderingEquals](/docs/ST_OrderingEquals.html)
* [ST_Equals](/docs/ST_Equals.html)

So a spatial join query can make use of a spatial relationship function and get index acceleration like this:

```postgres
SELECT a.*
FROM a
WHERE ST_Intersects(a.geom, ST_Point(-126, 45, 4326))
```

And can use a spatial relationship to drive a join, like this:

```postgres
SELECT a.name, b.id
FROM a
JOIN b
  ON ST_Contains(a.geom, b.geom)
WHERE a.name = 'Pleasantown'
```

