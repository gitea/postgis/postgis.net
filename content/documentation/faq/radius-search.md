---
title: "How can I find all objects within a radius of another object?"
date: 2022-02-01
draft: false
geekdocHidden: true
geekdocHiddenTocTree:
tags: [geometry,geography, ST_DWithin]
---

There are a few ways to do a radius search, but the key here is to do an **efficient** search.

There is a function purpose-built for this query, [ST_DWithin()](/docs/ST_DWithin.html).

```postgres
SELECT *
FROM mytable
WHERE ST_DWithin(geom, 'POINT(1000 1000)', 100.0)
```

The "DWithin" stands for "within distance". You could form the same query using [ST_Distance()](docs/ST_Distance.html), but it would run much slower, because there is no way for such a query to leverage a [spatial index]({{< relref "spatial-indexes" >}}).

Another approach is to use the [ST_Intersects()](docs/ST_Intersects.html) and [ST_Buffer()](docs/ST_Buffer.html) functions together like this.

```postgres
SELECT *
FROM mytable
WHERE ST_Intersects(ST_Buffer(geom, 100.0), 'POINT(1000 1000)')
```

This will also work and return the same results as the other approaches, but is frequently slower, because constructive functions like ST_Buffer()](docs/ST_Buffer.html) can be very expensive. However, sometimes for cases with a complex literal on one side that gets buffered, and a simple test set like a table of points, the buffer approach can actually be faster than [ST_DWithin()](/docs/ST_DWithin.html).


