---
title: "Will the GPL license used by PostGIS force me to release my source code?"
date: 2022-02-01
draft: false
geekdocHidden: true
geekdocHiddenTocTree: false
tags: [licensing]
---

PostGIS is open source software available under the [GNU GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html). While the GPL does have some expansive "share and share alike" clauses in it, they **do not apply** to the ordinary uses people make of a spatial database, such as loading data into it and running queries against it.

If you are releasing software that uses PostGIS, does that mean your software has to be licensed using the GPL like PostGIS? Will you have to publish all your code if your use PostGIS?

Almost certainly **not**. As an example, consider an Oracle database running on Linux. Linux is GPL, Oracle is not: does Oracle running on Linux have to be distributed using the GPL? No. Similarly your software can use a PostgreSQL/PostGIS database as much as it wants and be under any license you like.

The **only exception** would be if you made changes to the PostGIS source code, and distributed your changed version of PostGIS. In that case you would have to share the code of your changed PostGIS (but not the code of applications running on top of it). Even in this limited case, you would still only have to distribute source code to people you distributed binaries to. The GPL does not require that you publish your source code, only that you share it with people you give binaries to.
