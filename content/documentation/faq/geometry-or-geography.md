---
title: "Should I use the geometry type or the geography type?"
date: 2022-02-01
draft: false
geekdocHidden: true
geekdocHiddenTocTree: false
tags: [geometry,geography]
---

The **geography** type is very convenient for people whose data is **of global extent** and for people who do not want to learn about projected coordinate systems. However, calculations on the spheroid are very expensive, so many queries will be slower in geography than geometry.

Also, because calculations on the sphere are tricky, only a [subset of functions](https://postgis.net/docs/PostGIS_Special_Functions_Index.html#PostGIS_GeographyFunctions) are directly implemented for geography.  In particular, the following spatial functions do calculations on the sphere/spheroid, and using spatial indexes where possible.

* ST_Area
* ST_Azimuth
* ST_CoveredBy
* ST_Covers
* ST_DWithin
* ST_Distance
* ST_Intersects
* ST_Length
* ST_Perimeter
* ST_Project
* ST_Segmentize

The **geometry** type is easy to understand, and represents data on a cartesian plane. All the vector functions in PostGIS work against geometry. You will have to choose a planar "spatial reference system" for your data if it does not already have one, but then all your calculations will be very simple, and will run much faster than on geography.

If you can fit your data into a planar coordinate system (if it is not truly global in extent) you should consider using **geometry** rather than geography.
