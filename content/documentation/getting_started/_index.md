---
title: Getting Started
date: 2022-02-01
description: "The very basics of enabling and maybe installing PostGIS."
weight: 20
geekdocHidden: false
---

## Enabling PostGIS

If you are running PostgreSQL as a service from a cloud provider (not installing it yourself) then you probably **already** have PostGIS installed, and you just need to enable it in your database. Connect to your database as the `postgres` user or another super-user account, and run:

```postgres
CREATE EXTENSION postgis;
```

Congratulations! PostGIS is now enabled.


### Explore Your Install

You can find out what version you have, with [postgis_full_version()](https://postgis.net/docs/PostGIS_Full_Version.html).

```postgres
SELECT PostGIS_Full_Version();
```

You can also check to see what alternate PostGIS versions might be installed.

```postgres
SELECT * FROM pg_available_extensions WHERE name = 'postgis';
```


### Learn about Spatial SQL

Start with the **[PostGIS Workshop](/workshops/postgis-intro/)** for a hands-on guide to using spatial SQL for spatial data management and analysis.


## Installing PostGIS

If you are running your own system, and want to install PostGIS (and PostgreSQL) directly on it yourself, here are some basic instructions.

{{< toc-tree >}}

## Upgrading your PostGIS and PostgreSQL

If you are just upgrading PostGIS, keeping the same PostgreSQL version, generally all you need to do is install the latest 
PostGIS binaries and then in each of your spatial databases, run:

```postgres
SELECT postgis_extensions_upgrade();
-- verify you are running latest now
SELECT postgis_full_version();
```

[FOSS4G 2023: Upgrade your PostgreSQL and PostGIS video](https://video.osgeo.org/w/mJE7pDirdktknzAPN5KjB6) goes over why you should
upgrade your PostgreSQL and PostGIS, and steps to using pg_upgrade to do so.