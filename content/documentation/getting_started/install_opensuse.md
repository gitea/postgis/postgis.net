---
title: Installing on OpenSUSE and SUSE
date: 2022-02-01
weight: 100
geekdocHidden: true
geekdocHiddenTocTree: false
---

Packages are available from the [OpenSUSE build site](https://build.opensuse.org/package/show/Application:Geo/postgis).