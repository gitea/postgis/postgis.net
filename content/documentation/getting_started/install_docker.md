---
title: Installing on Docker
date: 2022-02-01
weight: 100
geekdocHidden: true
geekdocHiddenTocTree: false
---

* PostGIS has a community [docker-postgis](https://github.com/postgis/docker-postgis) project.
* PostGIS Docker builds can be pulled from the [PostGIS Docker Hub](https://registry.hub.docker.com/r/postgis/postgis/).
* Video covering [Running PostGIS with Docker on Windows, Mac & Linux](https://video.osgeo.org/w/9cZiX3fMCtpPwhZgRw3oqa)
