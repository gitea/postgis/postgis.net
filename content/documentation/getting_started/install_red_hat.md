---
title: Installing on Red Hat / Centos / Scientific Linux
date: 2022-02-01
weight: 100
geekdocHidden: true
geekdocHiddenTocTree: false
---

The best place to get the latest binaries for both PostgreSQL and PostGIS is from the [PostgreSQL download page Redhat Derivatives](https://www.postgresql.org/download/linux/redhat/).

* Select the PostgreSQL version
* Select a "Platform" and an "Architecture".
* Copy and run the setup script as root.

Further, run the following to add in PostGIS and dependencies.

```
# Install EPEL repo RPM:
dnf -y install epel-release

# Enable PowerTools repo (required for some of the dependencies):
dnf -y config-manager --set-enabled PowerTools

# you might need to do this instead for the Rockys
dnf config-manager --enable crb

# for Rocky 8 and above might need to do this as well
crb enable

# Now, you can finally install PostGIS
# Select the right PostGIS and PostgreSQL versions
dnf -y install postgis34_16

systemctl restart postgresql-16
```

