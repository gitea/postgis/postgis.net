---
title: Installing on MacOS
date: 2022-02-01
weight: 100
geekdocHidden: true
geekdocHiddenTocTree: false
---

* A popular distribution particularly for newbies is [Postgres.app](https://postgresapp.com/). It includes generally latest version of PostgreSQL and PostGIS. Great for development and testing. Do not mix with other installations. [Video](https://video.osgeo.org/w/pxcBCc4oHhAZvUi9NdWxXf) details how to install postgres.app and how to get started with PostGIS and QGIS.

* [Homebrew](https://brew.sh) users can just run "brew install postgis" and tends to be a favorite for more advanced users since there are brew scripts for most of the popular PostgreSQL extensions, not always present in other Mac distributions.

* The [EnterpriseDB OSX PostgreSQL](https://www.enterprisedb.com/postgres-tutorials/installation-postgresql-mac-os) combination from EnterpriseDB includes generally latest stable minor version of PostGIS.

