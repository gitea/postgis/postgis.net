---
title: Latest Released Version
date: 2024-09-28
weight: 10
geekdocHidden: true
geekdocHiddenTocTree: false
---

PostGIS 3.5.2 came out January 18th, 2025. Windows PostGIS Bundle 3.5.2 installers for PostgreSQL 13-17 was released February 1st 2025.

Binaries for versions of PostgreSQL 13-17 (64-bit) available in `Unreleased PostGIS Versions` and [OSGeo downloads][5]). Installers for 13-17(64-bit) available on [OSGeo downloads][5] and application stackbuilder.

PostGIS 3.5.2 bundle includes:

  * PostGIS 3.5.2 with MVT support, with raster, [GEOS](https://libgeos.org/posts/2024-09-06-geos-3-13-released/) 3.13.0, [PROJ](https://proj.org/download.html#past-releases) 8.2.1, [SFCGAL](http://sfcgal.org) support (1.5.2), address_standardizer, topology
  * PostGIS Tiger geocoder extension - Tiger 2024
  * pgRouting 3.7 [pgRouting 3.7.2](https://docs.pgrouting.org/3.7/en/index.html)
  * Commandline raster loader (raster2pgsql), shapefile import/export (shp2pgsql,pgsql2shp)
  * Commandline [osm2pgrouting 2.3.8](https://github.com/pgRouting/osm2pgrouting/wiki/Documentation-for-osm2pgrouting-v2.3) for loading data from .osm files into pgRouting routable format
  * GUI: shp2pgsql-gui which has both import and export support for geometry/geography
  * [ogrfdw 1.1.5](https://github.com/pramsey/pgsql-ogr-fdw/releases/tag/v1.1.5)  - spatial foreign data wrapper for
		reading both spatial (spatial columns become postgis geometry) and non-spatial data.    [IMPORT FOREIGN SCHEMA](http://www.postgresonline.com/journal/archives/359-Import-Foreign-Schema-for-ogr_fdw-for-PostgreSQL-9.5.html) support
    New features in 1.1 - `character_encoding` option and utility functions `ogr_fdw_drivers()`, `ogr_fdw_version()`.
    Changes in 1.1.5 include improved date handling and support of GDAL list types mapping to PostgreSQL array types.

  * [GDAL 3.9.2](https://gdal.org/download.html#current-releases) with OpenJPEG 2.5.20 (JPEG 2000), ODBC, Curl, SQLite3 (for GeoPackage and OSM support), excel (XLS) (via FreeXL 1.0.6), libreoffice, XLSX spreadsheet (via expat), Arrow 13.0 for Arrow, Parquet, and GeoParquet support  (used by both PostGIS raster and ogrfdw)
  * [pgpointcloud](https://github.com/pgpointcloud/pointcloud) 1.2.5
    for querying LIDAR point cloud and in/out functions to convert to PostGIS geometry
  * [h3-pg](https://github.com/zachasme/h3-pg) 4.1.4
    for using Uber h3 api and converting h3 index representations to postgis geometry/geography/raster (raster support is new in this release).
  * [MobilityDB](https://mobilitydb.com) [v1.2.0](https://github.com/MobilityDB/MobilityDB/releases/tag/v1.2.0)
    for managing trajectories.  Includes many temporal spatial types and functions for them. Refer to [Enabling PostGIS Extensions: MobilityDb](../enabling_postgis/#mobilitydb) for guidance.

The simplest way to get PostGIS on Windows for the EnterpriseDb Windows PostgreSQL distribution is using the StackBuilder.

Refer to [An Almost Idiot's Guide Installing PostGIS on Windows][4]
or watch the video [Getting started with PostGIS and QGIS on windows][8]

  1. Download and install PostgreSQL [from EnterpriseDB](https://www.postgresql.org/download/windows/).
  2. Run the "StackBuilder" utility and install the PostGIS add-on.

If you want to use the zip, setup, and setup source files directly
they are available:

* [Zip and Installer files][5]  The .zip files are just the binaries you can copy into
 your PostgreSQL installation and then enable in each database as usual. The exe are standard windows setup for 64-bit PostgreSQL.
* [Source Setup files][6] The source setup files are useful if you want to build your own [NSIS installer][7] or create a single setup for your own project that also installs PostGIS. The [Binary zip files][5] should contain all the binary files you need to customize your own setup minus the documentation. You can also just extract the .exe setup file to get the binaries, but the extract sometimes gets mangled leaving out some of the postgis-gui subfolders.

[4]: https://www.bostongis.com/PrinterFriendly.aspx?content_name=postgis_tut01
[5]: https://download.osgeo.org/postgis/windows
[6]: https://download.osgeo.org/postgis/windows/source
[7]: http://nsis.sourceforge.net/Download
[8]: https://video.osgeo.org/w/57e27085-6352-43e6-b64a-c29c1dcda8ee
