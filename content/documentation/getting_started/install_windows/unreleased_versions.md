---
title: Unreleased Versions
date: 2022-02-01
weight: 30
geekdocHidden: true
geekdocHiddenTocTree: false
---

If you are more adventurous, and risk-seeking you can partake in our experimental windows
binaries of PostGIS built automatically by Winnie whenever there is a change in any of the PostGIS Stable or development branches. These are especially useful for testing out new features or if you are badly in need of a bug fix that has not been released yet.

If you don't want to risk messing up an existing windows install or aren't allowed to write to the registry, we'd suggest using the companion [PostgreSQL EDB binaries-only](https://www.enterprisedb.com/download-postgresql-binaries) binaries which don't require installation. Setup is detailed in [Starting PostgreSQL in windows without install](https://www.postgresonline.com/journal/archives/172-Starting-PostgreSQL-in-windows-without-install.html).

## Windows: Winnie Bot PostGIS and related extensions Experimental Builds

* [for PostgreSQL 32-bit/64-bit][1]

### Installing Experimental Binaries

* Copy the contents of the respective folders in the zip file to your PostgreSQL install and when prompted to overwrite, overwrite
* The newer GEOS will work with older PostGIS installs, but newest PostGIS will only work with the newer GEOS.

[1]: https://winnie.postgis.net/download/windows/
