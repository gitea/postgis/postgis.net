---
title: Installing on windows
date: 2022-02-01
weight: 120
geekdocHidden: true
geekdocHiddenTocTree: false
---

If you need help using these packages (the installers and buildbot binaries), please ask on the [postgis-users](https://lists.osgeo.org/mailman/listinfo/postgis-users) mailing list.

If you find a bug with the installers or the buildbot packages, please report these on our [Ticket Tracker](/support) under component: build/upgrade/install with keyword: windows.

***The installers are designed to work with [EnterpriseDb PostgreSQL distributions](https://www.postgresql.org/download/windows/).***

Watch the video [Getting started with PostGIS and QGIS on windows](https://video.osgeo.org/w/57e27085-6352-43e6-b64a-c29c1dcda8ee)
or refer to [An Almost Idiot's Guide Installing PostGIS on Windows](https://www.bostongis.com/PrinterFriendly.aspx?content_name=postgis_tut01)

{{< toc-tree >}}