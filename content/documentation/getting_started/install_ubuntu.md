---
title: Installing on Ubuntu / Debian
date: 2022-02-01
weight: 100
geekdocHidden: true
geekdocHiddenTocTree: false
---

* The best place to get the latest binaries for both PostgreSQL and PostGIS is from the [PostgreSQL download page Debian](https://www.postgresql.org/download/linux/ubuntu/) and [PostgreSQL download page Ubuntu](https://www.postgresql.org/download/linux/ubuntu/).

* The PostgreSQL build team has packages for Debian and Ubuntu for several versions of PostgreSQL and PostGIS [APT repository](https://wiki.postgresql.org/wiki/Apt) for PostgreSQL builds.

* For specific PostGIS directions, see the [Ubuntu install guide](https://trac.osgeo.org/postgis/wiki/UsersWikiPostGIS3UbuntuPGSQLApt).