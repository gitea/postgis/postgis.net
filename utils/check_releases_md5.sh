#!/bin/sh

WORKDIR=$PWD/releases-check
API=https://git.osgeo.org/gitea/api/v1
DOWNLOAD_BASE=https://download.osgeo.org/postgis/source
MD5_BASE=https://postgis.net/stuff
CONFIG=$(cd $(dirname $0)/../; pwd)/config.toml

mkdir -p ${WORKDIR}
cd ${WORKDIR}

echo "Fetching list of published releases"
tomlq -r '.params.postgis.releases[] | select( .is_dev != true ) | .tag' \
  ${CONFIG} > checked_releases.txt

while read REL; do
  relname=postgis-${REL}.tar.gz
  md5name=${relname}.md5

  echo -n "Checking ${relname} ... "
  curl -s ${DOWNLOAD_BASE}/${relname} -o ${relname} || {
    echo "unable to download ${relname} from ${DOWNLOAD_BASE}"
    exit 1
  }
  curl -s ${MD5_BASE}/${md5name} -o ${md5name} || {
    echo "unable to download ${md5name} from ${MD5_BASE}"
    exit 1
  }
  md5sum ${relname} | diff - ${md5name} > /dev/null 2>&1 || {
    echo "MD5 mismatch"
    exit 1
  }
  echo "OK"
done < checked_releases.txt
