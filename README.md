[![status-badge](https://woodie.osgeo.org/api/badges/127/status.svg)](https://woodie.osgeo.org/repos/127)

## Static Site Build

Our site is built with [hugo](https://gohugo.io/).
To work with the docs you will probably want to
[install hugo](https://gohugo.io/getting-started/installing/#quick-install)
so you can see the rendered result as you work.

## Theme

This site is using a customized version of the
[hugo-geekdoc](https://geekdocs.de) theme.

All changes are implemented in the side, not in the theme,
so DO NOT EDIT THE THEME

	- "tips" now have their own "type", they are no longer posts
	- "news" is the post type for news!
	- "events" is the type for events!

DO NOT EDIT THE THEME!

## Development

Install the hugo binary for your operating system.
Run the local server:

    hugo server

Remember to set "draft: false" in the page front matter or your content
won't show up.

## Building

To build run hugo without any arguments.
This will create a public folder:

    hugo


## Testing

Run `make check` after changes, before push

